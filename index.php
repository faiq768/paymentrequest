<!DOCTYPE html>
<html>
<head>

<meta charset="utf-8">

<title>PT ASURANSI DAYIN MITRA TBK</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">

<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/style1.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/pages/signin.css" rel="stylesheet" type="text/css">
<link href="datatables/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="datatables/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="css/table-responsive.css" rel="stylesheet" type="text/css">
<link href="css/style-responsive.css" rel="stylesheet" type="text/css">
<link href="datatables/css/scroller.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="datatables/css/dataTables.checkboxes.css" rel="stylesheet" type="text/css">

<script src="js/jquery-3.1.1.min.js"></script> 
<script src="js/sweetalert.min.js"></script>
<script src="js/myscript1.js"></script>

</head>
<body>
  <?php
  if ($_GET['page'] != 'payreqnew') {
  ?>
  <div class="loader">
    <img src="img/loading.gif" alt="processing..." />
  </div>
  <?php } ?>
 <div id="pageloader">
   <img src="img/loading.gif" alt="processing..." />
</div>
  
<?php session_start();
  include "koneksi/koneksi.php";
  include "page/default/header.php";
 ?>

 
<div class="main-inner">
  <div class="container">
	<script>
	
	</script>
  
    <div class="row">
      <?php
        include "page/default/page.php"; 
       ?>
    </div>
  </div>
</div>

<?php 
  //include "page/default/footer.php";
  
?>

<script src="js/jquery-1.7.2.min.js"></script> 
<script src="js/jquery.mask.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="datatables/js/jquery.dataTables.min.js"></script>
<script src="datatables/js/dataTables.responsive.min.js"></script>
<script src="datatables/js/dataTables.scroller.min.js"></script>
<script src="datatables/js/dataTables.checkboxes.min.js"></script>
<script src="js/signin.js"></script>
<script src="js/faiqFormatMoney.js"></script>
<script>
window.addEventListener("load", function () {
    const loader = document.querySelector(".loader");
    loader.className += " hidden"; // class "loader hidden"
});
window.onbeforeunload = mySession;
function mySession(){
    $.ajax({
        url: "fungsi/exit.php",
        async: false,
        type: "GET"
    });
}
$(window).load(function() { $("#loading").fadeOut("slow"); });
  $(document).ready(function(){
	  //$('#intermediaryCode').modal('show');
	  
     $('.dropdown').css({ 'display': 'none'});
    var stickyNavTop = $('.navbar-2').offset().top;

        var stickyNav = function(){

          var scrollTop = $(window).scrollTop();

          if (scrollTop > stickyNavTop) {
            $('.navbar-1').css({ 'position': 'fixed', 'top':0, 'z-index':9999, 'width':'100%','padding':'0' });
            $('.navbar-2').css({ 'position': 'fixed', 'top':25, 'width':'100%','padding':'0' });
            $('.subnavbar').css({ 'position': 'fixed', 'top':30, 'z-index':999, 'width':'100%','padding':'0' });
            $('.subnavbar').css({ 'position': 'fixed', 'top':35, 'width':'100%','padding':'0','-moz-transition':'.3s all ease-in','-webkit-transition':'.3s all ease-in','transition':'.3s all ease-in' });
            $('.main-inner').css({'margin-top':'100px'});
          } else {
            $('.main-inner').css({'margin-top':'0px'});
            $('.navbar-1, .navbar-2').css({ 'position': 'relative', });
            $('.navbar-2').css({ 'top':0});
            $('.subnavbar, .navbar-2').css({ 'position': 'relative', });
            $('.subnavbar').css({ 'top':0});
          }

        };

        stickyNav();

        $(window).scroll(function() {

          stickyNav();

        });
    $('.msg').fadeIn('slow').delay( 2000 ).fadeOut('slow');

    $('.date').mask("99-99-9999");
  });

</script>



</body>
</html>
