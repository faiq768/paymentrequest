<?php session_start();

$table = 'tpaymentrequestheader';

$primaryKey = 'PaymentRequestNo';

$columns = array(
    array( 'db' => 'PaymentRequestNo',            'dt' => 0, 'field' => 'PaymentRequestNo' ),
    array( 'db' => 'IntermediaryName',            'dt' => 1, 'field' => 'IntermediaryName' ),
    array( 'db' => 'Bank',                        'dt' => 2, 'field' => 'Bank' ),
    array( 'db' => 'CREATED_DATE',                'dt' => 3, 'field' => 'CREATED_DATE' ),
    array( 'db' => 'STATUS_PR',                   'dt' => 4, 'field' => 'STATUS_PR' ),
    array( 'db' => 'STATUS_PAID',                 'dt' => 5, 'field' => 'STATUS_PAID' ),
    array( 'db' => 'VoucherNo',                   'dt' => 6, 'field' => 'VoucherNo' ),
    array( 'db' => 'PAY_PAID_DATE',               'dt' => 7, 'field' => 'PAY_PAID_DATE' )
    //array( 'db' => '`NET_BALANCE`',              'dt' => 9, 'field' => 'NET_BALANCE' )
  
                           
);
// SQL server connection information

$sql_details = array(
    'user' => 'root',
    'pass' => 'P@ssw0rd',
    'db'   => 'production_paymentrequest',
    'host' => '192.168.24.33'
);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
// require( 'ssp.class.php' );
require('../datatables/serverside.ssp.class.php' );

$where = " STATUS_PAID = 'PAID' and ISACTIVE=1 ";

// $groupBy = " `POLICY_NO`,  `NOTE_NO`";  
// $having = "round((sum(APPLY_AMT_ORIG) - NET_BALANCE),0) = 0";
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $where)
);