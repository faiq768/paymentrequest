<?php session_start();
$host = "192.168.24.33";
    //$host = "localhost";
    
$user = "root";
$pass = "P@ssw0rd";
// $pass = "";
//$db     = "production_paymentrequest";
$db   = "production_paymentrequest";

$conn = mysqli_connect($host,$user,$pass) or die("cannot connect");
mysqli_select_db($conn, $db);

$table = 'stg_pmt_req_datamart';

$primaryKey = 'NOTE_NO';
$PaymentNo = $_GET['PaymentNo'];

$sql_details = array(
    'user' => 'root',
    'pass' => 'P@ssw0rd',
    'db'   => 'production_paymentrequest',
    'host' => '192.168.24.33'
);

$QUERY      =   "SELECT DISTINCT IntermediaryType, IntermediaryName, IntermediaryCode, branch_code, Start_Periode, End_Periode From tpaymentrequestheader where PaymentRequestNo = '$PaymentNo' ";
$SQL        =   mysqli_query($conn, $QUERY) or die(mysqli_error($conn));
$FETCH      =   mysqli_fetch_array($SQL);
$columns = array(
    array( 'db' => '`u`.`NOTE_NO`',                  'dt' => 0, 'field' => 'NOTE_NO' ),
    array( 'db' => '`u`.`POLICY_NO`',                'dt' => 1, 'field' => 'POLICY_NO' ),
    array( 'db' => '`u`.`NOTE_NO`',                  'dt' => 2, 'field' => 'NOTE_NO' ),
    array( 'db' => '`u`.`INTERMEDIARY_CODE`',        'dt' => 3, 'field' => 'INTERMEDIARY_CODE' ),
    array( 'db' => '`u`.`INTERMEDIARY_NAME`',        'dt' => 4, 'field' => 'INTERMEDIARY_NAME' ),
    array( 'db' => '`u`.`INTERMEDIARY_TYPE`',        'dt' => 5, 'field' => 'INTERMEDIARY_TYPE' ),
    array( 'db' => '`u`.`POLICY_LINE_OF_BUSINESS`',  'dt' => 6, 'field' => 'POLICY_LINE_OF_BUSINESS' ),
    array( 'db' => '`u`.`POLICY_CORP_ID`',           'dt' => 7, 'field' => 'POLICY_CORP_ID' ),
    array( 'db' => '`u`.`PAYMENT_METHOD`',           'dt' => 8, 'field' => 'PAYMENT_METHOD' ),
    array( 'db' => '`ud`.`branch_name`',              'dt' => 9, 'field' => 'branch_name' ),
    array( 'db' => '`u`.`NET_BALANCE`',              'dt' => 10, 'field' => 'NET_BALANCE' )
  
                           
);
// SQL server connection information


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
// require( 'ssp.class.php' );
require('../datatables/new.ssp.class.php' );
$joinQuery = "FROM `stg_pmt_req_datamart` AS `u` LEFT JOIN `master_branch` AS `ud` ON (`ud`.`branch_code` = `u`.`POLICY_CORP_ID`)";
$extraWhere = "`u`.`NOTE_NO` NOT IN (SELECT a.NotaNo FROM tpaymentrequestdetail a join tpaymentrequestheader b on b.ID=a.paymentrequestid where b.ISACTIVE=1 AND b.status_pr!='REJECT') 
    AND `u`.`POLICY_CORP_ID` in ( ".$_SESSION['branch_codeStr']." ) and SUBSTRING(intermediary_type, 1, 30) IN(".$_SESSION['intermediary_typeStr']." ) AND `u`.`POLICY_CORP_ID` = '".$FETCH['branch_code']."' AND `u`.`INTERMEDIARY_TYPE` = '".$FETCH['IntermediaryType']."' AND `u`.`INTERMEDIARY_NAME` = '".$FETCH['IntermediaryName']."' AND POST_DATE BETWEEN '".$FETCH['Start_Periode']."' AND '".$FETCH['End_Periode']."'
";
$groupBy = "`u`.`POLICY_NO`, `u`.`NOTE_NO`";  
$having = "round((sum(APPLY_AMT_ORIG) - NET_BALANCE),0) = 0";
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
);
