<?php session_start();

$table = 'tpaymentrequest_installment';

$primaryKey = 'PaymentRequestNo';

$columns = array(
    array( 'db' => 'th.ID',  'dt' => 0, 'field' => 'ID' ),
    array( 'db' => 'CONCAT_WS("/",a.PaymentRequestNo,dt.Installment_ke)',         'dt' => 1, 'field' => 'CONCAT_WS("/",a.PaymentRequestNo,dt.Installment_ke)' ),
    array( 'db' => 'a.INTERMEDIARY_TYPE',        'dt' => 2, 'field' => 'INTERMEDIARY_TYPE' ),
    array( 'db' => 'a.Client',         'dt' => 3, 'field' => 'Client' ),
    array( 'db' => 'dt.CCY',         'dt' => 4, 'field' => 'CCY' ),
    array( 'db' => 'a.CREATED_DATE','dt' => 5, 'field' => 'CREATED_DATE' ),
    array( 'db' => 'a.STATUS_INS', 'dt' => 6, 'field' => 'STATUS_INS' ),
    array( 'db' => 'dt.PAID_STATUS', 'dt' => 7, 'field' => 'PAID_STATUS' ),
    array( 'db' => 'dt.VoucherNo', 'dt' => 8, 'field' => 'VoucherNo' ),
    array( 'db' => 'dt.VoucherDate', 'dt' => 9, 'field' => 'VoucherDate' ),
    array( 'db' => 'CONCAT_WS(";",th.ID,a.PaymentRequestNo,dt.Installment_ke,dt.CCY,dt.VoucherNO,dt.VoucherDate)', 'dt' => 10, 'field' => 'CONCAT_WS(";",th.ID,a.PaymentRequestNo,dt.Installment_ke,dt.CCY,dt.VoucherNO,dt.VoucherDate)' ),
                           
);
// SQL server connection information

$sql_details = array(
    'user' => 'root',
    'pass' => 'P@ssw0rd',
    'db'   => 'production_paymentrequest',
    'host' => '192.168.24.33'
);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
// require( 'ssp.class.php' );
require('../datatables/new.ssp.class.php' );
$joinQuery = "FROM tpaymentrequest_installment a
JOIN tpaymentrequest_installment_detail dt ON dt.installment_id = a.id
JOIN tpaymentrequestheader th ON th.PaymentRequestNo = a.PaymentRequestNo";
$extraWhere = " 1=1 AND dt.PAID_STATUS='PAID'";


$groupBy = "";  
$having = "";
// $orderBy = " a.CREATED_DATE DESC";
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
);