<?php session_start();

$table = 'stg_pmt_req_datamart';

$primaryKey = 'NOTE_NO';

$columns = array(
    array( 'db' => '`u`.`POLICY_NO`',                'dt' => 0, 'field' => 'POLICY_NO' ),
    array( 'db' => '`u`.`NOTE_NO`',                  'dt' => 1, 'field' => 'NOTE_NO' ),
    array( 'db' => '`u`.`INTERMEDIARY_CODE`',        'dt' => 2, 'field' => 'INTERMEDIARY_CODE' ),
    array( 'db' => '`u`.`INTERMEDIARY_NAME`',        'dt' => 3, 'field' => 'INTERMEDIARY_NAME' ),
    array( 'db' => '`u`.`INTERMEDIARY_TYPE`',        'dt' => 4, 'field' => 'INTERMEDIARY_TYPE' ),
    array( 'db' => '`u`.`POLICY_LINE_OF_BUSINESS`',  'dt' => 5, 'field' => 'POLICY_LINE_OF_BUSINESS' ),
    array( 'db' => '`u`.`POLICY_CORP_ID`',           'dt' => 6, 'field' => 'POLICY_CORP_ID' ),
    array( 'db' => '`u`.`PAYMENT_METHOD`',           'dt' => 7, 'field' => 'PAYMENT_METHOD' ),
    array( 'db' => '`ud`.`branch_name`',              'dt' => 8, 'field' => 'branch_name' ),
    array( 'db' => '`u`.`NET_BALANCE`',              'dt' => 9, 'field' => 'NET_BALANCE' )
  
                           
);
// SQL server connection information

$sql_details = array(
    'user' => 'root',
    'pass' => 'P@ssw0rd',
    'db'   => 'production_paymentrequest',
    'host' => '192.168.24.33'
);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
// require( 'ssp.class.php' );
require('../datatables/new.ssp.class.php' );
$joinQuery = "FROM `stg_pmt_req_datamart` AS `u` LEFT JOIN `master_branch` AS `ud` ON (`ud`.`branch_code` = `u`.`POLICY_CORP_ID`)";
$extraWhere = "`u`.`NOTE_NO` NOT IN (SELECT a.NotaNo FROM tpaymentrequestdetail a join tpaymentrequestheader b on b.ID=a.paymentrequestid where b.ISACTIVE=1 AND b.status_pr!='REJECT')
    AND `u`.`POLICY_CORP_ID` in ( ".$_SESSION['branch_codeStr']." ) and SUBSTRING(intermediary_type, 1, 30) IN(".$_SESSION['intermediary_typeStr']." )
";
$groupBy = "`u`.`POLICY_NO`, `u`.`NOTE_NO`";  
$having = "round((sum(APPLY_AMT_ORIG) - NET_BALANCE),0) = 0";
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
);