<?php
if (isset($_GET['id'])) {

$table = 'stg_pmt_req_datamart';
$primaryKey = 'INTERMEDIARY_CODE';
 
$columns = array(
	array( 'db' => '`u`.`INTERMEDIARY_CODE`',     'dt' => 0, 'field' => 'INTERMEDIARY_CODE' ),
    array( 'db' => '`u`.`INTERMEDIARY_NAME`',     'dt' => 1, 'field' => 'INTERMEDIARY_NAME' ),
    array( 'db' => '`c`.`payment_method`',        'dt' => 2, 'field' => 'payment_method' )
);
 
$sql_details = array(
    'user' => 'root',
    'pass' => 'P@ssw0rd',
    'db'   => 'production_paymentrequest',
    'host' => '192.168.24.33'
);
require('../datatables/ssp.class.php');

$joinQuery = " FROM `stg_pmt_req_datamart` AS `u` JOIN `company` AS `c` ON `c`.`company_code` = `u`.`INTERMEDIARY_CODE` ";

$extraWhere = "INTERMEDIARY_TYPE = '".$_GET['id']."'";

echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere )
);

}
