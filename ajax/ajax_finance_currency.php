<?php

$table = 'currency';
$primaryKey = 'currency_code';
 
$columns = array(
    array( 'db' => 'currency_code',     'dt' => 0, 'field' => 'currency_code' ),
    array( 'db' => 'currency_name',     'dt' => 1, 'field' => 'currency_name' )
);
 
$sql_details = array(
    'user' => 'root',
    'pass' => 'P@ssw0rd',
    'db'   => 'production_paymentrequest',
    'host' => '192.168.24.33'
);
require('../datatables/serverside.ssp.class.php');

echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);


