<!-- 
** ajax_type                                        **
** Modified By FAIQ                                 **
** Modified Date 20181024                           **
** QUERY RETRIVE                                    **
** UPGRADE PAGINATION TO AUTOMATIC WITH DATATABLES  ** 
** START                                            **
 -->
<?php 
session_start(); 
include "../koneksi/koneksi.php";
if (isset($_POST['group_id']) && !empty($_POST['group_id'])) {
    $group_id = $_POST['group_id'];
    $query = "SELECT a.* FROM security_menu a WHERE a.ISACTIVE = 1 ORDER BY a.menu_group, a.menu, a.ID";
    $data       = mysqli_query($conn, $query);

    $ceklist = '';
    while($rs = mysqli_fetch_array($data)){
        $query2 = "SELECT b.* FROM security_group_menu a
                INNER JOIN security_menu b ON b.ID=a.menu_id
                WHERE group_id=$group_id AND menu_id=".$rs['ID']."
                "; 
        $data2  = mysqli_query($conn, $query2);
        $dataRow    = $data2->num_rows;

        if($dataRow > 0)
        {
            $ceklist .= '<input checked=checked type="checkbox" name="menu[]" id="menu" name="color[]" value="'.$rs['ID'].'" />'.$rs['Menu'].' <br>'; 
        }
        else
        {
            $ceklist .= '<input type="checkbox" name="menu[]" id="menu" name="color[]" value="'.$rs['ID'].'" />'.$rs['Menu'].' <br>'; 
        }
        
    }
    echo $ceklist;
}
elseif (isset($_POST['VoucherNo']) && !empty($_POST['VoucherNo'])) {
    $VoucherNo = $_POST['VoucherNo'];
    $query = "SELECT * FROM tpaymentrequestheader WHERE VoucherNo='$VoucherNo' ";
    $data       = mysqli_query($conn, $query);

    $dataRow = $data->num_rows;

    $result = '';
    if($dataRow > 0)
    {
        $result = "$result Already Exists. Are you sure you want to continue ?";
    }

    echo $result;
}

elseif (isset($_POST['username']) && !empty($_POST['username'])) {
//lookup user group
    $username   = $_POST['username'];
    $query      = "SELECT * FROM security_group WHERE ISACTIVE = 1 ORDER BY NAME ";
    $data       = mysqli_query($conn, $query);

    $ceklist = '';
    while($rs = mysqli_fetch_array($data)){
        $query2 = "SELECT * FROM security_user_group
                WHERE username='$username' AND group_id=".$rs['ID']."
                "; 
        $data2  = mysqli_query($conn, $query2);
        $dataRow    = $data2->num_rows;

        if($dataRow > 0)
        {
            $ceklist .= '<input checked=checked type="checkbox" name="group[]" id="group" name="color[]" value="'.$rs['ID'].'" />'.$rs['NAME'].' <br>'; 
        }
        else
        {
            $ceklist .= '<input type="checkbox" name="group[]" id="group" name="color[]" value="'.$rs['ID'].'" />'.$rs['NAME'].' <br>'; 
        }
        
    }
    echo $ceklist;
}
elseif (isset($_POST['cCode']) && !empty($_POST['cCode']) && isset($_POST['cCurr']) && !empty($_POST['cCurr'])  ) {
       //payto
       
        $payQuery = mysqli_query($conn, "SELECT a.company_code,a.full_name,a.payment_method,b.BANK_ACCOUNT_NAME,
            b.BANK_ACCOUNT_NO,b.BANK_NAME,b.BRANCH_NAME,b.BANK_CITY,b.CURRENCY_CODE FROM company a 
            LEFT JOIN master_company_bank_accounts b ON a.company_code = B.company_code 
            WHERE a.company_code like '".$_POST['cCode']."%' and b.CURRENCY_CODE like '".$_POST['cCurr']."%' ");

      

        echo "
            <table id='PayTable' class='table table-bordered table-hover table-striped'>
                <thead>
                    <tr>
                        <th>Account Name</th>
                        <th>Account Number</th>
                        <th>Currency</th>
                    </tr>
                </thead>
                <tbody >";

        while ($payCode         =   mysqli_fetch_array($payQuery)) {
         echo "
            <tr class='payToPilih' 
                pay-fullname=' ".$payCode['BANK_ACCOUNT_NAME']."' 
                pay-code=' ".$payCode['company_code']. "'
                pay-paymethod = '".$payCode['payment_method']."'
                pay-bank_name = '".$payCode['BANK_NAME']."'
                pay-account_no = '".$payCode['BANK_ACCOUNT_NO']."'
                pay-branch = '".$payCode['BRANCH_NAME']."'
                pay-city = '".$payCode['BANK_CITY']."'>

                <td>".$payCode['BANK_ACCOUNT_NAME']."</td>
                <td>".$payCode['BANK_ACCOUNT_NO']."</td>
                <td>".$payCode['CURRENCY_CODE']." </td>
            </tr>
            ";
        }

        echo "
                </tbody>
            </table>  
            ";

}
elseif (isset($_POST['id_code']) && !empty($_POST['id_code'])  ) {
    //currency
        $currencyQuery = mysqli_query($conn, "SELECT DISTINCT a.CURRENCY_CODE,b.currency_name FROM master_company_bank_accounts a 
            LEFT JOIN currency b ON a.CURRENCY_CODE = B.currency_code 
            WHERE a.COMPANY_CODE like '".$_POST['id_code']."%' ");
      

        echo "
            <table id='CurrencyTable' class='table table-bordered table-hover table-striped'>
                <thead>
                    <tr>
                        <th>Currency Code</th>
                        <th>Currency</th>
                    </tr>
                </thead>
                <tbody >";

        while ($currencyCode         =   mysqli_fetch_array($currencyQuery)) {
         echo "
            <tr class='currencyPilih' 
                curr-ccy = '".$currencyCode['CURRENCY_CODE']."'
                curr-ccy-name = '".$currencyCode['currency_name']."'>

                
                <td>".$currencyCode['CURRENCY_CODE']." </td>
                <td>".$currencyCode['currency_name']." </td>
            </tr>
            ";
        }

        echo "
                </tbody>
            </table>  
            ";

}
elseif (isset($_POST['currency']) && !empty($_POST['currency'])  ) { //NEW=================================================
    //currency
        $FIRST_CURRENCY_QUERY = mysqli_query($conn, "SELECT * FROM currency where active_flag = 1");
      

        echo "
            <table id='CurrencyTable' class='table table-bordered table-hover table-striped'>
                <thead>
                    <tr>
                        <th>Currency Code</th>
                        <th>Currency</th>
                    </tr>
                </thead>
                <tbody >";

        while ($FETCH_CURRENCY_FIRST         =   mysqli_fetch_array($FIRST_CURRENCY_QUERY)) {
         echo "
            <tr class='currencyPilih' 
                curr-ccy = '".$FETCH_CURRENCY_FIRST['currency_code']."'
                curr-ccy-name = '".$FETCH_CURRENCY_FIRST['currency_name']."'>

                
                <td>".$FETCH_CURRENCY_FIRST['currency_code']." </td>
                <td>".$FETCH_CURRENCY_FIRST['currency_name']." </td>
            </tr>
            ";
        }

        echo "
                </tbody>
            </table>  
            ";

} 
//MODIFIED BY FAIQ
elseif (isset($_POST['inCode']) && !empty($_POST['inCode']) ) 
{
//PaymentRequest-Retrieve

        $CCY        =   '';
        $interCode  =   '';
        $LOB        =   '';
        $proPreiod  =   '';
        $no_polis   =   '';
        $payDate    =   '';
        $tgl1       =  date('Y-m-d', strtotime($_POST['periode_1'])); 
        $tgl2       =  date('Y-m-d', strtotime($_POST['periode_2']));
        $tglpay1    =  date('Y-m-d', strtotime($_POST['payment_1'])); 
        $tglpay2    =  date('Y-m-d', strtotime($_POST['payment_2']));
		$branchCode = $_POST['branch_code'];
     
     if (isset($_POST['inCode']) && !empty($_POST['inCode'])) {
         $interCode = 'INTERMEDIARY_CODE = "'.$_POST['inCode'].'" ';
        
     }if (isset($_POST['Lob']) && !empty($_POST['Lob'])) {
          $LOB = 'AND POLICY_LINE_OF_BUSINESS = "'.$_POST['Lob'].'" ';
         
     }if (isset($_POST['Curren']) && !empty($_POST['Curren'])) {
         $CCY = 'AND DBCR_CURRENCY = "'.$_POST['Curren'].'" ';
         
     }if (isset($_POST['periode_1']) && !empty($_POST['periode_1'])) {
         $proPreiod = 'AND POST_DATE BETWEEN "'.$tgl1.'" AND "'.$tgl2.'" ';
         
     }if (isset($_POST['payment_1']) && !empty($_POST['payment_1'])) {
         $payDate = 'AND TRANS_DATE BETWEEN "'.$tglpay1.'" AND "'.$tglpay2.'" ';
         
     }if (isset($_POST['polisNo']) && !empty($_POST['polisNo'])) {
        if (strlen($_POST['polisNo']) < 28 ) {
            $no_polis = 'AND NOTE_NO = "'.$_POST['polisNo'].'"' ;
        }else{
            $no_polis = 'AND POLICY_NO = "'.$_POST['polisNo'].'"' ;
        }
         
     }
//MODIFIED QUERY BY FAIQ
    $query = "";
    if (isset($_POST['periode_1']) && !empty(['periode_1']) && empty($_POST['polisNo'])) {
        $query = "SELECT INTERMEDIARY_CODE, INSURED_CODE, POLICY_NO, INSURED_NAME, NOTE_NO, DBCR_CURRENCY, (CASE WHEN DATEDIFF(PAID_DATE, DUE_DATE) > 0 THEN 'OVERDUE' ELSE 'DUE' END) AS GROUP_TYPE
		, GROSS_PREMIUM ,NET_BALANCE
                FROM stg_pmt_req_datamart
                WHERE $interCode $LOB $CCY $proPreiod $payDate 
                    AND POLICY_CORP_ID = '".$branchCode."' 
                    AND PAID_DATE IS NOT NULL 
                    AND NOTE_NO NOT IN (SELECT a.NotaNo FROM tpaymentrequestdetail a join tpaymentrequestheader b on b.ID=a.paymentrequestid where b.ISACTIVE=1 AND b.status_pr!='REJECT') 
                GROUP BY policy_no,note_no HAVING round((sum(APPLY_AMT_ORIG) - NET_BALANCE),0) = 0 ORDER BY policy_no ASC";



    }elseif (isset($_POST['periode_1']) && !empty(['periode_1']) && isset($_POST['polisNo']) && !empty($_POST['polisNo'])) {
        $query = "SELECT INTERMEDIARY_CODE, INSURED_CODE, POLICY_NO, INSURED_NAME, NOTE_NO , DBCR_CURRENCY, (CASE WHEN DATEDIFF(PAID_DATE, DUE_DATE) > 0 THEN 'OVERDUE' ELSE 'DUE' END) AS GROUP_TYPE
		, GROSS_PREMIUM ,NET_BALANCE
                FROM stg_pmt_req_datamart
                WHERE $interCode $no_polis $CCY $proPreiod $payDate 
                    AND POLICY_CORP_ID = '".$branchCode."' 
                    AND PAID_DATE IS NOT NULL 
                    AND NOTE_NO NOT IN (SELECT a.NotaNo FROM tpaymentrequestdetail a join tpaymentrequestheader b on b.ID=a.paymentrequestid where b.ISACTIVE=1 AND b.status_pr!='REJECT') 
                GROUP BY policy_no, note_no HAVING round((sum(APPLY_AMT_ORIG) - NET_BALANCE),0) = 0 ORDER BY policy_no ASC";

    }elseif (empty($_POST['periode_1']) && isset($_POST['polisNo']) && !empty($_POST['polisNo'])) {
        $query = "SELECT INTERMEDIARY_CODE, INSURED_CODE, POLICY_NO, INSURED_NAME, NOTE_NO , DBCR_CURRENCY, (CASE WHEN DATEDIFF(PAID_DATE, DUE_DATE) > 0 THEN 'OVERDUE' ELSE 'DUE' END) AS GROUP_TYPE
        , GROSS_PREMIUM ,NET_BALANCE
                FROM stg_pmt_req_datamart
                WHERE $interCode $no_polis $CCY $proPreiod $payDate 
                    AND POLICY_CORP_ID = '".$branchCode."' 
                    AND PAID_DATE IS NOT NULL 
                    AND NOTE_NO NOT IN (SELECT a.NotaNo FROM tpaymentrequestdetail a join tpaymentrequestheader b on b.ID=a.paymentrequestid where b.ISACTIVE=1 AND b.status_pr!='REJECT') 
                GROUP BY policy_no, note_no HAVING round((sum(APPLY_AMT_ORIG) - NET_BALANCE),0) = 0 ORDER BY policy_no ASC";

    }

    if(isset($query))
    {
		
        $data = mysqli_query($conn, $query) or die(mysqli_error($conn));
    }     
    echo "
        <table id='retrive-table' class='table table-bordered table-hover table-striped'>
            <thead>
                <tr>
                    <th><input type='checkbox' id='pilihsemua'></th>
                    <th>Insured Name</th>
                    <th>Intermediary Code</th>
                    <th>Polis No</th>
                    <th>Note No</th>
                    <th>CCY</th>
					<th>Gross</th>
                    <th>GROUP TYPE</th>
                </tr>
            </thead>
            <tbody>";
       while ($retriveCode         =   mysqli_fetch_array($data)) {
         echo "
            <tr class='retrivePilih' >
                <td><input name='checkRetrive[]' value='".$retriveCode['NOTE_NO']."' type=checkbox class='pilih'></td>
                <td>".substr($retriveCode['INSURED_NAME'], 0,35)."</td>
                <td>".$retriveCode['INSURED_CODE']." </td>
                <td>".$retriveCode['POLICY_NO']." </td>
                <td>".$retriveCode['NOTE_NO']." </td>
                <td>".$retriveCode['DBCR_CURRENCY']." </td>
				 <td>".number_format( $retriveCode['GROSS_PREMIUM'] , 2 , '.' , ',' ).	" </td>
                <td>".$retriveCode['GROUP_TYPE']." </td>
            </tr>
            ";
        }
    echo "
            </tbody>
        </table>  
        ";
}
?>

 <script type="text/javascript">
 var tableDataIntermediary;
	$(function () {
	   tableDataIntermediary =  $("#intermediaryCodeTable").DataTable(
		{
		"pageLength": 99,
		 "paging": false,
		 "retrieve": false,					
		 "info": false,
		  "searching":false,
		  "destroy": true
		
		}
		).destroy();
        $("#PayTable").dataTable();
        $("#CurrencyTable").dataTable();
		
		
		
		
	});
	
	
	
	
 </script>