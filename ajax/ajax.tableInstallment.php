<?php session_start();

$table = 'tpaymentrequestheader';

$primaryKey = 'PaymentRequestNo';

$columns = array(
    array( 'db' => 'a.ID',                      'dt' => 0, 'field' => 'ID' ),
    array( 'db' => 'a.PaymentRequestNo',        'dt' => 1, 'field' => 'PaymentRequestNo' ),
    array( 'db' => 'a.IntermediaryName',        'dt' => 2, 'field' => 'IntermediaryName' ),
    array( 'db' => 'a.BeneficiaryName',         'dt' => 3, 'field' => 'BeneficiaryName' ),
    array( 'db' => 'CONCAT_WS(";",a.ID,a.PaymentRequestNo)', 'dt' => 4, 'field' => 'CONCAT_WS(";",a.ID,a.PaymentRequestNo)' ),
    array( 'db' => 'CONCAT_WS(";",a.ID,a.PaymentRequestNo)', 'dt' => 5, 'field' => 'CONCAT_WS(";",a.ID,a.PaymentRequestNo)' ),
    
    
  
                           
);
// SQL server connection information

$sql_details = array(
    'user' => 'root',
    'pass' => 'P@ssw0rd',
    'db'   => 'production_paymentrequest',
    'host' => '192.168.24.33'
);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
// require( 'ssp.class.php' );
require('../datatables/new.ssp.class.php' );
$joinQuery = "FROM tpaymentrequestheader AS a JOIN tpaymentrequestdetail AS b ON b.PaymentRequestID = a.ID JOIN company AS c ON c.company_code = a.intermediaryCode";
$extraWhere = "SUBSTRING(a.PaymentRequestNo,1,2) IN (".$_SESSION['branch_codeStr'].") 
                            AND a.STATUS_PR = 'APPROVE'
                            AND a.STATUS_PAID != 'PAID'
                            AND (a.installment = 'N' OR a.installment= 'R') 
                            AND a.CREATED_BY = '".$_SESSION['username']."'
                            AND c.payment_method = 'TRNF' 
";
$groupBy = "a.PaymentRequestNo";  
$having = "";
// $orderBy = " a.CREATED_DATE DESC";
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
);