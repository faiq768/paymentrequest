<?php session_start();

$table = 'tpaymentrequest_installment';

$primaryKey = 'PaymentRequestNo';

$columns = array(
    array( 'db' => 'th.ID',                      'dt' => 0, 'field' => 'ID' ),
    array( 'db' => 'a.PaymentRequestNo',        'dt' => 1, 'field' => 'PaymentRequestNo' ),
    array( 'db' => 'a.INTERMEDIARY_TYPE',        'dt' => 2, 'field' => 'INTERMEDIARY_TYPE' ),
    array( 'db' => 'a.Client',         'dt' => 3, 'field' => 'Client' ),
    array( 'db' => 'a.STATUS_INS',              'dt' => 4, 'field' => 'STATUS_INS' ),
    array( 'db' => 'CONCAT_WS(";",th.ID,a.PaymentRequestNo)', 'dt' => 5, 'field' => 'CONCAT_WS(";",th.ID,a.PaymentRequestNo)' ),
                           
);
// SQL server connection information

$sql_details = array(
    'user' => 'root',
    'pass' => 'P@ssw0rd',
    'db'   => 'production_paymentrequest',
    'host' => '192.168.24.33'
);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
// require( 'ssp.class.php' );
require('../datatables/new.ssp.class.php' );
$joinQuery = "FROM tpaymentrequest_installment a
JOIN tpaymentrequest_installment_detail dt ON dt.installment_id = a.id
JOIN tpaymentrequestheader th ON th.PaymentRequestNo = a.PaymentRequestNo
JOIN security_user b ON b.username=a.created_by
JOIN (
        SELECT DISTINCT c.username,c.home_branch,c.dept_id,a.id AS group_id 
                    , (CASE a.id 
                            WHEN 2 THEN (SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' AND Value=4) 
                            WHEN 3 THEN (SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' AND Value=1) 
                            WHEN 4 THEN (SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' AND Value=2) 
                            WHEN 5 THEN (SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' AND Value=3) 
                            ELSE '' 
                        END) AS STATUS
        FROM security_group a 
        JOIN security_user_group b on b.group_id=a.id
        JOIN security_user c on c.username=b.username
        WHERE c.ISACTIVE=1 
            AND a.ID in (1,2,3,4,5)
            AND c.username='".$_SESSION['username']."'
        UNION ALL
        SELECT DISTINCT c.username,c.home_branch,c.dept_id,a.id as group_id 
            , (CASE a.id 
                    WHEN 3 THEN (SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' AND Value=8) 
                    ELSE '' 
                END) AS STATUS
        FROM security_group a 
        JOIN security_user_group b on b.group_id=a.id
        JOIN security_user c on c.username=b.username
        WHERE c.ISACTIVE=1 
            AND a.ID in (1,3,4) 
            AND c.username='".$_SESSION['username']."'
) c ON (
            (
                c.group_id in (2) 
                AND a.created_by=c.username
            )
            OR
            (
                c.STATUS = a.STATUS_INS 
                AND c.group_id in (3)                                
                AND a.CREATED_BY!=c.username
            )
             OR
            (
                c.STATUS = a.STATUS_INS 
                AND c.group_id in (4) 
            
                AND a.CREATED_BY!=c.username
            )
            OR
            (
                c.STATUS = a.STATUS_INS 
                AND c.group_id in (4) 
             
                AND a.CREATED_BY!=c.username
            )           
        ) ";
$extraWhere = " 1=1 and a.INTERMEDIARY_TYPE in (".$_SESSION['intermediary_typeStr'].") AND SUBSTRING(a.PaymentRequestNo,1,2) IN (".$_SESSION['branch_codeStr'].")";
// if ($_SESSION['group_id'] == 2) {
//     $extraWhere = "SUBSTRING(a.PaymentRequestNo,1,2) IN (".$_SESSION['branch_codeStr'].") 
//                             AND a.STATUS_INS !='CREATE' 
//                             AND a.CREATED_BY = '".$_SESSION['username']."'
//     ";
// }elseif ($_SESSION['group_id'] == 3) {
//     $extraWhere = "SUBSTRING(a.PaymentRequestNo,1,2) IN (".$_SESSION['branch_codeStr'].") 
//                             AND a.STATUS_INS !='CREATE' 
//     ";
// }elseif ($_SESSION['group_id'] == 4) {
//     $extraWhere = "SUBSTRING(a.PaymentRequestNo,1,2) IN (".$_SESSION['branch_codeStr'].") 
//                             AND a.STATUS_INS !='CREATE' 
//                             AND a.CREATED_BY = '".$_SESSION['username']."'
//     ";
// }

$groupBy = "a.PaymentRequestNo";  
$having = "";
// $orderBy = " a.CREATED_DATE DESC";
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
);