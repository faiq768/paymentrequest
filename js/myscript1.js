
$(document).ready(function(){

  $("#myformData").on("submit", function(){
    $("#pageloader").fadeIn();
  });


  var table = $('#dataPolicy').DataTable( { 
         "processing": true,
         "serverSide": true,
         "retrieve": true,
         "ajax": "ajax/ajax.polis.php"
    });


// ==================== KETIKA LOOKUP POLICY DIKLIK ======================================

$('#polis').click(function(){

    table.destroy();
      
      table = $('#dataPolicy').DataTable( { 
           "processing": true,
           "serverSide": true,
           "retrieve": true,
           "ajax": "ajax/ajax.polis.php"
      });

      $('#dataPolicy tbody').on( 'click', 'td', function () {
       var dataP = table.row( $(this).parents('tr') ).data();

          document.getElementById("pnt").value = dataP[0];
          document.getElementById("noteno").value = dataP[1];
          document.getElementById("branch_code").value = dataP[6];
          document.getElementById("branch_name").value = dataP[8];
          document.getElementById("type").value = dataP[4];
          document.getElementById("code").value = dataP[2];
          document.getElementById("name").value = dataP[3];
          document.getElementById("bussines").value = dataP[5];

          var IntermediaryTypeCode = dataP[2].charAt(0);
          document.getElementById("typecode").value = IntermediaryTypeCode;

           var INTCODE = dataP[2];
           $.ajax({
                  type  :   'POST',
                  url   :   'ajax/ajax_payment_method.php',
                  data  :   'INTCODE='+INTCODE,
                  success : function(response){
                     //$('#mop').val(response);
                    var mop = response.trim();
          // console.log('['+mop+']');
                    if (mop === 'CASH') {
                      $('#mop').val('CASH');
                      $('.pay').css({"display" : "none"});
                    }else if(mop === 'CHEQ'){
                        $('#mop').val('CHEQ');
                                  $('.pay').css({"display" : "none"});
                      }else{
                      $('#mop').val('TRANSFER');
                      $('.pay').css({"display" : "inline-block"});
                    }
                  },
                  beforeSend : function(){
                    $('#mop').val('LOADING...');
                  }
                })
          
           $('#lookup-polis').modal('hide');

      } );

  });


  // });

//TOMBOL TAMBAH POLIS RETRIVE KETIKA EDIT

$('#polis-add-retrive, .in').click(function(){
  $("#output, #EDIT_TABLE_ADD tbody").load(" #output > *");
  $('#EDIT_TABLE_ADD, #label').css({'display' : 'none'});

  
  
});


$('#financeCurrency').click(function(){
    table.destroy();
      
      table = $('#tableLookupCurrencyFinance').DataTable( { 
           "processing": true,
           "serverSide": true,
           "retrieve": true,
           "ajax": "ajax/ajax_finance_currency.php"
      });

      $('#tableLookupCurrencyFinance tbody').on( 'click', 'td', function () {
       var dataP = table.row( $(this).parents('tr') ).data();

          document.getElementById("icodeCurrency").value = dataP[0];
          document.getElementById("inameCurrency").value = dataP[1];
          
          $('#currencyFinance').modal('hide');
      });
});

// ======================================= TOMBOL PREVIEW =====================================
 $('#previewID').click(function() {
      
      $('.previewClass').css({"display":"block"});
      $.ajax({
        type: 'POST',
        url: $('#myformData').attr('preview'),
        data: $('#myformData').serialize(),
        success: function(data) {
          $('.preview').html(data);
        },
        beforeSend : function(){
          $('.preview').html('LOADING... <br> <img src="img/loading.gif">');
        }
      })
  
    });
    $('#close_priview').click(function(){
      $('.previewClass').css({"display":"none"});
    });


  $('#tcode').click(function(){
      if ($('#type').val() == '') {
        swal("Intermediary Type is Empty!!!", "", "error");
        return false;
      }
      //var interType = $('#typecode').val();

      table.destroy();
      
      table = $('#dataIntermediaryCode').DataTable( { 
           "processing": true,
           "serverSide": true,
           "retrieve": true,
           "ajax": "ajax/ajax.intermediary.php?id="+$('#type').val()
           
      });

      $('#dataIntermediaryCode tbody').on( 'click', 'td', function () {
          var data = table.row( $(this).parents('tr') ).data();
          document.getElementById("code").value = data[0];
          document.getElementById("name").value = data[1];

           var INTCODE = data[0];
           $.ajax({
                  type  :   'POST',
                  url   :   'ajax/ajax_payment_method.php',
                  data  :   'INTCODE='+INTCODE,
                  success : function(response){
                     //$('#mop').val(response);
                    var mop = response.trim();
           //console.log('['+mop+']');
                    if (mop === 'CASH') {
                      $('#mop').val('CASH');
                      $('.pay').css({"display" : "none"});
                    }else if(mop === 'CHEQ'){
            $('#mop').val('CHEQ');
                      $('.pay').css({"display" : "none"});
          }else{
                      $('#mop').val('TRANSFER');
                      $('.pay').css({"display" : "inline-block"});
                    }
                  },
                  beforeSend : function(){
                    $('#mop').val('LOADING...');
                   
                  }
            })

          document.getElementById("ptc").value = '';
          document.getElementById("bename").value = '';
          document.getElementById("bank").value = '';
          document.getElementById("account").value = '';
          document.getElementById("branch").value = '';
          document.getElementById("cc").value = '';

          

          $('#intermediaryCode').modal('hide');

      } );
    
  });
  // =========================================== Ketika Lookup Currency pertama diklik ===========================
  $('#Curr-button').click(function(){
       
     var C_TRUE = 'TRUE';

    $.ajax({
      type  :   'POST',
      url   :   'ajax/ajax_type.php',
      data  :   'currency='+C_TRUE,
      success : function(response){
        $('#data-currency-first').html(response);
      },
      beforeSend : function(){
        $('#data-currency-first').html('LOADING... <br> <img src="img/loading.gif">');
       
      }
    })
  });

  // =======================================  ketika lookup currency KEDUA diklik ============================
  $('#tccy').click(function(){
    if ($('#code').val() == '') {
      swal("Intermediary Code is Empty!!!", "", "error");
      return false;
    }
   
    var intcode = $('#code').val();
     var cINTCODE = intcode.trim();

    $.ajax({
      type  :   'POST',
      url   :   'ajax/ajax_type.php',
      data  :   'id_code='+cINTCODE,
      success : function(response){
        $('#data-currency').html(response);
      },
      beforeSend : function(){
        $('#data-currency').html('LOADING... <br> <img src="img/loading.gif">');
       
      }
    })
  });

  // =======================================  ketika lookup Pay To diklik ============================
  $('.pay').click(function(){
    if ($('#code').val() == '') {
      swal("Intermediary Code is Empty!!!", "", "error");
      return false;
    }
    else if ($('#ccy').val() == '') {
      swal("Currencys is Empty!!!", "", "error");
      return false;
    };

    var codeC = $('#code').val();
    var c = codeC.trim();
    var cry = $('#ccy').val();
    var subCcy = cry.trim();
    $.ajax({
          type  :   'POST',
          url   :   'ajax/ajax_type.php',
          data  :   'cCode='+c+'&cCurr='+subCcy,
          dataType : 'html',
          success : function(response){
            $('#pCode').html(response);
          },
          beforeSend : function(){
            $('#pCode').html('LOADING... <br> <img src="img/loading.gif">');
           
          }
        });
  });

  // =======================================  ketika lookup retrive diklik ============================
  $('.r-trive').click(function(){
	 
	if($('#branch_code').val() == ''){
     swal("Branch Code is Empty!!!", "", "error");
      return false; 
	}
	
	
	
	  
	if ($('#code').val() == '') {
      swal("Intermediary Code is Empty!!!", "", "error");
      return false;
    }else if ($('#period-1').val() == '' && $('#pnt').val() == '') {
      swal("Check The Production Period / Policy Note Type!!!", "", "error");
      return false;
    };
      
    var inCode = $('#code').val();
    var conv_inCode = inCode.trim();
	
	
	
    var Lob = $('#bussines').val();
    var Curr = $('#Curr').val();
	
	
    var periode1 = $('#period-1').val();
    var periode2 = $('#period-2').val();
    var payment1 = $('#payment-1').val();
    var payment2 = $('#payment-2').val();
    var polisNo  = $('#pnt').val();
	var branchCode = $('#branch_code').val();
	
    $.ajax({
      type  :   'POST',
      url   :   'ajax/ajax_type.php',
      data  :   'inCode='+conv_inCode+'&Lob='+Lob+'&periode_1='+periode1+'&Curren='+Curr+
                '&periode_2='+periode2+'&polisNo='+polisNo+'&payment_1='+payment1+ '&branch_code=' + branchCode + 
                '&payment_2='+payment2,
      dataType : 'html',
      success : function(response){
        $('.data').html(response);
        var detailDataPolis = $('#retrive-table').DataTable({
              "scrollY":        "350px",
              "scrollCollapse": true,
              "paging":         false
          });
        $("#pilihsemua").click(function () { 
          $('.pilih').attr('checked', this.checked); 
        });
         
        $(".pilih").click(function(){
          if($(".pilih").length == $(".pilih:checked").length) {
            $("#pilihsemua").attr("checked", "checked");
          } else {
            $("#pilihsemua").removeAttr("checked");
          }
        });
		
		//jumlah
		//alert('jumlah ' + detailDataPolis.data().count());
		//alert('jumlasssh ' + $("#pnt").val());
		
		$("#btn-save-data-polis").attr("disabled", false);
		if(detailDataPolis.data().count() == 0 && $("#pnt").val() == '' ){
			
			$("#btn-save-data-polis").attr("disabled", true);
			
		}
		
		
        $('.retrive').fadeIn();
      },
        beforeSend : function(){
          $('.retrive').fadeIn();
          $('.data').html('LOADING... <br> <img src="img/loading.gif">');
        }
      });
  });
  $('.cancel').click(function(){
      $('.retrive').fadeOut();
      $('body').css({'overflow' : 'auto'});
      $("#pilihsemua").removeAttr("checked");
      $("#pilih").removeAttr("checked");
      $('.r-trive').show();
      $('.btn-show').hide();
  });
  $('.ok').click(function(){
	  $('.retrive').fadeOut();
      $('body').css({'overflow' : 'auto'});
      $('.r-trive').hide();
      $('.btn-show').show();
  });
  $('.btn-show').click(function(){
	 
      $('.retrive').fadeIn();
     
  });
  $('#ccy').focus(function(){
    if ($('#code').val()=='') {
      swal("Intermediary Code is Empty!!!", "", "error");
      return false;
    }
  });
  // $('#polis').click(function(){
  //   $.ajax({
  //       type  :   'POST',
  //       url   :   'ajax/polis.php',
  //       success : function(response){
  //         $('#data-policy').html(response);
  //       },
  //       beforeSend : function(){
  //         $('#data-policy').html('LOADING... <br> <img src="img/loading.gif">');
         
  //       }
  //     })
  // });

  //$('.btn-save-1').click(function(){
    // if($(".pilih:checked").length == 0){
    //   swal("Check Your Detail Data Entry !", "", "error");
    //   return false;
    // }else if ($('#code').val()=='' || $('#rate').val()=='' ) {
    //   if($('#mop').val()=='TRANSFER'){
    //     if ($('#ptc').val() == '') {
    //        swal("Check Your Data Entry mop!", "", "error");
    //         return false;
    //     };
    //   };
    // };
  //});
  $('.btn-save-2').click(function(){
    if($(".pilih:checked").length == 0){
		alert('ddoor');
      swal("Check Your Detail Data Entry !", "", "error");
      return false;
    }
  });
  
  $('.btn-save-3').click(function(){
	
	if($('#iname').val() == ''){
		swal("Check Your Detail Data Pay To !", "", "error");
		return false;
	}
	
	if($('#pntc').val() == ''){
		swal("Check Your Detail Data Client No !", "", "error");
		return false;
	}
	
	if($('#bn').val() == ''){
		swal("Check Your Detail Data Beneficiary Name !", "", "error");
		return false;
	}
	
	if($('#bank').val() == ''){
		swal("Check Your Detail Data Bank !", "", "error");
		return false;
	}
	
	if($('#an').val() == ''){
		swal("Check Your Detail Data Account No !", "", "error");
		return false;
	}
	
	
	//return false;
	
  });

  $('.btn-notif').click(function(){
    $('.notif').toggle('slow');
  });
 

});
