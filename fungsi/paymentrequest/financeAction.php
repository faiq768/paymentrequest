<?php 
session_start();
include "../../koneksi/koneksi.php";


if (isset($_GET['id']) && isset($_GET['VoucherNo']) && isset($_GET['VoucherDate']) ) 
{
	try
	{
		$id 		=	$_GET['id'];
		$VoucherNo 		= 	$_GET['VoucherNo'];
		$VoucherDate 	= 	$_GET['VoucherDate'];

		$queryVal = "SELECT * FROM tpaymentrequestheader WHERE VoucherNo='$VoucherNo' ";
		$dataVal = mysqli_query($conn, $queryVal) or die(mysqli_error($conn));
		$dataRow    = $dataVal->num_rows;
		if($dataRow > 0)
		{
			echo "<script>swal('THE PAYMENT REQUEST ALREADY EXISTS', '$VoucherNo', 'success'); </script>";
			//echo "<script>return confirm( '$voucherNo Already Exists. Are you sure you want to continue ?' ); </script>";
			//$_SESSION['notif'] = 'HOPAIDFAILED';
			//exit();
		}

		
		$date=date_create($VoucherDate);
		$date2 = date_format($date,"Y-m-d");
			
		// Set autocommit to off
		mysqli_autocommit($conn, FALSE);
		mysqli_begin_transaction($conn, MYSQLI_TRANS_START_READ_WRITE);

		$queryPayReq 	= "UPDATE tpaymentrequestheader SET VoucherNo='$VoucherNo',VoucherDate='$date2', PAY_PAID_DATE=now(), MOD_DATE=now(), MOD_BY = '".$_SESSION['username']."', PAY_PAID_BY = '".$_SESSION['username']."' , STATUS_PAID = (SELECT Description FROM mgeneral_table WHERE Code='PaymentRequest_STATUS' and Value=7) WHERE ID='$id'; ";
		$queryHistory 	= "INSERT INTO thistory_approval values('PAYMENT_REQUEST', $id, (SELECT Description FROM mgeneral_table WHERE Code='PaymentRequest_STATUS' AND Value=7),NOW(),'".$_SESSION['username']."',5,'')";

		$dataPayReq = mysqli_query($conn, $queryPayReq) or die(mysqli_error($conn));
		$dataHistory = mysqli_query($conn, $queryHistory) or die(mysqli_error($conn));

		$queryPayReq;

		// Commit transaction
		mysqli_commit($conn);
		$_SESSION['notif'] = 'HOPAID';
		
	}
	catch(exception $e)
	{
		// Rollback transaction
		mysqli_rollback($conn);
		
		$_SESSION['notif'] = 'HOPAIDFAILED';
	}
	header("location:../../home");
}
else if (isset($_GET['PayReqNo']) && isset($_GET['VoucherNo']) && isset($_GET['VoucherDate']) ) 
{
	try
	{
		$PayReqNo 		=	$_GET['PayReqNo'];
		$VoucherNo 		= 	$_GET['VoucherNo'];
		$VoucherDate 	= 	$_GET['VoucherDate'];

		$queryVal = "SELECT * FROM tpaymentrequestheader WHERE VoucherNo='$VoucherNo' ";
		$dataVal = mysqli_query($conn, $queryVal) or die(mysqli_error($conn));
		$dataRow    = $dataVal->num_rows;
		if($dataRow > 0)
		{
			echo "<script>return confirm( '$voucherNo Already Exists. Are you sure you want to continue ?' ); </script>";
			//exit();
		}

		$date=date_create($VoucherDate);
		$date2 = date_format($date,"Y-m-d");
			
		// Set autocommit to off
		mysqli_autocommit($conn, FALSE);
		mysqli_begin_transaction($conn, MYSQLI_TRANS_START_READ_WRITE);

		$queryPayReq 	= "UPDATE tpaymentrequestheader SET VoucherNo='$VoucherNo',VoucherDate='$date2', PAY_PAID_DATE=now(), MOD_DATE=now(), MOD_BY = '".$_SESSION['username']."', PAY_PAID_BY = '".$_SESSION['username']."' , STATUS_PAID = (SELECT Description FROM mgeneral_table WHERE Code='PaymentRequest_STATUS' and Value=7) WHERE PaymentRequestNo='$PayReqNo'; ";
		$queryHistory 	= "INSERT INTO thistory_approval values('PAYMENT_REQUEST', $id, (SELECT Description FROM mgeneral_table WHERE Code='PaymentRequest_STATUS' AND Value=7),NOW(),'".$_SESSION['username']."',5,'')";

		$dataPayReq = mysqli_query($conn, $queryPayReq) or die(mysqli_error($conn));
		$dataHistory = mysqli_query($conn, $queryHistory) or die(mysqli_error($conn));

		$queryPayReq;

		// Commit transaction
		mysqli_commit($conn);
		$_SESSION['notif'] = 'HOPAID';
		
	}
	catch(exception $e)
	{
		// Rollback transaction
		mysqli_rollback($conn);
		
		$_SESSION['notif'] = 'HOPAIDFAILED';
	}
	header("location:../../home");
}
else
{
	$_SESSION['notif'] = 'HOPAIDFAILED';
	header("location:../../home");
}
?>