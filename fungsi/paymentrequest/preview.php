<?php
include "../../koneksi/koneksi.php";
if (isset($_POST['checkRetrive'])) {
?> 
<p>Kepada Yth,</p>
<p>Bagian Branch Co / Bagian Finance</p>
<br>
<p>Mohon diberikan persetujuan untuk pembayaran sebagai berikut :</p>
<br>
<table class="form">
	<tr>
		<td>PAY TO / CLIENT NO</td>
		<td>:</td>
		<td> <?php echo $_POST['bn']." / ".$_POST['ptcn']; ?>  </td>
	</tr>
	<tr>
		<td>BENEFICIARY NAME</td>
		<td>:</td>
		<td><?php echo $_POST['bn']; ?></td>
	</tr>
	<tr>
		<td>BANK</td>
		<td>:</td>
		<td><?php echo $_POST['bank']; ?></td>
	</tr>
	<tr>
		<td>DESTINATION ACCOUNT NUMBER</td>
		<td>:</td>
		<td><?php echo $_POST['an']; ?></td>
	</tr>
	<tr>
		<td>BRANCH </td>
		<td>:</td>
		<td><?php echo $_POST['branch']; ?></td>
	</tr>
	<tr>
		<td>CITY</td>
		<td>:</td>
		<td><?php echo $_POST['cc']; ?></td>
	</tr>
	<tr>
		<td>PERSENTASE FEE</td>
		<td>:</td>
		<td><?php echo $_POST['rate']; ?></td>
	</tr>
	<tr>
		<td>TYPE OF PAYMENT</td>
		<td>:</td>
		<td><?php echo $_POST['top']; ?></td>
	</tr>
	<tr>
		<td style="vertical-align: top">Memo</td>
		<td style="vertical-align: top">:</td>
		<td width="800px"><?php echo $_POST['nmemo']; ?></td>
	</tr>
</table>
<br>
<table border="1" width="1200px">
	<thead>
		<tr>
			<th rowspan="2">NO</th>
			<th rowspan="2" style="width: 150px;">POLIS NUMBER</th>
			<th rowspan="2" style="width: 30px;">CORR</th>
			<th rowspan="2" style="width: 120px;">INSURED NAME</th>
			<th colspan="6">SETTLEMENT REFFERENCE</th>
			<th rowspan="2" style="width: 20px;">CCY</th>
			<th rowspan="2" style="width: 80px;">GROSS PREMIUM</th>
			<th rowspan="2" style="width: 80px;">AMOUNT</th>
		</tr>
		<tr>
			<th style="width: 120px;">NOTE NO</th>
			<th style="width: 30px;">STATUS</th>
			<th style="width: 70px;">DATE</th>
			<th style="width: 100px;">BANK</th>
			<th style="width: 100px;">ACCOUNT</th>
			<th style="width: 100px;">VOUCHER</th>
		</tr>
	</thead>
	<tbody>

<?php
$number = 1;
	$pilih = $_POST['checkRetrive'];
	    foreach ( $pilih AS $retrive)
		{
			$noteno =	$retrive;
			$sql = "SELECT POLICY_NO, INSURED_CODE, INSURED_NAME, group_concat(date_format(`TRANS_DATE`,'%d-%m-%Y')) AS `TRANS_DATE`, group_concat(`BANK_NAME`) AS `BANK_NAME`, group_concat(`ACCOUNT_NUMBER`) AS `ACCOUNT_NO`, group_concat(`STTL_NO`) AS `STTL_NO`, DBCR_CURRENCY, GROSS_PREMIUM  from stg_pmt_req_datamart where NOTE_NO = '$noteno'";
			$query = mysqli_query($conn, $sql);
			$fetch_query = mysqli_fetch_array($query);
			$amount 		=	$fetch_query['GROSS_PREMIUM'] * $_POST['rate'] / 100;
		?>
		<tr>
			<td width="25px" align="center"><?php echo $number; ?></td>
			<td><?php echo $fetch_query['POLICY_NO']; ?></td>
			<td align="center"><?php echo $fetch_query['INSURED_CODE']; ?></td>
			<td ><?php echo $fetch_query['INSURED_NAME']; ?></td>
			<td><?php echo $noteno; ?></td>
			<td align="center">PAID</td>
			<td align="left"><?php echo $fetch_query['TRANS_DATE']; ?></td>
			<td align="left"><?php echo $fetch_query['BANK_NAME']; ?></td>
			<td align="left"><?php echo $fetch_query['ACCOUNT_NO']; ?></td>
			<td align="center"><?php echo $fetch_query['STTL_NO']; ?></td>
			<td align="center"><?php echo $fetch_query['DBCR_CURRENCY']; ?></td>
			<td align="right"><?php echo number_format( $fetch_query['GROSS_PREMIUM'] , 2 , '.' , ',' );?></td>
			<td align="right"><?php echo number_format( $amount , 2 , '.' , ',' ); ?></td>
		</tr>
	
		<?php $number++;} ?>
		<!-- <tr class="garis">
			<td colspan="11" align="right" style="padding-right:10px;">TOTAL  <?php echo $CurrTable['CCY'] ?></td>
			<td align="right"><?php echo number_format( $fetchCCYAmout2['total'] , 2 , '.' , ',' ); ?></td>
			<td align="right"><?php echo number_format( $fetchCCYAmout['total'] , 2 , '.' , ',' ); ?></td>
		</tr> -->
	</tbody>
</table>
<br>
<?php
} else {
	echo "<h1 style='color:red;'>Retrive is empty</h1>";
}
?>