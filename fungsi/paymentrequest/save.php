<?php 
/*
** save 						**
** Modified By Hockey        	**
** Modified Date 20180309	 	**
** SAVE 						**
** RE-SUBMIT 					**
** START 						**
*/ 

/*
** save 						**
** Modified By FAIQ        		**
** Modified Date 20181024	 	**
** SAVE QUERY					**
** RE-SUBMIT QUERY				**
** START 						**
*/ 

session_start();
include "../../koneksi/koneksi.php";
//For Maker

if(!isset($_SESSION['username']) || $_SESSION['username'] ==''){
	header('location:../index.php');
	exit(); 
}
	if (isset($_POST['save'])) {
		$flagCountRollback = 0;
		try
		{
			$user				=	$_SESSION['username'];
			//$branch_user		=	$_SESSION['branch_code'];
			$branch_user		=	$_POST['branch_code'];
			//echo "BRANCH CUER ".$branch_user;
			//die();
			
			$sobCode 			=	$_POST['typecode'];
			$intermediary_type 	= 	$_POST['intertype'] ; 	
			$intermediary_code 	= 	$_POST['intercode'] ; 	
			$intermediary_name	=	$_POST['intername'] ; 	
			$class_of_business	=	$_POST['cob'] ; 		
			$currency			=	$_POST['ccy']; 			
			$policy_no			=	$_POST['pnt'] ; 		
			$note_no			=	$_POST['noteno'] ; 		
			$rate				=	$_POST['rate']; 		
			$type_of_payment	=	$_POST['top'] ; 		
			$method_of_payment	=	$_POST['mop'] ; 		
			$pay_to				=	$_POST['ptcn'] ; 		
			$beneficiary		=	$_POST['bn'] ; 			
			$bank				=	$_POST['bank']; 		
			$account_number		=	$_POST['an'] ; 			
			$branch				=	$_POST['branch'] ; 		
			$city				=	$_POST['cc'] ; 			
			$top				=	$_POST['top'] ; 		
			$memo				=	$_POST['nmemo'] ; 	
			$Start_Periode		=	$_POST['periode1'];
			$End_Periode 		=	$_POST['periode2'];	

			$_Start=date_create($Start_Periode);
			$_START_PERIODE = date_format($_Start,"Y-m-d");

			$_End=date_create($End_Periode);
			$_End_PERIODE = date_format($_End,"Y-m-d");

			$Memo = str_replace("'", "''", $memo);

			if ($currency == '') {
				$beneficiary = $_POST['intername'];
			}

			$pilih = $_POST['checkRetrive'];
			
			//echo "AAAA ".sizeof($pilih);
			//die();


			// Set autocommit to off
			mysqli_autocommit($conn, FALSE);
			mysqli_begin_transaction($conn, MYSQLI_TRANS_START_READ_WRITE);

			$run_function 			=	mysqli_query($conn, "SELECT fgenerateno($branch_user)");
			$function 				=	mysqli_query($conn, "SELECT * FROM mgenerate_no where CodeBranch = '$branch_user' ");
			$function_row 			=	mysqli_fetch_array($function);
			$generate_no		= 	$function_row['CodeBranch'].'-'.$function_row['Nyear'].'-'.$function_row['Nmonth'].'-'.$function_row['Number'];

			
			$sqlInsertHeader = "INSERT INTO tpaymentrequestheader (intermediary_type_code,  branch_code, PaymentRequestNo, IntermediaryType, IntermediaryCode, IntermediaryName, Rate, PayTo, BeneficiaryName, Bank, DestinationAccountNumber, Branch, City, STATUS_PR, TypeOfPayment, CREATED_BY, Memo, Start_Periode, End_Periode)
				VALUES('$sobCode', '$branch_user', '$generate_no', '$intermediary_type', '$intermediary_code', '$intermediary_name', '$rate', '$pay_to','$beneficiary','$bank','$account_number','$branch','$city',(SELECT Description FROM mgeneral_table WHERE Code='PaymentRequest_STATUS' and Value=1),'$top','$user','$memo','$_START_PERIODE','$_End_PERIODE')";
				
			$savePayReqHeader		=	mysqli_query($conn, $sqlInsertHeader) or die(mysqli_error($conn));

			if ($savePayReqHeader) {
			    $last_id = mysqli_insert_id($conn);
			    //echo "New record created successfully. Last inserted ID is: " . $last_id;

			    //Insert History
			    $sqlInserHistory = "INSERT INTO thistory_approval(REQUEST_TYPE, REQUEST_ID, ACTION, CREATED_BY, GROUP_ID, REMARKS) 
									VALUES('PAYMENT_REQUEST','$last_id',(SELECT Description FROM mgeneral_table WHERE Code='PaymentRequest_STATUS' AND Value=1),'".$_SESSION['username']."', 2, '')";
				$History 	=	mysqli_query($conn, $sqlInserHistory) or die(mysqli_error($conn));

			    //Insert Detail Data
			    //Looping data from retrieve
			    if(sizeof($pilih) == 0)
			    {
			    	$sqlCheckPolis = "SELECT POLICY_NO, INSURED_CODE, INSURED_NAME_FULL as INSURED_NAME, group_concat(date_format(`TRANS_DATE`,'%d-%m-%Y')) AS `TRANS_DATE`, group_concat(`BANK_NAME`) AS `BANK_NAME`, group_concat(`ACCOUNT_NUMBER`) AS `ACCOUNT_NO`, group_concat(`STTL_NO`) AS `STTL_NO`, DBCR_CURRENCY, GROSS_PREMIUM  from stg_pmt_req_datamart where NOTE_NO = '$note_no'";
					$cekpolis	=	mysqli_query($conn, $sqlCheckPolis) or die(mysqli_error($conn));

					$dataRow    = $cekpolis->num_rows;

					if($dataRow == 0)
					{
						$flagCountRollback++;
						// Rollback transaction
						mysqli_rollback($conn);
					 	exit();
					}
					else 
					{
						while ($fetchPolis 	= mysqli_fetch_array($cekpolis)) 
						{
							$amount 		=	$fetchPolis['GROSS_PREMIUM'] * $rate / 100;
							$sqlInserDetail = "INSERT INTO tpaymentrequestdetail(PaymentRequestID, PolisNo, NotaNo, Corr, InsuredName, StatusSettlement, DateSettlement, BankSettlement, AccountSettlement, VoucherSettlement, CCY, GrossPremium, Amount) 
												VALUES('$last_id','".$fetchPolis['POLICY_NO']."','$note_no','".$fetchPolis['INSURED_CODE']."',
													'".$fetchPolis['INSURED_NAME']."','PAID','".$fetchPolis['TRANS_DATE']."','".$fetchPolis['BANK_NAME']."','".$fetchPolis['ACCOUNT_NO']."','".$fetchPolis['STTL_NO']."',
													'".$fetchPolis['DBCR_CURRENCY']."','".$fetchPolis['GROSS_PREMIUM']."','$amount')";		
						
							$savePayReqDetail	=	mysqli_query($conn, $sqlInserDetail);
						}
					}
			    }
			    else
			    {
			 	    $pilih = $_POST['checkRetrive'];

			 	    if(!empty($pilih))
			 	    {
					    foreach ( $pilih AS $retrive)
						{
							$noteno =	$retrive;
							
							$sqlCheckPolis = "SELECT POLICY_NO, INSURED_CODE, INSURED_NAME_FULL AS INSURED_NAME, group_concat(date_format(`TRANS_DATE`,'%d-%m-%Y')) AS `TRANS_DATE`, group_concat(`BANK_NAME`) AS `BANK_NAME`, group_concat(`ACCOUNT_NUMBER`) AS `ACCOUNT_NO`, group_concat(`STTL_NO`) AS `STTL_NO`, DBCR_CURRENCY, GROSS_PREMIUM  from stg_pmt_req_datamart where NOTE_NO = '$noteno'";
							$cekpolis	=	mysqli_query($conn, $sqlCheckPolis) or die(mysqli_error($conn));
							$dataRow    = $cekpolis->num_rows;
							
							if($dataRow == 0)
							{
								$flagCountRollback++;
								// Rollback transaction
								mysqli_rollback($conn);
							 	exit();
							}
							else 
							{
								
								while ($fetchPolis	= mysqli_fetch_array($cekpolis)) 
								{
									$InsuredName = str_replace("'", "''", $fetchPolis['INSURED_NAME']);
									
									$fetchPolis['POLICY_NO']; echo "<br>";
									//echo $fetchPolis['POLICY_NO']; echo "<br>";
									$amount 		=	$fetchPolis['GROSS_PREMIUM'] * $rate / 100;
									$sqlInserDetail = "INSERT INTO tpaymentrequestdetail(PaymentRequestID, PolisNo, NotaNo, Corr, InsuredName, StatusSettlement, DateSettlement, BankSettlement, AccountSettlement, VoucherSettlement, CCY, GrossPremium, Amount) 
														VALUES('$last_id','".$fetchPolis['POLICY_NO']."','$noteno','".$fetchPolis['INSURED_CODE']."',
															'$InsuredName','PAID','".$fetchPolis['TRANS_DATE']."','".$fetchPolis['BANK_NAME']."','".$fetchPolis['ACCOUNT_NO']."','".$fetchPolis['STTL_NO']."',
															'".$fetchPolis['DBCR_CURRENCY']."','".$fetchPolis['GROSS_PREMIUM']."','$amount')";		
								
									$savePayReqDetail	=	mysqli_query($conn, $sqlInserDetail);
								}
					 		}
					 	}
			 		}
			 	    else
			 	    {
			 	    	$flagCountRollback++;
			 	    	// Rollback transaction
						mysqli_rollback($conn);
					 	exit();
			 		}
			 	}

				// Commit transaction
				mysqli_commit($conn);
			} 
			else {
			    //echo "Error: " . $sql . "<br>" . mysqli_error($conn);
			    echo "Error save data <br> Please contact your Administrator";
			    
			    $flagCountRollback++;
			    // Rollback transaction
				mysqli_rollback($conn);
				exit();
			}

			if($flagCountRollback == 0)
			{
				$_SESSION['notif'] = 'PAYREQSUBMIT';
				$_SESSION['paymentnumber'] = $generate_no;
				header("location:../../payreqnew");
			}
		}
		catch(exception $e)
		{
			// Rollback transaction
			mysqli_rollback($conn);

			$_SESSION['notif'] = 'PAYREQSUBMITFAILED';
			header('location:../../');
		}
		finally
		{
			// close connection
			mysqli_close($conn);
		}
		
	}
	elseif (isset($_POST['re_submit'])) {
		try
		{
			$id 					=	$_POST['id'];
			$user					=	$_SESSION['username'];
			$rate					=	$_POST['rate']; 		
			$pay_to					=	$_POST['pntc'] ; 		
			$beneficiary			=	$_POST['bn'] ; 			
			$bank					=	$_POST['bank']; 		
		 	$account_number			=	$_POST['an'] ; 			
			$branch					=	$_POST['branch'] ; 		
			$city					=	$_POST['cc'] ; 			
			$type_of_payment 		=	$_POST['top'];
			$Memo 					= 	$_POST['nmemo'];
			
			$Memo = str_replace("'", "''", $Memo);

			// Set autocommit to off
			//mysqli_autocommit($conn,FALSE);	
			//mysqli_begin_transaction($conn, MYSQLI_TRANS_START_READ_WRITE);

			//Update Header
			$queryHeader = "UPDATE tpaymentrequestheader 
							SET Rate='$rate'
								, PayTo='$pay_to'
								, BeneficiaryName='$beneficiary'
								, Bank='$bank'
								, DestinationAccountNumber='$account_number'
								, Branch='$branch'
								, City='$city'
								, STATUS_PR=(SELECT Description FROM mgeneral_table where Code='PaymentRequest_STATUS' AND Value=6)
								, APPROVAL_BY=''
								, APPROVAL_DATE = NULL
								, CHECKER_BY=''
								, CHECKER_DATE = NULL
								, MOD_DATE=NOW()
								, MOD_BY='$user'
								, TypeOfPayment='$type_of_payment' 
								, Memo='$Memo'
							WHERE ID = '$id' ";
			$updateheaderQuery		=	mysqli_query($conn,$queryHeader) or die(mysqli_error($conn));
			
		
			//History
			$queryHistory = "INSERT INTO thistory_approval 
							values('PAYMENT_REQUEST'
								, '$id'
								, (SELECT Description FROM mgeneral_table WHERE Code='PaymentRequest_STATUS' AND Value=6)
								, NOW()
								, '".$_SESSION['username']."'
								, 1
								, '')";
			$History 	=	mysqli_query($conn, $queryHistory) or die(mysqli_error($conn));
			
			$deleteDetail			=	mysqli_query($conn, "DELETE FROM tpaymentrequestdetail where PaymentRequestID = '$id' ");
			
			$cek = $_POST['retriveupdate'];
			// // $nomor = 1;
			foreach ( $cek AS $retrive_update)
			{
				//echo $retrive_update;
				$NotaNo =	$retrive_update;
				$sqlCheckPolis = "SELECT POLICY_NO,
									INSURED_CODE, 
									INSURED_NAME_FULL AS INSURED_NAME, 
									group_concat(date_format(`TRANS_DATE`,'%d-%m-%Y')) AS `TRANS_DATE`, 
									group_concat(`BANK_NAME`) AS `BANK_NAME`, 
									group_concat(`ACCOUNT_NUMBER`) AS `ACCOUNT_NO`, 
									group_concat(`STTL_NO`) AS STTL_NO, 
									DBCR_CURRENCY, 
									GROSS_PREMIUM  
									from stg_pmt_req_datamart where NOTE_NO = '$NotaNo' GROUP BY POLICY_NO";
									$cekpolis	=	mysqli_query($conn, $sqlCheckPolis) or die(mysqli_error($conn));
				while ($fetchPolis 	= mysqli_fetch_array($cekpolis)) 
				{
					$InsuredName = str_replace("'", "''", $fetchPolis['INSURED_NAME']);
														
					$amount 		=	$fetchPolis['GROSS_PREMIUM'] * $rate / 100;
					$sqlInserDetail = "INSERT INTO tpaymentrequestdetail(PaymentRequestID, PolisNo, NotaNo, Corr, InsuredName, StatusSettlement, DateSettlement, BankSettlement, AccountSettlement, VoucherSettlement, CCY, GrossPremium, Amount) 
										VALUES('$id','".$fetchPolis['POLICY_NO']."','$NotaNo','".$fetchPolis['INSURED_CODE']."',
											'$InsuredName','PAID','".$fetchPolis['TRANS_DATE']."','".$fetchPolis['BANK_NAME']."','".$fetchPolis['ACCOUNT_NO']."','".$fetchPolis['STTL_NO']."',
											'".$fetchPolis['DBCR_CURRENCY']."','".$fetchPolis['GROSS_PREMIUM']."','$amount')";		
				
					$savePayReqDetail	=	mysqli_query($conn, $sqlInserDetail);
				}
			}

			
			if (isset($_POST['Tambahbaru'])) {
				
				$TambahBaru = $_POST['Tambahbaru'];

				foreach ( $TambahBaru AS $retrive_tambah_baru)
				{
					$NotaNoTambahBaru =	$retrive_tambah_baru;
					$sqlTambahPolis = "SELECT POLICY_NO,
										INSURED_CODE, 
										INSURED_NAME_FULL AS INSURED_NAME, 
										group_concat(date_format(`TRANS_DATE`,'%d-%m-%Y')) AS `TRANS_DATE`, 
										group_concat(`BANK_NAME`) AS `BANK_NAME`, 
										group_concat(`ACCOUNT_NUMBER`) AS `ACCOUNT_NO`, 
										group_concat(`STTL_NO`) AS STTL_NO, 
										DBCR_CURRENCY, 
										GROSS_PREMIUM  
										from stg_pmt_req_datamart where NOTE_NO = '$NotaNoTambahBaru' GROUP BY POLICY_NO";
										$cekPolisTambah	=	mysqli_query($conn, $sqlTambahPolis) or die(mysqli_error($conn));
					while ($fetchTambahPolis 	= mysqli_fetch_array($cekPolisTambah)) 
					{
						$InsuredNameTambahPolis = str_replace("'", "''", $fetchTambahPolis['INSURED_NAME']);
															
						$amountTambahPolis 		=	$fetchTambahPolis['GROSS_PREMIUM'] * $rate / 100;
						$sqlInsertTambahPolis = "INSERT INTO tpaymentrequestdetail(PaymentRequestID, PolisNo, NotaNo, Corr, InsuredName, StatusSettlement, DateSettlement, BankSettlement, AccountSettlement, VoucherSettlement, CCY, GrossPremium, Amount) 
											VALUES('$id','".$fetchTambahPolis['POLICY_NO']."','$NotaNoTambahBaru','".$fetchTambahPolis['INSURED_CODE']."',
												'$InsuredNameTambahPolis','PAID','".$fetchTambahPolis['TRANS_DATE']."','".$fetchTambahPolis['BANK_NAME']."','".$fetchTambahPolis['ACCOUNT_NO']."','".$fetchTambahPolis['STTL_NO']."',
												'".$fetchTambahPolis['DBCR_CURRENCY']."','".$fetchTambahPolis['GROSS_PREMIUM']."','$amountTambahPolis') ";		
					
						$saveTambahPolisDetail	=	mysqli_query($conn, $sqlInsertTambahPolis);
					}
				}
			}

			// Commit transaction
			// mysqli_commit($conn);

			$_SESSION['notif'] = 'RE-SUBMIT';
			 header("location:../../payreqlist");
		}
		catch(exception $e)
		{
			// Rollback transaction
			mysqli_rollback($conn);

			header("location:../../payreqlist");
		}

	}

/*
** END 						**
*/
?>
