<?php 
/*
** save 						**
** Created By Hockey        	**
** Created Date 20180402	 	**
** Group Menu Data 				**
** START 						**
*/ 

session_start();
include "../../koneksi/koneksi.php";
//For Maker
	if (isset($_POST['usergroup-save'])) {
		
		try
		{
			$user	=	$_SESSION['username'];
			$username = 	$_POST['username'];
			$group = $_POST['group'] ; 	

			// Set autocommit to off
			//mysqli_autocommit($conn, FALSE);
			
			if(!empty($group))
			{
				echo $queryDelete = "DELETE FROM security_user_group where username = '$username' ";
				$deleteDetail	=	mysqli_query($conn, $queryDelete);

				foreach ($group AS $retrive)
				{
					//echo $retrieve; echo "<br>";

					echo $query = "INSERT INTO security_user_group (username, group_id, CREATED_BY, MOD_DATE, MOD_BY) 
								VALUES ('$username', $retrive, '$user', now(), '$user')";
					$saveGroupMenu	=	mysqli_query($conn, $query);
				}
			}
			
			// Commit transaction
			//mysqli_commit($conn);

			$_SESSION['notif'] = 'USERGROUP-SAVE';
			header("location:../../usergrouplistdetail");
		}
		catch(exception $e)
		{
			// Rollback transaction
			mysqli_rollback($conn);

			$_SESSION['notif'] = 'USERGROUP-SAVEFAILED';
			header('location:../../');
		}
		finally
		{
			// close connection
			mysqli_close($conn);
		}
		
	}
	else
	{
		$_SESSION['notif'] = 'USERGROUP-SAVEFAILED';
		header('location:../../');
	}

/*
** END 						**
*/
?>
