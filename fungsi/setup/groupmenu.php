<?php 
/*
** save 						**
** Created By Hockey        	**
** Created Date 20180402	 	**
** Group Menu Data 				**
** START 						**
*/ 

session_start();
include "../../koneksi/koneksi.php";
//For Maker
	if (isset($_POST['groupmenu-save'])) {
		
		try
		{
			$user	=	$_SESSION['username']; 
			$group = 	$_POST['group'] ;
			
			$menu = $_POST['menu'] ; 	

			// Set autocommit to off
			mysqli_autocommit($conn, FALSE);
			
			$deleteDetail	=	mysqli_query($conn, "DELETE FROM security_group_menu where group_id = $group ");

			if(!empty($menu))
			{				
				foreach ($menu AS $retrive)
				{
					echo $query = "INSERT INTO security_group_menu (group_id, menu_id) VALUES ($group, $retrive)";
					$saveGroupMenu	=	mysqli_query($conn, $query);
				}
			}
			
			//cara 2
			// $jumlah_dipilih = count($menu);
			// for($x = 0; $x < $jumlah_dipilih; $x++){
			// 	echo $x;
			// }
			
			// Commit transaction
			mysqli_commit($conn);

			$_SESSION['notif'] = 'GROUPMENU-SAVE';
			header("location:../../groupmenulistdetail");
		}
		catch(exception $e)
		{
			// Rollback transaction
			mysqli_rollback($conn);

			$_SESSION['notif'] = 'GROUPMENU-FAILED';
			header('location:../../');
		}
		finally
		{
			// close connection
			mysqli_close($conn);
		}
		
	}
	else
	{
		$_SESSION['notif'] = 'GROUPMENU-FAILED';
		header('location:../../');
	}

/*
** END 						**
*/
?>
