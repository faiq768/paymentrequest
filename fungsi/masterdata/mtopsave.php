<?php 
/*
** save  master TOP				**
** Created By Hockey        	**
** Created Date 20180314	 	**
** Save Master TOP 				**
** Delete Master TOP 			**
** START 						**
*/ 

session_start();
	include "../../koneksi/koneksi.php";
	if (isset($_POST['mtop-save'])) {
		try
		{
			$user =	$_SESSION['username'];
			$code = $_POST['ncode-mtop'];
			$desc = $_POST['ndesc-mtop'];

			// Set autocommit to off
			mysqli_autocommit($conn, FALSE);	

			//Master Type Of Payment without IsActive
			$queryCheckTOP 	= "SELECT * FROM mtypeofpayment WHERE CODE='$code' ";
			$dataCheck		= mysqli_query($conn, $queryCheckTOP) or die(mysqli_error($conn));
			$totalrow		= mysqli_num_rows($dataCheck);

			if($totalrow > 0)
			{
				$queryCheckTOP 	= "SELECT * FROM mtypeofpayment WHERE ISACTIVE=1 AND CODE='$code' ";
				$dataCheck		= mysqli_query($conn, $queryCheckTOP) or die(mysqli_error($conn));
				$totalrow		= mysqli_num_rows($dataCheck);

				if($totalrow > 1 )
				{
					$query = "UPDATE mtypeofpayment 
				 		SET DESCRIPTION='$desc'
				 			, MOD_DATE=NOW()
				 			, MOD_BY='$user'
				 		WHERE CODE = '$code' ";
				}
				else
				{
					$queryCheckTOPInTrx = "SELECT * FROM tpaymentrequestheader WHERE TypeOfPayment=(SELECT DESCRIPTION FROM mtypeofpayment WHERE CODE='$code') ";
					$dataCheckTOPInTrx	= mysqli_query($conn, $queryCheckTOPInTrx) or die(mysqli_error($conn));
					$totalrowCheckTOPInTrx	= mysqli_num_rows($dataCheckTOPInTrx);	

					if($totalrowCheckTOPInTrx > 0)
					{
						$_SESSION['notif'] = 'MTOP-SAVEExists';
						header("Location:../../mtypeofpaymentlistdetail?code=$code&desc=$desc");
						exit;
					}
					else
					{
						$query = "UPDATE mtypeofpayment 
					 		SET DESCRIPTION='$desc'
					 			, ISACTIVE=1
					 			, MOD_DATE=NOW()
					 			, MOD_BY='$user'
					 		WHERE CODE = '$code' ";
					}
				}
			}
			else
			{
				$query = " INSERT INTO mtypeofpayment (CODE, DESCRIPTION, CREATED_BY)
							VALUES ('$code', '$desc', '$user')";
			}

			$updateheaderQuery		=	mysqli_query($conn,$query) or die(mysqli_error($conn));

			// Commit transaction
			mysqli_commit($conn);

			$_SESSION['notif'] = 'MTOP-SAVE';
			header("location:../../mtypeofpaymentlist");
		}
		catch (exception $e)
		{
			// Rollback transaction
			mysqli_rollback($conn);
			$_SESSION['notif'] = 'MTOP-SAVEFAILED';
			header("location:../../mtypeofpaymentlist");
		}
		finally
		{
			// close connection
			mysqli_close($conn);
		}
	}
	elseif (isset($_POST['mtop-edit'])) 
	{
		try
		{
			$user =	$_SESSION['username'];
			$code = $_POST['ncode-mtop'];
			$desc = $_POST['ndesc-mtop'];

			// Set autocommit to off
			mysqli_autocommit($conn, FALSE);

			$queryCheckTOP 	= "SELECT * FROM mtypeofpayment WHERE CODE!='$code' AND DESCRIPTION='$desc' ";
			$dataCheck		= mysqli_query($conn, $queryCheckTOP) or die(mysqli_error($conn));
			$totalrow		= mysqli_num_rows($dataCheck);
			if($totalrow > 0)
			{
				$_SESSION['notif'] = 'MTOP-SAVEExistsTrx';
				//header("Location:../../mtypeofpaymentlistdetail?form=edit&code=$code&desc='$desc'");
				header("location:../../mtypeofpaymentlist");
				exit;
			}
			else 
			{
				$queryCheckTOPInTrx = "SELECT * FROM tpaymentrequestheader WHERE TypeOfPayment=(SELECT DESCRIPTION FROM mtypeofpayment WHERE CODE='$code') ";
				$dataCheckTOPInTrx	= mysqli_query($conn, $queryCheckTOPInTrx) or die(mysqli_error($conn));
				$totalrowCheckTOPInTrx	= mysqli_num_rows($dataCheckTOPInTrx);	

				if($totalrowCheckTOPInTrx > 0)
				{
					$_SESSION['notif'] = 'MTOP-SAVEExists';
					header("location:../../mtypeofpaymentlist");
					exit;
				}
				else
				{
					$query = "UPDATE mtypeofpayment 
					 		SET DESCRIPTION='$desc'
					 			, ISACTIVE=1
					 			, MOD_DATE=NOW()
					 			, MOD_BY='$user'
					 		WHERE CODE = '$code' ";
				}
			}

			$updateheaderQuery		=	mysqli_query($conn,$query) or die(mysqli_error($conn));

			// Commit transaction
			mysqli_commit($conn);

			$_SESSION['notif'] = 'MTOP-SAVE';
			header("location:../../mtypeofpaymentlist");
		}
		catch (exception $e)
		{
			// Rollback transaction
			mysqli_rollback($conn);
			$_SESSION['notif'] = 'MTOP-SAVEFAILED';
			header("location:../../mtypeofpaymentlist");
		}
		finally
		{
			// close connection
			mysqli_close($conn);
		}
	}
	else if (isset($_POST['mtop-delete'])) {
		try
		{
			$user =	$_SESSION['username'];
			$code = $_POST['ncode-mtop'];
			$desc = $_POST['ndesc-mtop'];

			// Set autocommit to off
			mysqli_autocommit($conn,FALSE);	


			$queryCheckTOPInTrx = "SELECT * FROM tpaymentrequestheader WHERE TypeOfPayment=(SELECT DESCRIPTION FROM mtypeofpayment WHERE CODE='$code') ";
			$dataCheckTOPInTrx	= mysqli_query($conn, $queryCheckTOPInTrx) or die(mysqli_error($conn));
			$totalrowCheckTOPInTrx	= mysqli_num_rows($dataCheckTOPInTrx);	

			if($totalrowCheckTOPInTrx > 0)
			{
				$_SESSION['notif'] = 'MTOP-DELETEFAILED';
				header("Location:../../mtypeofpaymentlist");
				exit;
			}
			else
			{
				$query = "UPDATE mtypeofpayment 
					SET ISACTIVE=0
						, MOD_DATE=NOW()
						, MOD_BY='$user'
					WHERE CODE = '$code' ";
			}			

			$datadelete		=	mysqli_query($conn, $query) or die(mysqli_error($conn));

			// Commit transaction
			mysqli_commit($conn);

			$_SESSION['notif'] = 'MTOP-DELETE';
			header("location:../../mtypeofpaymentlist");
		}
		catch (exception $e)
		{
			// Rollback transaction
			mysqli_rollback($conn);
			$_SESSION['notif'] = 'MTOP-DELETEFAILED';
			header("location:../../mtypeofpaymentlist");
		}
		finally
		{
			// close connection
			mysqli_close($conn);
		}
	}
	if (isset($_POST['mtop-cancel'])) 
	{
		header("location:../../mtypeofpaymentlist");
	}

?>