<?php 
/*
** save  master Group			**
** Created By Hockey        	**
** Created Date 20180402	 	**
** Save Master Group 			**
** Delete Master Group 			**
** START 						**
*/ 

session_start();
	include "../../koneksi/koneksi.php";
	if (isset($_POST['mgroup-save'])) {
		try
		{
			$user =	$_SESSION['username'];
			$name = $_POST['nGroupName'];
			
			
			//Master Type Of Payment without IsActive
			$queryCheckGroup 	= "SELECT * FROM security_group WHERE NAME='$name' ";
			$dataCheck			= mysqli_query($conn, $queryCheckGroup) or die(mysqli_error($conn));
			$totalrow			= mysqli_num_rows($dataCheck);

			if($totalrow > 0)
			{
				$queryCheckGroup 	= "SELECT * FROM security_group WHERE ISACTIVE=1 AND NAME='$name' ";
				$dataCheck		= mysqli_query($conn, $queryCheckGroup) or die(mysqli_error($conn));
				$totalrow		= mysqli_num_rows($dataCheck);

				if($totalrow > 0 )
				{
					//harus validate trx

					echo $queryCheckExists 	= "SELECT group_id FROM security_group_menu WHERE group_id = (SELECT ID FROM security_group WHERE NAME='$name')
											UNION ALL
											SELECT group_id FROM security_user_group WHERE group_id = (SELECT ID FROM security_group WHERE NAME='$name') ";
					$dataCheckExists	= mysqli_query($conn, $queryCheckExists) or die(mysqli_error($conn));
					$totalrow			= mysqli_num_rows($dataCheckExists);

					if($totalrow > 0 )
					{
						$_SESSION['notif'] = 'MGROUP-SAVEExists';
						header("location:../../mgrouplist");
					}
					else
					{
						$query = "UPDATE security_group 
					 		SET NAME='$name'
					 			, MOD_DATE=NOW()
					 			, MOD_BY='$user'
					 		WHERE NAME = '$name' ";	
					}

					
				}
				else
				{
					//harus validate trx
					$queryCheckExists 	= "SELECT group_id FROM security_group_menu WHERE group_id = (SELECT ID FROM security_group WHERE NAME='$name')
											UNION ALL
											SELECT group_id FROM security_user_group WHERE group_id = (SELECT ID FROM security_group WHERE NAME='$name') ";
					$dataCheckExists	= mysqli_query($conn, $queryCheckExists) or die(mysqli_error($conn));
					$totalrow			= mysqli_num_rows($dataCheckExists);

					if($totalrow > 0 )
					{
						$_SESSION['notif'] = 'MGROUP-SAVEExists';
						header("location:../../mgrouplist");
					}
					else
					{

						$query = "UPDATE security_group 
					 		SET NAME='$name'
					 			, ISACTIVE=1
					 			, MOD_DATE=NOW()
					 			, MOD_BY='$user'
					 		WHERE NAME = '$name' ";
					}
				}
			}
			else
			{
				$query = " INSERT INTO security_group (NAME, CREATED_BY)
							VALUES ('$name', '$user')";
			}

			if(!empty($query))
			{
				// Set autocommit to off
				mysqli_autocommit($conn, FALSE);	

				$updateQuery		=	mysqli_query($conn,$query) or die(mysqli_error($conn));	

				// Commit transaction
				mysqli_commit($conn);

				$_SESSION['notif'] = 'MGROUP-SAVE';
				header("location:../../mgrouplist");
			}
			else
			{
				$_SESSION['notif'] = 'MGROUP-SAVEFAILED';
				header("location:../../mgrouplist");
			}
		}
		catch (exception $e)
		{
			// Rollback transaction
			mysqli_rollback($conn);
			$_SESSION['notif'] = 'MGROUP-SAVEFAILED';
			header("location:../../mgrouplist");
		}
		finally
		{
			// close connection
			mysqli_close($conn);
		}
	}
	elseif (isset($_POST['mgroup-edit'])) 
	{
		try
		{
			$user 	=	$_SESSION['username'];
			$name 	= $_POST['nGroupName'];
			$id 	= $_POST['nGroupid'];
			
			$queryCheck 	= "SELECT * FROM security_group WHERE ID!='$id' AND NAME='$name' ";
			$dataCheck		= mysqli_query($conn, $queryCheck) or die(mysqli_error($conn));
			$totalrow		= mysqli_num_rows($dataCheck);
			if($totalrow > 0)
			{
				$_SESSION['notif'] = 'MGROUP-SAVEExists';
				header("location:../../mgrouplist");
			}
			else 
			{
				//harus validate trx
				echo $queryCheckExists 	= "SELECT group_id FROM security_group_menu WHERE group_id = (SELECT ID FROM security_group WHERE ID = '$id')
										UNION ALL
										SELECT group_id FROM security_user_group WHERE group_id = (SELECT ID FROM security_group WHERE ID = '$id' ) ";
				$dataCheckExists	= mysqli_query($conn, $queryCheckExists) or die(mysqli_error($conn));
				$totalrow			= mysqli_num_rows($dataCheckExists);

				if($totalrow > 0)
				{
					$_SESSION['notif'] = 'MGROUP-SAVEExists';
					header("location:../../mgrouplist");
				}
				else
				{
					$query = "UPDATE security_group 
					 		SET NAME='$name'
					 			, MOD_DATE=NOW()
					 			, MOD_BY='$user'
					 		WHERE ID = '$id' ";
				}
			}

			if(!empty($query))
			{
				// Set autocommit to off
				mysqli_autocommit($conn, FALSE);

				$updateheaderQuery		=	mysqli_query($conn,$query) or die(mysqli_error($conn));

				// Commit transaction
				mysqli_commit($conn);

				$_SESSION['notif'] = 'MGROUP-SAVE';
				header("location:../../mgrouplist");
			}
			else
			{
				$_SESSION['notif'] = 'MGROUP-SAVEFAILED';
				header("location:../../mgrouplist");
			}
		}
		catch (exception $e)
		{
			// Rollback transaction
			mysqli_rollback($conn);
			$_SESSION['notif'] = 'MGROUP-SAVEFAILED';
			header("location:../../mgrouplist");
		}
		finally
		{
			// close connection
			mysqli_close($conn);
		}
	}
	else if (isset($_POST['mgroup-delete'])) {
		try
		{
			$user 	= $_SESSION['username'];
			$name 	= $_POST['nGroupName'];
			$id 	= $_POST['nGroupid'];
			

			// Set autocommit to off
			mysqli_autocommit($conn,FALSE);	


			//harus validate trx
			$queryCheckExists 	= "SELECT group_id FROM security_group_menu WHERE group_id = (SELECT ID FROM security_group WHERE ID = '$id' AND NAME='$name')
									UNION ALL
									SELECT group_id FROM security_user_group WHERE group_id = (SELECT ID FROM security_group WHERE ID = '$id' AND NAME='$name') ";
			$dataCheckExists	= mysqli_query($conn, $queryCheckExists) or die(mysqli_error($conn));
			$totalrow			= mysqli_num_rows($dataCheckExists);

			if($totalrow > 0)
			{
				$_SESSION['notif'] = 'MGROUP-DELETEFAILED';
				header("Location:../../mgrouplist");
				exit;
			}
			else
			{
				$query = "UPDATE security_group 
					SET ISACTIVE=0
						, MOD_DATE=NOW()
						, MOD_BY='$user'
					WHERE ID = '$id' AND NAME = '$name' ";
			}			

			$datadelete		=	mysqli_query($conn, $query) or die(mysqli_error($conn));

			// Commit transaction
			mysqli_commit($conn);

			$_SESSION['notif'] = 'MGROUP-DELETE';
			header("location:../../mgrouplist");
		}
		catch (exception $e)
		{
			// Rollback transaction
			mysqli_rollback($conn);
			$_SESSION['notif'] = 'MGROUP-DELETEFAILED';
			header("location:../../mgrouplist");
		}
		finally
		{
			// close connection
			mysqli_close($conn);
		}
	}
	if (isset($_POST['mgroup-cancel']))
	{
		header("location:../../mgrouplist");
	}

?>