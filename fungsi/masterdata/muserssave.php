<?php 
/*
** save  master User			**
** Created By Hockey        	**
** Created Date 20180405	 	**
** Save Master User 			**
** Delete Master User 			**
** START 						**
*/ 

session_start();
	include "../../koneksi/koneksi.php";
	if (isset($_POST['musers-save'])) {
		try
		{
			$user =	$_SESSION['username'];
			$username = $_POST['nusername'];
			//$branch = $_POST['nbranchcode'];
			$branch = $_POST['nBranchCodeId'];
			
			$intermediaryType = $_POST['intermediaryId'];
			
			
			$fullname = $_POST['nfullname'];
			$dept = $_POST['nDeptId'];
			
			$dept = substr($dept, 0, strrpos($dept, ','));
			
			$branch = substr($branch, 0, strrpos($branch, ','));
			
			$intermediaryType = substr($intermediaryType, 0, strrpos($intermediaryType, ','));
			
			$intermediaryType;
			
			$query = '';
			
			$query1 	= "SELECT * FROM security_user WHERE username='$username' ";
			$data1		= mysqli_query($conn, $query1) or die(mysqli_error($conn));
			$totalrow1	= mysqli_num_rows($data1);

			if($totalrow1 > 0)
			{
				$query2 	= "SELECT * FROM security_user WHERE ISACTIVE=1 AND username='$username' ";
				$data2		= mysqli_query($conn, $query2) or die(mysqli_error($conn));
				$totalrow2		= mysqli_num_rows($data2);

				if($totalrow2 > 0 )
				{
					$query3 	= "SELECT * FROM tpaymentrequestheader WHERE CREATED_BY='$username' AND  STATUS_PR NOT IN (SELECT Description FROM mgeneral_table WHERE CODE='PaymentRequest_STATUS' AND Value in (5,7) ) ";
					$data3		= mysqli_query($conn, $query3) or die(mysqli_error($conn));
					$totalrow3		= mysqli_num_rows($data3);

					if($totalrow3 > 0)
					{
						$_SESSION['notif'] = 'MUSERS-SAVEExists';
						header("Location:../../muserslistdetail");
						exit;
					}
					else
					{
						$query = "UPDATE security_user 
							SET home_branch = '$branch'
									,intermediary_type = '$intermediaryType'
									,full_name = '$fullname'
									,Dept_Id = '$dept'
									,MOD_DATE = now()
									,MOD_BY = '$user'
							WHERE username = '$username'
					";
					}
				}
				else
				{
					$query = "UPDATE security_user 
							SET home_branch = '$branch'
									intermediary_type = '$intermediaryType',
									,full_name = '$fullname'
									,Dept_Id = '$dept'
									,MOD_DATE = now()
									,MOD_BY = '$user'
									,ISACTIVE = 1
							WHERE username = '$username'
					";
				}
			}
			else
			{
				echo $query = " INSERT INTO security_user (username,intermediary_type, home_branch, full_name, Dept_Id, CREATED_BY, Password)
							VALUES ('$username', '$intermediaryType', '$branch', '$fullname', '$dept', '$user', (SELECT Description FROM mgeneral_table WHERE CODE='PASSWORD_DEFAULT'))";
			}

			if(!empty($query))
			{
				// Set autocommit to off
				mysqli_autocommit($conn, FALSE);	

				//$updateQuery		=	mysqli_query($conn,$query) or die(mysqli_error($conn));	

				// Commit transaction
				mysqli_commit($conn);

				//$_SESSION['notif'] = 'MUSERS-SAVE';
				//header("location:../../muserslist");
			}
			else
			{
				//$_SESSION['notif'] = 'MUSERS-SAVEFAILED';
				//header("location:../../muserslist");
			}
		}
		catch (exception $e)
		{
			// Rollback transaction
			mysqli_rollback($conn);
			//$_SESSION['notif'] = 'MUSERS-SAVEFAILED';
			//header("location:../../muserslist");
		}
		finally
		{
			// close connection
			mysqli_close($conn);
		}
	}
	elseif (isset($_POST['musers-edit'])) 
	{
		try
		{
			$user =	$_SESSION['username'];
			$username = $_POST['nusername'];
			//$branch = $_POST['nbranchcode'];
			$branch = $_POST['nBranchCodeId'];
			$fullname = $_POST['nfullname'];
			$dept = $_POST['nDeptId'];
			
			
			$intermediaryType = $_POST['intermediaryId'];
			
			
			$dept = substr($dept, 0, strrpos($dept, ','));
			
			//echo "EEE ".$dept;
			//die();
			
			$branch = substr($branch, 0, strrpos($branch, ','));
			
			$intermediaryType = substr($intermediaryType, 0, strrpos($intermediaryType, ','));
			
			$query = '';
			
			$query1 	= "SELECT * FROM security_user WHERE username='$username' ";
			$data1		= mysqli_query($conn, $query1) or die(mysqli_error($conn));
			$totalrow1	= mysqli_num_rows($data1);
			

			if($totalrow1 > 0)
			{
				$query2 	= "SELECT * FROM security_user WHERE ISACTIVE=1 AND username='$username' ";
				$data2		= mysqli_query($conn, $query2) or die(mysqli_error($conn));
				$totalrow2		= mysqli_num_rows($data2);

				
			
			
				if($totalrow2 > 0 )
				{
					$query3 	= "SELECT * FROM tpaymentrequestheader WHERE CREATED_BY='$username' AND  STATUS_PR NOT IN (SELECT Description FROM mgeneral_table WHERE CODE='PaymentRequest_STATUS' AND Value in (5,7) ) ";
					$data3		= mysqli_query($conn, $query3) or die(mysqli_error($conn));
					$totalrow3		= mysqli_num_rows($data3);
	
	
					
				
					if($totalrow3 > 0)
					{
						$query = "UPDATE security_user 
							SET home_branch = '$branch'
									, intermediary_type = '$intermediaryType'
									,full_name = '$fullname'
									,Dept_Id = '$dept'
									,MOD_DATE = now()
									,MOD_BY = '$user'
							WHERE username = '$username'
						";
						
						mysqli_autocommit($conn, true);	

						$updateQuery		=	mysqli_query($conn,$query) or die(mysqli_error($conn));	

						// Commit transaction
						mysqli_commit($conn);

						$_SESSION['notif'] = 'MUSERS-SAVE';
						header("location:../../muserslist");
				
						//$_SESSION['notif'] = 'MUSERS-SAVEExists';
						//header("Location:../../muserslist");
						//exit;
					}
					else
					{
						$query = "UPDATE security_user 
							SET home_branch = '$branch'
									, intermediary_type = '$intermediaryType'
									,full_name = '$fullname'
									,Dept_Id = '$dept'
									,MOD_DATE = now()
									,MOD_BY = '$user'
							WHERE username = '$username'
						";
						
						
					}
				}
				else
				{
					$query = "UPDATE security_user 
							SET home_branch = '$branch'
									,intermediary_type = '$intermediaryType'
									,full_name = '$fullname'
									,Dept_Id = '$dept'
									,MOD_DATE = now()
									,MOD_BY = '$user'
									,ISACTIVE = 1
							WHERE username = '$username'
					";
				}
			}
			else
			{
				$query = " INSERT INTO security_user (username, intermediary_type, home_branch, full_name, Dept_Id, CREATED_BY, Password)
							VALUES ('$username', '$intermediaryType', '$branch', '$fullname', '$dept', '$user', (SELECT Description FROM mgeneral_table WHERE CODE='PASSWORD_DEFAULT'))";
			}

			if(!empty($query))
			{
				// Set autocommit to off
				mysqli_autocommit($conn, FALSE);	

				$updateQuery		=	mysqli_query($conn,$query) or die(mysqli_error($conn));	

				// Commit transaction
				mysqli_commit($conn);

				$_SESSION['notif'] = 'MUSERS-SAVE';
				header("location:../../muserslist");
			}
			else
			{
				$_SESSION['notif'] = 'MUSERS-SAVEFAILED';
				header("location:../../muserslist");
			}
		}
		catch (exception $e)
		{
			// Rollback transaction
			mysqli_rollback($conn);
			$_SESSION['notif'] = 'MUSERS-SAVEFAILED';
			header("location:../../muserslist");
		}
		finally
		{
			// close connection
			mysqli_close($conn);
		}
	}
	elseif (isset($_POST['musers-delete'])) 
	{
		try
		{
			$user =	$_SESSION['username'];
			$username = $_POST['nusername'];
			$branch = $_POST['nbranchcode'];
			$fullname = $_POST['nfullname'];
			$dept = $_POST['nDeptId'];
			
			$query = '';
			

			//harus validate trx
			$query3 	= "SELECT * FROM tpaymentrequestheader WHERE CREATED_BY='$username' AND  STATUS_PR NOT IN (SELECT Description FROM mgeneral_table WHERE CODE='PaymentRequest_STATUS' AND Value in (5,7) ) ";
			$data3		= mysqli_query($conn, $query3) or die(mysqli_error($conn));
			$totalrow3		= mysqli_num_rows($data3);

			if($totalrow3 > 0)
			{
				$_SESSION['notif'] = 'MUSERS-DELETEFAILED';
				header("Location:../../muserslist");
				exit;
			}
			else
			{
				$query = "UPDATE security_user 
					SET ISACTIVE=0
						, MOD_DATE=NOW()
						, MOD_BY='$user'
					WHERE username = '$username' ";
			}			

			if(!empty($query))
			{
				// Set autocommit to off
				mysqli_autocommit($conn, FALSE);	

				$updateQuery		=	mysqli_query($conn, $query) or die(mysqli_error($conn));	

				// Commit transaction
				mysqli_commit($conn);

				$_SESSION['notif'] = 'MUSERS-DELETE';
				header("location:../../muserslist");
			}
			else
			{
				$_SESSION['notif'] = 'MUSERS-SAVEFAILED';
				header("location:../../muserslist");
			}

			
			
		}
		catch (exception $e)
		{
			// Rollback transaction
			mysqli_rollback($conn);
			$_SESSION['notif'] = 'MUSERS-SAVEFAILED';
			header("location:../../muserslist");
		}
		finally
		{
			// close connection
			mysqli_close($conn);
		}
	}
	elseif (isset($_POST['musers-cancel'])) 
	{
		header("location:../../muserslist");
	}
?>