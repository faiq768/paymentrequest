<?php 
session_start();
include "../../koneksi/koneksi.php";


if (isset($_POST['btnPaid'])) 
{
	try
	{	
		$code 				=   explode(";",$_POST['id']);
		$PaymentRequestNo 	=	$code[0];
		$ccy				=	$code[1];
		$insllment_ke		=	$code[2];
		echo $VoucherNo 			= 	$_POST['voucherNo'];
		$VoucherDate 		= 	$_POST['VoucherDate'];
// exit();
		$queryVal = "SELECT * FROM tpaymentrequest_installment_detail WHERE VoucherNo='$VoucherNo' ";
		$dataVal = mysqli_query($conn, $queryVal) or die(mysqli_error($conn));
		$dataRow    = $dataVal->num_rows;
		
		if($dataRow > 0)
		{
			echo "<script>swal('THE PAYMENT REQUEST ALREADY EXISTS', '$VoucherNo', 'warning'); </script>";
			//echo "<script>return confirm( '$voucherNo Already Exists. Are you sure you want to continue ?' ); </script>";
			// $_SESSION['notif'] = 'HOPAIDFAILED';
			exit();
		}

		$SQL_GET_ID			=	mysqli_query($conn, "SELECT a.id, b.ID AS idHeader FROM tpaymentrequest_installment a 
													JOIN tpaymentrequestheader b 
													ON b.PaymentRequestNo = a.PaymentRequestNo 
													WHERE a.PaymentRequestNo = '$PaymentRequestNo' ") ;
		$FETCH_SQL_GET_ID	=	mysqli_fetch_array($SQL_GET_ID);
		echo $ID_INSTALLMENT 	=	$FETCH_SQL_GET_ID['id'];
		$ID_PAYMENTREQUEST	=	$FETCH_SQL_GET_ID['idHeader'];
		
		$STATUS = "INSTALLMENT ".$insllment_ke." - ".$ccy." - PAID";

		// if ($SUMDATA['install'] == $SUMDATA['Amount']) {
		// 	$sqlQuery = mysqli_query($conn, "UPDATE tpaymentrequestheader SET STATUS_PAID = '' WHERE PaymentRequestNo=$id ");
		// }

		
		$date=date_create($VoucherDate);
		$date2 = date_format($date,"Y-m-d");
		// Set autocommit to off
		mysqli_autocommit($conn, FALSE);
		mysqli_begin_transaction($conn, MYSQLI_TRANS_START_READ_WRITE);

		echo $queryPayReq 	= "UPDATE tpaymentrequest_installment_detail SET VoucherNo='$VoucherNo', VoucherDate='$date2', PAY_PAID_DATE=now(), PAY_PAID_BY = '".$_SESSION['username']."' , PAID_STATUS = (SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' AND Value=6) WHERE installment_id ='$ID_INSTALLMENT' AND Installment_ke = '$insllment_ke' AND CCY = '$ccy' ";
		$queryHistory 	= "INSERT INTO thistory_approval values('$STATUS', $ID_PAYMENTREQUEST, (SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' AND Value=6),NOW(),'".$_SESSION['username']."',5,'')";

		$dataPayReq = mysqli_query($conn, $queryPayReq) or die(mysqli_error($conn));
		//$dataHistory = mysqli_query($conn, $queryHistory) or die(mysqli_error($conn));

		//$queryPayReq;

		// Commit transaction
		mysqli_commit($conn);
		$_SESSION['notif'] = 'HOPAID';
		
	}
	catch(exception $e)
	{
		// Rollback transaction
		mysqli_rollback($conn);
		
		$_SESSION['notif'] = 'HOPAIDFAILED';
	}
	header("location:../../installmentListFinance");
}
elseif (isset($_POST['btnPaidEdit'])) 
{
	try
	{	
		$code 				=   explode(";",$_POST['id']);
		$PaymentRequestNo 	=	$code[0];
		$ccy				=	$code[1];
		$insllment_ke		=	$code[2];
		echo $VoucherNo 			= 	$_POST['voucherNo'];
		$VoucherDate 		= 	$_POST['VoucherDate'];
// exit();
		$queryVal = "SELECT * FROM tpaymentrequest_installment_detail WHERE VoucherNo='$VoucherNo' ";
		$dataVal = mysqli_query($conn, $queryVal) or die(mysqli_error($conn));
		$dataRow    = $dataVal->num_rows;
		
		if($dataRow > 0)
		{
			echo "<script>swal('THE PAYMENT REQUEST ALREADY EXISTS', '$VoucherNo', 'warning'); </script>";
			//echo "<script>return confirm( '$voucherNo Already Exists. Are you sure you want to continue ?' ); </script>";
			// $_SESSION['notif'] = 'HOPAIDFAILED';
			exit();
		}

		$SQL_GET_ID			=	mysqli_query($conn, "SELECT a.id, b.ID AS idHeader FROM tpaymentrequest_installment a 
													JOIN tpaymentrequestheader b 
													ON b.PaymentRequestNo = a.PaymentRequestNo 
													WHERE a.PaymentRequestNo = '$PaymentRequestNo' ") ;
		$FETCH_SQL_GET_ID	=	mysqli_fetch_array($SQL_GET_ID);
		echo $ID_INSTALLMENT 	=	$FETCH_SQL_GET_ID['id'];
		$ID_PAYMENTREQUEST	=	$FETCH_SQL_GET_ID['idHeader'];
		
		$STATUS = "INSTALLMENT ".$insllment_ke." - ".$ccy." - EDIT NO VOUCHER BY ".$_SESSION['username'];

		// if ($SUMDATA['install'] == $SUMDATA['Amount']) {
		// 	$sqlQuery = mysqli_query($conn, "UPDATE tpaymentrequestheader SET STATUS_PAID = '' WHERE PaymentRequestNo=$id ");
		// }

		
		$date=date_create($VoucherDate);
		$date2 = date_format($date,"Y-m-d");
		// Set autocommit to off
		mysqli_autocommit($conn, FALSE);
		mysqli_begin_transaction($conn, MYSQLI_TRANS_START_READ_WRITE);

		echo $queryPayReq 	= "UPDATE tpaymentrequest_installment_detail SET VoucherNo='$VoucherNo', VoucherDate='$date2', PAY_PAID_DATE=now(), PAY_PAID_BY = '".$_SESSION['username']."' , PAID_STATUS = (SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' AND Value=6) WHERE installment_id ='$ID_INSTALLMENT' AND Installment_ke = '$insllment_ke' AND CCY = '$ccy' ";
		$queryHistory 	= "INSERT INTO thistory_approval values('$STATUS', $ID_PAYMENTREQUEST, (SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' AND Value=6),NOW(),'".$_SESSION['username']."',5,'')";

		$dataPayReq = mysqli_query($conn, $queryPayReq) or die(mysqli_error($conn));
		//$dataHistory = mysqli_query($conn, $queryHistory) or die(mysqli_error($conn));

		//$queryPayReq;

		// Commit transaction
		mysqli_commit($conn);
		$_SESSION['notif'] = 'HOPAID';
		
	}
	catch(exception $e)
	{
		// Rollback transaction
		mysqli_rollback($conn);
		
		$_SESSION['notif'] = 'HOPAIDFAILED';
	}
	header("location:../../installmentListPaid");
}else
{
	$_SESSION['notif'] = 'HOPAIDFAILED';
	header("location:../../home");
}
?>