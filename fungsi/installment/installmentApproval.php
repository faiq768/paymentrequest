<?php 
/*
** Payment Request Installment 	**
** Created By Faiq				**
** Created Date 06-11-2020      **
** START 						**
*/

session_start();
include "../../koneksi/koneksi.php";

	if (isset($_GET['id']) && !empty($_GET['id'])) {
		$id			=	$_GET['id'];
		$idInstall	=	$_GET['idInstall'];
		$ccy 		=	$_GET['ccy'];

		$queryGetPaymentRequestNo = mysqli_query($conn, "SELECT PaymentRequestNo
														 FROM tpaymentrequestheader
														 WHERE ID = '$id' ");
		$fetchGetPaymentRequestNo = mysqli_fetch_array($queryGetPaymentRequestNo);
		$NO_PAYMENT_REQUEST = $fetchGetPaymentRequestNo['PaymentRequestNo'];
		$STATUS = "INSTALLMENT PAYMENT REQUEST";

	try
		{
			switch ($_GET['function']) 
			{
				case 'INSTALLMENT_MAKER':
					$remarks		=	'';
					$queryPayReq	=	"UPDATE tpaymentrequest_installment SET MOD_DATE=now(), MOD_BY = '".$_SESSION['username']."', STATUS_INS = (SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' and Value=1) WHERE PaymentRequestNo = '$NO_PAYMENT_REQUEST'";
					$updateStatus 	=	"UPDATE tpaymentrequestheader SET installment = 'Y' WHERE PaymentRequestNo = '$NO_PAYMENT_REQUEST'";
					$updateSQL		=	mysqli_query($conn, $updateStatus);
					
					$queryHistory 	=	"INSERT INTO thistory_approval values('$STATUS','$id', (SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' AND Value=1), NOW(),'".$_SESSION['username']."',2,'$remarks')";
					$_SESSION['notif'] = 'INSTALLSUBMIT';
					break;
				case 'INSTALLMENT_CHECKER':
					$remarks		=	'';
					$queryPayReq	=	"UPDATE tpaymentrequest_installment SET MOD_DATE=now(), MOD_BY = '".$_SESSION['username']."', CHECKER_DATE=now(), CHECKER_BY = '".$_SESSION['username']."' , STATUS_INS = (SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' and Value=2) WHERE PaymentRequestNo = '$NO_PAYMENT_REQUEST'";
					$queryHistory 	=	"INSERT INTO thistory_approval values('$STATUS','$id', (SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' AND Value=2), NOW(),'".$_SESSION['username']."',3,'$remarks')";
					$_SESSION['notif'] = 'CHECKER';
					break;
				case 'INSTALLMENT_REVISECHECKER':
					$remarks	=	$_GET['remarks'];
					$queryPayReq	=	"UPDATE tpaymentrequest_installment SET MOD_DATE=now(), MOD_BY = '".$_SESSION['username']."', CHECKER_DATE = now(), CHECKER_BY = '".$_SESSION['username']."', STATUS_INS = (SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' and Value=4) WHERE PaymentRequestNo = '$NO_PAYMENT_REQUEST'";
					$queryHistory 	=	"INSERT INTO thistory_approval values('$STATUS','$id',(SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' AND Value=4), NOW(),'".$_SESSION['username']."',3,'$remarks')";
					$_SESSION['notif'] = 'REVISE';
					break;
				case 'INSTALLMENT_REJECTCHECKER':
					$remarks	=	$_GET['remarks'];
					$queryPayReq 	=	"UPDATE tpaymentrequest_installment SET MOD_DATE=now(), MOD_BY = '".$_SESSION['username']."', CHECKER_DATE = now(), CHECKER_BY = '".$_SESSION['username']."', STATUS_INS = (SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' and Value=5) WHERE PaymentRequestNo = '$NO_PAYMENT_REQUEST'";
					$queryHistory 	=	"INSERT INTO thistory_approval values('$STATUS','$id', (SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' AND Value=5), NOW(),'".$_SESSION['username']."',3,'$remarks')";
					$_SESSION['notif'] = 'REJECT';
					break;

				case 'INSTALLMENT_APPROVE':
					$remarks		=	'';
					$queryPayReq	=	"UPDATE tpaymentrequest_installment SET MOD_DATE=now(), MOD_BY = '".$_SESSION['username']."', APPROVAL_DATE=now(), APPROVAL_BY = '".$_SESSION['username']."', STATUS_INS = (SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' and Value=3) WHERE PaymentRequestNo = '$NO_PAYMENT_REQUEST'";
					$queryHistory 	=	"INSERT INTO thistory_approval values('$STATUS','$id',(SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' AND Value=3), NOW(),'".$_SESSION['username']."',4,'$remarks')";
					$_SESSION['notif'] = 'APPROVE';
					break;
				case 'INSTALLMENT_REVISEAPPROVE':
					$remarks	=	$_GET['remarks'];
					$queryPayReq	=	"UPDATE tpaymentrequest_installment SET MOD_DATE=now(), MOD_BY = '".$_SESSION['username']."', APPROVAL_DATE=now(), APPROVAL_BY = '".$_SESSION['username']."', STATUS_INS = (SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' and Value=4) WHERE PaymentRequestNo = '$id'";
					$queryHistory 	=	"INSERT INTO thistory_approval values('$STATUS','$id',(SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' AND Value=4), NOW(),'".$_SESSION['username']."',4,'$remarks')";
					$_SESSION['notif'] = 'REVISE';
					break;
				case 'INSTALLMENT_REJECTAPPROVE':
					$remarks	=	$_GET['remarks'];
					$queryPayReq 	=	"UPDATE tpaymentrequest_installment SET MOD_DATE=now(), MOD_BY = '".$_SESSION['username']."', APPROVAL_DATE=now(), APPROVAL_BY = '".$_SESSION['username']."', STATUS_INS = (SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' and Value=5) WHERE PaymentRequestNo = '$id'";
					$queryHistory 	=	"INSERT INTO thistory_approval values('$STATUS','$id', (SELECT Description FROM mgeneral_table WHERE Code='Installment_STATUS' AND Value=5), NOW(),'".$_SESSION['username']."',4,'$remarks')";
					$_SESSION['notif'] = 'REJECT';
					break;
				
			}

			if(isset($queryPayReq) && isset($queryHistory))
			{
				// Set autocommit to off
				mysqli_autocommit($con,FALSE);
				mysqli_begin_transaction($conn, MYSQLI_TRANS_START_READ_WRITE);

				$dataPayReq = mysqli_query($conn, $queryPayReq)  or die(mysqli_error($conn));
				$dataHistory = mysqli_query($conn, $queryHistory)  or die(mysqli_error($conn));

				// Commit transaction
				mysqli_commit($con);
			}
		}
		catch (exception $e)
		{
			// Rollback transaction
			mysqli_rollback($con);
		}
		finally
		{
			// close connection
			mysqli_close($conn);
		}
		
		header('location:../../installList');
	}
?>