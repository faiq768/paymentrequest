<?php
session_start();
include "../../koneksi/koneksi.php";
if (isset($_POST['save_installment'])) {
		
			$user				=	$_SESSION['username'];
			$PaymentRequestNo 	= 	$_POST['PaymentRequestNo']; 
			$id 				=	$_POST['PaymentRequestID']; 
			$ccyData			=	$_POST['ccy'];
			$ccy 				=	substr($ccyData,0,3); 			
			$queryHeader		= "SELECT SUM(b.Amount) AS Gross, a.IntermediaryName, a.IntermediaryCode,
								  a.IntermediaryType FROM tpaymentrequestheader a INNER JOIN tpaymentrequestdetail b ON 
								  b.PaymentRequestID = a.ID where a.PaymentRequestNo = '$PaymentRequestNo' AND b.CCY = '$ccy' ";
			$paymentInstall 	= mysqli_query($conn, $queryHeader);

			$paymentData		= 	mysqli_fetch_array($paymentInstall);
			$ROW 				=	mysqli_num_rows($paymentInstall);

			if ($ROW == 0 || $ROW == NULL) {
				echo "Data Tidak Ditemukan";
				$_SESSION['notif'] = 'PAYREQSUBMITFAILED';
				header("location:../../installment?msg=error");
			}else{
				$amount 		=	$paymentData['Gross'];
				echo $intername 		=	$paymentData['IntermediaryName'];echo "<br>";
				echo $intercode 		=	$paymentData['IntermediaryCode'];echo "<br>";
				echo $intertype 		=	$paymentData['IntermediaryType'];echo "<br>";

				$SQL_INSTALLMENT_HEADER 	= mysqli_query($conn, "SELECT id FROM tpaymentrequest_installment WHERE 
											  PaymentRequestNo = '$PaymentRequestNo'") or die(mysqli_error($conn));
				$ROWS_INSTALLMENT_HEADER	= mysqli_num_rows($SQL_INSTALLMENT_HEADER);
				// exit();
				if ($ROWS_INSTALLMENT_HEADER == 0) {
					$insert_installment = "INSERT INTO tpaymentrequest_installment (PaymentRequestNo,INTERMEDIARY_TYPE,
								   		  Client,CREATED_BY,CREATED_DATE,STATUS_INS) values('$PaymentRequestNo',
								   		  '$intertype','$intername','$user',NOW(),(SELECT Description 
								   		  FROM mgeneral_table WHERE Code='Installment_STATUS' and Value=7))";
					
					$sqlInsert 			=	mysqli_query($conn,$insert_installment) or die(mysqli_error($conn));
					
				}

				$SQL_INSTALLMENT		= mysqli_query($conn, "SELECT id FROM tpaymentrequest_installment WHERE 
											   				   PaymentRequestNo='$PaymentRequestNo'") 
															   or die(mysqli_error($conn));
				$INSTALLMENT_DETAIL_ROWS	= mysqli_num_rows($SQL_INSTALLMENT);

				$SQL_FETCH_INSTALLMENT		= mysqli_fetch_array($SQL_INSTALLMENT);

				$ID_INSTALLMENT				= $SQL_FETCH_INSTALLMENT['id'];

				$dataInstallmentDetail 		= $_POST['installmentItem'];

				if(!empty($dataInstallmentDetail)){
					foreach ( $dataInstallmentDetail AS $data)
					{
						$dataMentah 			= explode(";",$data);
						$installment_ke 		= $dataMentah[0];
						$installment_ccy 		= $dataMentah[1];
						$installment_amount		= $dataMentah[2];
						$installment_average	= $dataMentah[3];
						$installment_duedate	= $dataMentah[4];
						$_dueDate 				= date_create($installment_duedate);
						$_DUE_DATE 				= date_format($_dueDate,"Y-m-d");

						$SQL_INSTALLMENT_DETAIL = "INSERT INTO tpaymentrequest_installment_detail (Installment_id,
												   Installment_ke,CCY,Amount,Installment,Average,Due_Date) values('$ID_INSTALLMENT',
												   '$installment_ke','$installment_ccy','$amount','$installment_amount',
												   '$installment_average','$_DUE_DATE')";
						$QUERY_INSTALLMENT_DETAIL = mysqli_query($conn, $SQL_INSTALLMENT_DETAIL) or die(mysqli_error($conn));
						if (!$QUERY_INSTALLMENT_DETAIL) {
							$_SESSION['notif'] = 'PAYREQSUBMITFAILED';
							header("location:../../installment?msg=error");
						}
					}

						// $sqlCCY  		= "SELECT CCY, Sum(Amount) AS TOTAL FROM tpaymentrequestdetail 
	     //                                    where CCY
	     //                                    NOT IN( 
	     //                                    SELECT CCY FROM (
	     //                                    SELECT b.CCY, a.STATUS_INS 
						// 					FROM tpaymentrequest_installment a
						// 					JOIN tpaymentrequest_installment_detail b 
						// 					ON b.installment_id = a.id 
	     //                                    where a.PaymentRequestNo = '$PaymentRequestNo'
	     //                                    GROUP BY b.CCY, a.STATUS_INS
	     //                                    ) ABC WHERE ((STATUS_INS = 'PAID') OR STATUS_INS = 'CREATE' OR STATUS_INS = 'SUBMIT' OR STATUS_INS = 'CHECKER' OR STATUS_INS = 'APPROVAL')
	     //                                    ) AND PaymentRequestID = '$id'
	     //                                    GROUP BY CCY 
	     //                                   ";
      //                   $ccyExecute 	= mysqli_query($conn, $sqlCCY) or die(mysqli_error($conn));
						// $rowSqlCCY 		= mysqli_num_rows($ccyExecute);

						// if ($rowSqlCCY == 0) {
						// 	$sqlUpdateStatus = "UPDATE tpaymentrequestheader SET installment='Y' 
						// 						WHERE PaymentRequestNo = '$PaymentRequestNo'";
						// 	$executeUpdate = mysqli_query($conn, $sqlUpdateStatus);
						// }
						
						$_SESSION['notif'] = 'INSTALLSUCCESS';
							header("location:../../installment");
				}else{
					$_SESSION['notif'] = 'PAYREQSUBMITFAILED';
					header("location:../../installment?msg=error");
				}
			}
		
	}

?>