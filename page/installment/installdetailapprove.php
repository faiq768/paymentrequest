<?php 
	if (isset($_GET['id']) && !empty($_GET['id'])) 
	{
		$Installment_ID = $_GET['idInstallment'];
		$id 	=	$_GET['id'];
		$status =	$_GET['status'];
		$_SESSION['REQ_ID']	=	$id;
	
		
		$queryHeader 		= "SELECT a.*,b.* FROM tpaymentrequestheader a JOIN tpaymentrequest_installment b ON b.PaymentRequestNo = a.PaymentRequestNo WHERE a.ISACTIVE=1 AND b.Install_ID = '$Installment_ID' AND a.ID = '$id' AND b.STATUS_INS != 'INSTALLMENT PAID' ";
		$data				= mysqli_query($conn, $queryHeader) or die(mysqli_error($conn));
		$dataHeader			= mysqli_fetch_array($data);
			
		$queryHeader2 		= "SELECT * FROM tpaymentrequest_installment WHERE PaymentRequestNo = '".$dataHeader['PaymentRequestNo']."' AND Install_ID = '$Installment_ID'";
		$data2				= mysqli_query($conn, $queryHeader2) or die(mysqli_error($conn));
		$dataHeader2		= mysqli_fetch_array($data2);
		
		$createWhoData	 =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$dataHeader[CREATED_BY]'");
		$checkerWhoData	 =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$dataHeader[CHECKER_BY]'");
		$approvalWhoData =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$dataHeader[APPROVAL_BY]'");
		
		$createWho = mysqli_fetch_array($createWhoData);
		$checkerWho = mysqli_fetch_array($checkerWhoData);
		$approvalWho = mysqli_fetch_array($approvalWhoData);	
		
		
?>


<div class="print" style="font-size : 11px;">
	<div class="container">
		<div class="head">
			<h3>PAYMENT REQUEST</h3>
			<p> <?php echo $dataHeader['PaymentRequestNo']; ?></p>
		</div>

			<H3>DETAIL INSTALLMENT</H3>
			<br><br>
			<div class="isi">
				<table class="table" border="1">
					<thead>
						<tr>
							<th rowspan="2">NO</th>
							<th rowspan="2">NO.PAYMENT REQUEST</th>
							<th rowspan="2">SOB</th>
							<th rowspan="2">CLIENT</th>
							<th rowspan="2">CCY</th>
							<th rowspan="2">AMOUNT</th>
							<th rowspan="2">AVERAGE(%)</th>
							<th rowspan="2">JUMLAH YANG DIBAYARKAN(%)</th>
							<th rowspan="2">REMAINING AMOUNT</th>
							<th rowspan="2">NO.VOUCHER</th>
							<th rowspan="2">TANGGAL BAYAR</th>	
						</tr>
						<tr>
						</tr>
					</thead>
					<tbody>
					<?php
						$view = mysqli_query($conn, "SELECT * FROM tpaymentrequest_installment WHERE PaymentRequestNo = '".$dataHeader['PaymentRequestNo']."' AND Install_ID = '$Installment_ID'");
						$number =1;
						while($sql = mysqli_fetch_array($view)){
						
					?>
						<tr>
							<td width="20px";><?php echo $number; ?></td>
							<td><?php echo $sql['PaymentRequestNo']."/".$sql['Install_ID'];?></td>
							<td><?php echo $sql['INTERMEDIARY_TYPE'];?></td>
							<td><?php echo $sql['Client'];?></td>
							<td><?php echo $sql['CCY'];?></td>
							<td><?php echo number_format($sql['Amount'] , 2 , '.' , ',' );?></td>
							<td><?php echo $sql['Average'];?></td>
							<td><?php echo number_format($sql['Installment'] , 2 , '.' , ',' );?></td>
							<td><?php echo number_format($sql['RemainingAmount'] , 2 , '.' , ',' );?></td>
							<td><?php echo $sql['VoucherNo'];?></td>
							<td><?php echo $sql['PAID_DATE'];?></td>
						</tr> 
							<!-- <a href="home?page=summaryreport&id=<?php echo $sql['ID'] ?>&status=<?php echo $queryRow['status']?>" class="btn btn-primary">Detail</a> -->
						<?php 
						$number++;}
						?>
					</tbody>	
				</table>
				<?php 
					$btnquery1 	=	mysqli_query($conn, "SELECT * FROM security_user_group where username = '".$_SESSION['username']."' AND group_id = (SELECT Value FROM mgeneral_table WHERE Code='PaymentRequest_GROUP' AND Value=1) ");
					$btnquery2 	=	mysqli_query($conn, "SELECT * FROM security_user_group where username = '".$_SESSION['username']."' AND group_id = (SELECT Value FROM mgeneral_table WHERE Code='PaymentRequest_GROUP' AND Value=2) ");
					$btnquery3 	=	mysqli_query($conn, "SELECT * FROM security_user_group where username = '".$_SESSION['username']."' AND group_id = (SELECT Value FROM mgeneral_table WHERE Code='PaymentRequest_GROUP' AND Value=3) ");
					$btnquery4 	=	mysqli_query($conn, "SELECT * FROM security_user_group where username = '".$_SESSION['username']."' AND group_id = (SELECT Value FROM mgeneral_table WHERE Code='PaymentRequest_GROUP' AND Value=4) ");
					$btnquery5 	=	mysqli_query($conn, "SELECT * FROM security_user_group where username = '".$_SESSION['username']."' AND group_id = (SELECT Value FROM mgeneral_table WHERE Code='PaymentRequest_GROUP' AND Value=5) ");
					
					$btn1 		=	mysqli_num_rows($btnquery1);
					$btn2 		=	mysqli_num_rows($btnquery2);
					$btn3 		=	mysqli_num_rows($btnquery3);
					$btn4 		=	mysqli_num_rows($btnquery4);
					$btn5 		=	mysqli_num_rows($btnquery5);

					// <a href='#!' id='btn-revise-checker' class='btn btn-primary' style='float:right;'></i> REVISE</a>
					
					if ($btn3 > 0 && $dataHeader['STATUS_INS'] == 'INSTALLMENT SUBMIT' && $dataHeader['VoucherNo'] == NULL) 
					{
						echo "<a href='#!' id='btn-reject-checker' class='btn btn-primary' style='float:right;'></i> REJECT</a>
							
						  	<a href='#!' id='btn-approve-checker' class='btn btn-primary' style='float:right;'></i> APPROVE</a>";
					}
					if ($btn4 > 0 && $dataHeader['STATUS_INS'] == 'INSTALLMENT CHECKER') 
					{
						echo "<a href='#!' id='btn-approve-approve' class='btn btn-primary' style='float:right;'></i> APPROVE</a>";
					}
					if($btn5 > 0 && $dataHeader['STATUS_INS'] == 'INSTALLMENT APPROVE' && $dataHeader['VoucherNo'] == ''){
						echo "<a href='#!' id='btn-paid' class='btn btn-primary' style='float:right;'></i>PAID</a>";
					}
				?>
			</div>
		
	</div>PR08
</div>


<script type="text/javascript">
$(document).ready(function(){
  $('#btn-approve-checker').click(function(){
    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons:{
      	cancel: true,
	    confirm: "APPROVE",
  		},
    })
    .then((willDelete) => {
      if (willDelete) {
        window.location.assign("fungsi/paymentrequest/approveinstallment.php?function=CHECKER&id=<?php echo $dataHeader['PaymentRequestNo']; ?>&id_installment=<?php echo $dataHeader['Install_ID']; ?>");
      } else {
        return false;
      }
    });
  });
 $('#btn-reject-checker').click(function(){
    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
       swal("Write remarks something here:", {
          content: "input",
        })
        .then((value) => {
          var comment = `${value}`;
          if(comment.length > 0)
          {
            window.location.assign("fungsi/paymentrequest/approveinstallment.php?function=REJECTCHECKER&id_installment=<?php echo $dataHeader['Install_ID']; ?>&id=<?php echo $dataHeader['PaymentRequestNo'];?>&remarks="+comment+" ");
          }
          else
          {
            swal('Remarks must fill', '', 'error');
          }
        });
      } else {
        return false;
      }
    });
  });
$('#btn-revise-checker').click(function(){
    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        swal("Write remarks something here:", {
          content: "input",
        })
        .then((value) => {
          var comment = `${value}`;
          if(comment.length > 0)
          {
            window.location.assign("fungsi/paymentrequest/approveinstallment.php?function=REVISECHECKER&id_installment=<?php echo $dataHeader['Install_ID']; ?>&id=<?php echo $dataHeader['PaymentRequestNo']; ?>&remarks="+comment+"");
          }
          else
          {
            swal('Remarks must fill', '', 'error');
          }
        });
        
      } else {
        return false;
      }
    });
  });
});
$('#btn-approve-approve').click(function(){
    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.location.assign("fungsi/paymentrequest/approveinstallment.php?function=APPROVE&id=<?php echo $dataHeader['PaymentRequestNo']; ?>&id_installment=<?php echo $dataHeader['Install_ID']; ?>");
      } else {
        return false;
      }
    });
  });
</script>

<?php 
}
else
{
	echo "eror";
	echo "<script>javascript:history.back()</script>";
}
?>