<?php 
	if (isset($_GET['id']) && !empty($_GET['id'])) 
	{
		$id 	=	$_GET['id'];
		$status =	$_GET['status'];
		$_SESSION['REQ_ID']	=	$id;
	
		
		$queryHeader 		= "SELECT * FROM tpaymentrequestheader WHERE ISACTIVE=1 AND ID = '$id' ";
		$data				= mysqli_query($conn, $queryHeader) or die(mysqli_error($conn));
		$dataHeader			= mysqli_fetch_array($data);
		$queryHeader2 		= "SELECT * FROM tpaymentrequest_installment WHERE PaymentRequestNo = '".$dataHeader['PaymentRequestNo']."'";
		$data2				= mysqli_query($conn, $queryHeader2) or die(mysqli_error($conn));
		$dataHeader2		= mysqli_fetch_array($data2);
		
		$createWhoData	 =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$dataHeader[CREATED_BY]'");
		$checkerWhoData	 =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$dataHeader[CHECKER_BY]'");
		$approvalWhoData =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$dataHeader[APPROVAL_BY]'");
		
		$createWho = mysqli_fetch_array($createWhoData);
		$checkerWho = mysqli_fetch_array($checkerWhoData);
		$approvalWho = mysqli_fetch_array($approvalWhoData);	
?>

<div class="print" style="font-size : 11px;">
	<div class="container">
		<div class="head">
			<h3>PAYMENT REQUEST</h3>
			<p> <?php echo $dataHeader['PaymentRequestNo']; ?></p>
		</div>

			<H3>DETAIL INSTALLMENT</H3>
			<br><br>
			<div class="isi">
				<?php
				 $SQLperCcy = mysqli_query($conn, "SELECT a.CCY 
				 									FROM tpaymentrequest_installment_detail a
													JOIN tpaymentrequest_installment b ON b.id = a.installment_id 
				 									where b.PaymentRequestNo = '".$dataHeader['PaymentRequestNo']."' GROUP BY CCY ");
				 while ($fetchPerCcy = mysqli_fetch_array($SQLperCcy)) {
				  	
				?>
				<table class="table" border="1">
					<thead>
						<tr>
							<th rowspan="2">NO</th>
							<th rowspan="2">NO.PAYMENT REQUEST</th>
							<th rowspan="2">SOB</th>
							<th rowspan="2">CLIENT</th>
							<th rowspan="2">CCY</th>
							<th rowspan="2">GROSS PREMIUM</th>
							<th rowspan="2">JUMLAH YANG DIBAYARKAN(%)</th>
							<th rowspan="2">AVERAGE(%)</th>
							<th rowspan="2">DUE DATE</th>
							<!-- <th rowspan="2">TANGGAL BAYAR</th>	 -->
							<!-- <th rowspan="2">STATUS</th> -->
						</tr>
						<tr>
						</tr>
					</thead>
					<tbody>
					<?php
						$SQL_INSTALLMENT = "SELECT b.PaymentRequestNo, a.installment_ke, b.INTERMEDIARY_TYPE, b.Client, a.CCY, a.Amount, a.Installment, a.Average, DATE_FORMAT(a.Due_Date, '%d-%m-%Y') AS Due_Date 
													FROM tpaymentrequest_installment_detail a
													JOIN tpaymentrequest_installment b ON b.id = a.installment_id
													WHERE b.PaymentRequestNo = '".$dataHeader['PaymentRequestNo']."' 
													AND a.CCY = '".$fetchPerCcy['CCY']."'";
						$view = mysqli_query($conn, $SQL_INSTALLMENT);
						$number =1;
						while($sql = mysqli_fetch_array($view)){
						
					?>
						<tr>
							<td style="text-align: center;" width="20px"><?php echo $number; ?></td>
							<td><?php echo $sql['PaymentRequestNo']."/".$sql['installment_ke'];?></td>
							<td><?php echo $sql['INTERMEDIARY_TYPE'];?></td>
							<td><?php echo $sql['Client'];?></td>
							<td><?php echo $sql['CCY'];?></td>
							<td style="text-align: right;"><?php echo number_format($sql['Amount'] , 2 , '.' , ',' );?></td>
							<td style="text-align: right;"><?php echo number_format($sql['Installment'] , 2 , '.' , ',' );?></td>
							<td style="text-align: center;"><?php echo $sql['Average'];?>%</td>
							<td style="text-align: center;"><?php echo $sql['Due_Date'];?></td>
						</tr>
						<?php 
						$number++;}
						$SQLtotal  = mysqli_query($conn ,"SELECT a.CCY, SUM(a.Installment) AS Installment, 
																		SUM(a.Average) AS Average
														FROM tpaymentrequest_installment_detail a
														JOIN tpaymentrequest_installment b ON b.id = a.installment_id 
														where b.PaymentRequestNo = '".$dataHeader['PaymentRequestNo']."' AND a.CCY = '".$fetchPerCcy['CCY']."' ");
						$fetchTotal = mysqli_fetch_array($SQLtotal);
						?>
						<tr>
							<td colspan="4" style="text-align: right;">TOTAL</td>
							<td><b><?php echo $fetchTotal['CCY']; ?></b></td>
							<td style="text-align: right;"></td>
							<td style="text-align: right;"><b><?php echo $fetchTotal['Installment']; ?></b></td>
							<td style="text-align: center;"><b><?php echo $fetchTotal['Average']; ?>%</b></td>
						</tr>
					</tbody>	
				</table>
			<?php } ?>
				<hr>
				<?php include "page/installment/viewInstallmentFooter.php"; ?>
				<div class="col-lg-12" style="text-align: center;">
					<?php
					if ($_SESSION['group_id'] == 2) {
					?>
						<a href="#!" id="btn-installment-print" class="btn btn-info">Print <i class="fa fa-print" aria-hidden="true"></i></a>
					<?php
					} elseif ($_SESSION['group_id'] == 3) {
					?>
						<a href="#!" id="btn-checker-installment-approve" class="btn btn-success">Approve</a>
						<a href="#!" id="btn-checker-installment-revise" class="btn btn-info">Revise</a>
						<a href="#!" id="btn-checker-installment-reject" class="btn btn-danger">Reject</a>
					<?php 
					} elseif ($_SESSION['group_id'] == 4) {
					?>
						<a href="#!" id="btn-approval-installment-approve" class="btn btn-success">Approve</a>
						<a href="#!" id="btn-approval-installment-revise" class="btn btn-info">Revise</a>
						<a href="#!" id="btn-approval-installment-reject" class="btn btn-danger">Reject</a>
					<?php
					}
					?>
				</div>
			</div>
		
	</div>PR08
</div>

<script>
	$(document).ready(function(){

		// ================== BUTTON UNTUK MAKER PRINT ==================

		$('#btn-installment-print').click(function(){
		    swal({
		      title: "Are you sure?",
		      icon: "warning",
		      buttons:{
		      	cancel: true,
			    confirm: "PRINT",
		  		},
		    })
		    .then((willDelete) => {
		      if (willDelete) {
		        window.location.assign("report/report_installment_list_detail.php?id=<?php echo $id; ?>");
		      } else {
		        return false;
		      }
		    });
	    });

		// ================== BUTTON UNTUK CHECKER ==================

		$('#btn-checker-installment-approve').click(function(){
		    swal({
		      title: "Are you sure?",
		      icon: "warning",
		      buttons:{
		      	cancel: true,
			    confirm: "APPROVE",
		  		},
		    })
		    .then((willDelete) => {
		      if (willDelete) {
		        window.location.assign("fungsi/installment/installmentApproval.php?function=INSTALLMENT_CHECKER&id=<?php echo $id; ?>&idInstall=<?php echo $idInstallment; ?>&ccy=<?php echo $CCY;?>");
		      } else {
		        return false;
		      }
		    });
	    });

	    $('#btn-checker-installment-revise').click(function(){
		    swal({
		      title: "Are you sure?",
		      icon: "warning",
		      buttons:{
		      	cancel: true,
			    confirm: "APPROVE",
		  		},
		    })
		    .then((willDelete) => {
		      if (willDelete) {
		        swal("Write remarks something here:", {
		          content: "input",
		        })
		        .then((value) => {
		          var comment = `${value}`;
		          if(comment.length > 0)
		          {
		            window.location.assign("fungsi/installment/installmentApproval.php?function=INSTALLMENT_REVISECHECKER&id=<?php echo $id; ?>&idInstall=<?php echo $idInstallment; ?>&ccy=<?php echo $CCY;?>&remarks="+comment+"");
		          }
		          else
		          {
		            swal('Remarks must fill', '', 'error');
		          }
		        });

		        
		      } else {
		        return false;
		      }
		    });
	    });

	    $('#btn-checker-installment-reject').click(function(){
		    swal({
		      title: "Are you sure?",
		      icon: "warning",
		      buttons:{
		      	cancel: true,
			    confirm: "APPROVE",
		  		},
		    })
		    .then((willDelete) => {
		      if (willDelete) {
		        swal("Write remarks something here:", {
		          content: "input",
		        })
		        .then((value) => {
		          var comment = `${value}`;
		          if(comment.length > 0)
		          {
		            window.location.assign("fungsi/installment/installmentApproval.php?function=INSTALLMENT_REJECTCHECKER&id=<?php echo $id; ?>&idInstall=<?php echo $idInstallment; ?>&ccy=<?php echo $CCY;?>&remarks="+comment+"");
		          }
		          else
		          {
		            swal('Remarks must fill', '', 'error');
		          }
		        });

		        
		      } else {
		        return false;
		      }
		    });
	    });

	    // ================== BUTTON UNTUK APPROVAL ==================

	    $('#btn-approval-installment-approve').click(function(){
		    swal({
		      title: "Are you sure?",
		      icon: "warning",
		      buttons:{
		      	cancel: true,
			    confirm: "APPROVE",
		  		},
		    })
		    .then((willDelete) => {
		      if (willDelete) {
		        window.location.assign("fungsi/installment/installmentApproval.php?function=INSTALLMENT_APPROVE&id=<?php echo $id; ?>&idInstall=<?php echo $idInstallment; ?>&ccy=<?php echo $CCY;?>");
		      } else {
		        return false;
		      }
		    });
	    });

	    $('#btn-approval-installment-revise').click(function(){
		    swal({
		      title: "Are you sure?",
		      icon: "warning",
		      buttons:{
		      	cancel: true,
			    confirm: "APPROVE",
		  		},
		    })
		    .then((willDelete) => {
		      if (willDelete) {
		        swal("Write remarks something here:", {
		          content: "input",
		        })
		        .then((value) => {
		          var comment = `${value}`;
		          if(comment.length > 0)
		          {
		            window.location.assign("fungsi/installment/installmentApproval.php?function=INSTALLMENT_REVISEAPPROVE&id=<?php echo $id; ?>&idInstall=<?php echo $idInstallment; ?>&ccy=<?php echo $CCY;?>&remarks="+comment+"");
		          }
		          else
		          {
		            swal('Remarks must fill', '', 'error');
		          }
		        });

		        
		      } else {
		        return false;
		      }
		    });
	    });

	    $('#btn-approval-installment-reject').click(function(){
		    swal({
		      title: "Are you sure?",
		      icon: "warning",
		      buttons:{
		      	cancel: true,
			    confirm: "APPROVE",
		  		},
		    })
		    .then((willDelete) => {
		      if (willDelete) {
		        swal("Write remarks something here:", {
		          content: "input",
		        })
		        .then((value) => {
		          var comment = `${value}`;
		          if(comment.length > 0)
		          {
		            window.location.assign("fungsi/installment/installmentApproval.php?function=INSTALLMENT_REJECTAPPROVE&id=<?php echo $id; ?>&idInstall=<?php echo $idInstallment; ?>&ccy=<?php echo $CCY;?>&remarks="+comment+"");
		          }
		          else
		          {
		            swal('Remarks must fill', '', 'error');
		          }
		        });

		        
		      } else {
		        return false;
		      }
		    });
	    });

	});	
</script>

<?php 
}
else
{
	echo "eror";
	echo "<script>javascript:history.back()</script>";
}
?>