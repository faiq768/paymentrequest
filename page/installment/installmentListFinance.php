<?php
  $userGroup = mysqli_query($conn, "SELECT a.username, b.group_id FROM security_user a JOIN security_user_group b ON b.username = a.username WHERE a.username = '".$_SESSION['username']."'");
                 $groupidData = mysqli_fetch_array($userGroup);
?>

<div class="container">
  <div class="widget-content">
    <div class="tab-content">
      <div class="tab-pane active" id="formcontrols">
        <div class="widget widget-table action-table">
          <div class="widget-content">
            <h3>INSTALLMENT LIST</h3>
            <table id="table-installment-list-finance" class="display cell-border" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>NO</th>
                  <th>PAYMENT REQUEST NO</th>
                  <th>INTERMEDIARY NAME</th>
                  <th>BENEFICIARY NAME</th>
                  <th>CCY</th>
                  <th>REQUEST DATE</th>
                  <th>REQUEST STATUS</th>
                  <th>ACTION</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>   
      </div> 
    </div>
  </div>
</div>

<div class="modal fade" id="lookup-installment-paid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Installment Paid</h4>
          </div>
          <div class="modal-body" id="data-installment-paid" style="text-align: center;">
            <form action="fungsi/installment/financeActionInstallment.php" method="post">
              <input type="hidden" name="id" class="idPaid">
              <input type="text" name="voucherNo" class="form-control span2 voucheNo" placeholder="Voucher No" required>
              <input type="text" name="voucherDate" class="form-control date span2 voucherDate" placeholder="Voucher Date" required>
              <input name="btnPaid" type="submit" class="btn-primary" value="Paid">
            </form>
          </div>
      </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
  // var table = $('#query-table').DataTable();

  var tableInstallmentList = $("#table-installment-list-finance").DataTable({
          "paging":   true,
          "ordering": true,
          "scrollY": '45vh',
          "scrollX": true,
          "processing": true,
          "ajax": "ajax/ajax.tableInstallmentListFinance.php",
          "columnDefs":
          [
            {
              "targets": 0, 
              "data": null,
               // "defaultContent": "<button class='btn btn-success btn-xs tblEdit'>Edit / Delete</button>"
               render: function (data,type,row,meta){
                return meta.row + meta.settings._iDisplayStart + 1;
               }
            },
            {
              "targets":[0,1,4,5,6,7],
              "className" : "text-center"
            },
            {
              "targets":5,
              render: function(data){
                let date = new Date(data);
                return date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()
              }
            },            
            {
              "targets" : 7,
              "width"   : "10%",
              render : function(data){
                var id                = data.split(";")[0];
                var PaymentRequestNo  = data.split(";")[1];
                var Installment_ke    = data.split(";")[2];
                var ccy               = data.split(";")[3];
                
                // var dataT = $.ajax({
                //       type  :   'POST',
                //       url   :   'ajax/ajaxButtonInstallment.php',
                //       data  :   {"id":id,"PaymentRequestNo":PaymentRequestNo},
                //       async: false,
                //   }).responseText;

                $(".btn-paid").click(function(){
                    $(".idPaid").val($(this).attr('data')+";"+$(this).attr('data-ccy')+";"+$(this).attr('installment-ke'));
                    $(".voucheNo").val("");
                    $(".voucherDate").val("");
                });

                return "<a data='"+PaymentRequestNo+"' installment-ke='"+Installment_ke+"' data-ccy='"+ccy+"' data-toggle='modal' data-target='#lookup-installment-paid' href='#!' class='btn btn-primary btn-paid'>PAID</a>"
              }
            }
          ],
          "serverSide": true,
          "language": {
            "lengthMenu": "",
            "zeroRecords": "Nothing found - sorry",
            "info": "Showing page _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }
        });

        tableInstallmentList.order([1,'asc']).draw();
  

    $('#query-table-installment-list').on( 'click', 'button', function () {
      
      var data = table.row( $(this).parents('tr') ).data();
      var idx = data[0] - 1;
      var No = data[0];
      var ccy = data[3].replace(/\s/g, '').substr(22,3);
      var PayReqNo = data[1].substr(0,15);
      var InstallID = data[1].substr(18,19);
      var PayReqID = data[10];
      var CountAlert = 0;
      var dataRow = table.$('input, VoucherNo').serialize();

      var res = dataRow.split('&');
      var colVNo = res[((((idx+1)*2)-1)-1)];
      var colVDate = res[(((idx+1)*2)-1)];

      var res1 = colVNo.split('=');
      var colVNoValue = res1[1];
      
      var res2 = colVDate.split('=');
      var colVDateValue = res2[1];
      
      if(colVNoValue == '')
      {
        swal('Voucher No is Empty', '', 'error');
        return false;
      }
      else if(colVDateValue == '')
      {
        swal('Date is Empty', '', 'error');
        return false; 
      }
      else if(colVNoValue != '' && colVDateValue != '')
      {
        // console.log();
        $.ajax({
          type  :   'POST',
          url   :   'ajax/ajax_type2.php',
          data  :   'VoucherNoInstall='+colVNoValue,
          success : function(data){
            if(data != '')
            {
              if(!confirm(colVNoValue + ' Already Exists. Are you sure you want to continue ?')){
                return false;
              }
            }
            window.location.assign('fungsi/installment/financeActionInstallment.php?installID='+ InstallID +'&id='+ PayReqNo +'&ccy='+ ccy +'&VoucherNo='+ colVNoValue +'&VoucherDate='+ colVDateValue +' ');
          }
        });
        //window.location.assign('fungsi/paymentrequest/financeAction.php?id='+ PayReqID +'&VoucherNo='+ colVNoValue +'&VoucherDate='+ colVDateValue +' ');
      }
      
    });



});
</script>