<?php 
	if (isset($_GET['id']) && !empty($_GET['id'])) 
	{
		$id 	=	$_GET['id'];
		$status =	$_GET['status'];
		$_SESSION['REQ_ID']	=	$id;
	

		$queryInstall 		= "SELECT DISTINCT a.PaymentRequestNo,
								               a.ID,
								               a.IntermediaryType,
								               a.IntermediaryName,
								               a.IntermediaryCode,
								               b.CCY
							FROM   tpaymentrequestheader a
								   JOIN tpaymentrequestdetail b
								             ON b.paymentrequestid = a.id
							WHERE  a.id = '$id'  ";

		$dataInstall		=  mysqli_query($conn, $queryInstall) or die(mysqli_error($conn));
		$queryRowInstall 	=  mysqli_fetch_array($dataInstall);
		
?>

		<div class="container">
			
			<div class="installment-menu">
				<h3>INSTALLMENT</h3> <br>
				<div class="table-installment">
					<h4>PAYMENT REQUEST NUMBER : <?php echo $queryRowInstall['PaymentRequestNo']; ?></h4>
					<h4>GROSS PREMIUM : </h4>
					<?php

						$CCYSQLI  		= "SELECT CCY, Sum(Amount) AS TOTAL FROM tpaymentrequestdetail 
                                            where CCY
                                            NOT IN( 
                                            SELECT CCY FROM (
                                            SELECT b.CCY, a.STATUS_INS 
											FROM tpaymentrequest_installment a
											JOIN tpaymentrequest_installment_detail b 
											ON b.installment_id = a.id 
                                            where a.PaymentRequestNo = '".$queryRowInstall['PaymentRequestNo']."'
                                            GROUP BY b.CCY, a.STATUS_INS
                                            ) ABC WHERE ((STATUS_INS = 'PAID') OR STATUS_INS = 'CREATE' OR STATUS_INS = 'SUBMIT' OR STATUS_INS = 'CHECKER' OR STATUS_INS = 'APPROVAL')
                                            ) AND PaymentRequestID = '".$queryRowInstall['ID']."'
                                            GROUP BY CCY 
                                           ";
                        $ccyQuery 		= mysqli_query($conn, $CCYSQLI) or die(mysqli_error($conn));
						$rowCCY 		= mysqli_num_rows($ccyQuery);

					 	$grossQuery = mysqli_query($conn, "SELECT b.CCY,  Sum(b.Amount) AS TOTAL
															FROM tpaymentrequestheader a
															INNER JOIN tpaymentrequestdetail b
															ON b.PaymentRequestID = a.id
															WHERE  a.ID = '$id'
															AND b.CCY NOT IN (
																SELECT CCY FROM (
																	SELECT b.CCY, a.STATUS_INS 
																	FROM tpaymentrequest_installment a
																	JOIN tpaymentrequest_installment_detail b 
																	ON b.installment_id = a.id
																	where a.PaymentRequestNo = '".$queryRowInstall['PaymentRequestNo']."'
																	GROUP BY b.CCY, a.STATUS_INS
																) ABC WHERE (STATUS_INS = 'PAID' OR STATUS_INS = 'CREATE' OR STATUS_INS = 'SUBMIT' OR STATUS_INS = 'CHECKER' OR STATUS_INS = 'APPROVAL')
															)
															GROUP  BY b.CCY ");

					 	while ($grossData = mysqli_fetch_array($grossQuery)) {
					?>
						<ul>
							<li><?php echo $grossData['CCY']?> : <?php echo number_format($grossData['TOTAL'], 2 , '.' , ',' ); ?></li>
						</ul>
					<?php 
						} //TUTUP WHILE
					?>
					<form action="fungsi/installment/installment_save.php" method="post" >
						<input type="hidden" name="PaymentRequestID" value="<?php echo $id;?>">
						<input type="hidden" name="PaymentRequestNo" value="<?php echo $queryRowInstall['PaymentRequestNo']; ?>">
						<?php 
							if ($rowCCY == 1) {
								$ccyQueryData 	= mysqli_fetch_array($ccyQuery);
						   		echo "<input type='hidden' id='Gross' value='".$ccyQueryData['TOTAL']."'>";
						 	} else {
						 		echo "<input type='hidden' id='Gross' value=''>";	
							} 
						?>
						<table border="1" class="table table-collapse">
							<thead>
								<tr>
									<th>CURRENCY</th>
									<th>INSTALLMENT</th>
									<th>AVERAGE</th>
									<th>DUE DATE</th>
								</tr>
							</thead>
							<div id="show">
																
							</div>
							<tbody id="body-table-install">				
								<tr>
									<td>
										<select name="ccy" id="ccy">
											<?php 
											if ($rowCCY == 1) {
												echo "<option value='".$ccyQueryData['CCY']."'>".$ccyQueryData['CCY']."</option>";
											}else{

											?>
												<option>-- Choose Currency --</option>
												<?php
												while ($ccyData = mysqli_fetch_array($ccyQuery)) {
												?>
												<option value="<?php echo $ccyData['CCY'].';'.$ccyData['TOTAL'] ?>"><?php echo $ccyData['CCY'] ?></option>
												<?php
												} 
											} 
											?>
										</select>
									</td>
									<td>
										<input name="amount" type="text" placeholder="" id="amount" class="form-control span2 currency"  autocomplete="off" readonly="readonly">
									</td>
									<td>
										<input name="average" type="text" placeholder="0.00%" id="average" class="form-control span2"  autocomplete="off" readonly="readonly">
									</td>
									<td>
										<input name="dueDate" type="text" placeholder="" id="dueDate" class="form-control span2 date"  autocomplete="off" readonly="readonly">
									</td>
									
								</tr>
							</tbody>

							<tfoot>
								<tr>
									<td colspan="4" class="right-button"> 
										<input type="button" id="add-install" class="btn btn-primary btn-margin" value="Add">
										<button type="button" class="btn btn-danger" onclick="javascript:history.back()">Cancel</button>
									</td>
								</tr>
							</tfoot>
						</table>
						<center>			
						<table border="1" id="detailInstallment" class="table table-hover" style="width: 70%">
							<thead>
								<th style="text-align: center;">No</th>
								<th style="text-align: center;">Currency</th>
								<th style="text-align: center; width: 200px;">Installment</th>
								<th style="text-align: center;">%</th>
								<th style="text-align: center;">Due Date</th>
								<th style="text-align: center;">Action</th>
							</tbody>
							<tbody id="tbody">
								
							</tbody>
							<tfoot>
								<tr>
									<td colspan="2" style="text-align: right;">Total Installment</td>
									<td style="text-align: right;">
										<span id="tdTotalInstallment" style="color: red;"></span>
										<input type="hidden" id="totalInstallment" name="totalInstallment">
									</td>
									<td colspan="3" style="text-align: center;"><input type="submit" id="submit-installment" name="save_installment" class="btn btn-primary btn-margin hidden" value="Submit"></td>
								</tr>
							</tfoot>
						</table>
						</center>
					</form>
				</div>
				<!-- <a href="?page=installmentdetail&id=<?php echo $id ?>&status=<?php echo $status ?>" class="btn btn-primary">Detail</a> -->
			</div>
		</div>

		<script type="text/javascript">

		$(document).ready(function(){

			if ($("#Gross").val() != "") {
				$("#amount").removeAttr("readonly");
					$("#average").removeAttr("readonly");
					$("#dueDate").removeAttr("readonly");
			}

			$('#ccy').change(function() { 
				if ($(this).val() != "-- Choose Currency --") {
					$("#amount").removeAttr("readonly");
					$("#average").removeAttr("readonly");
					$("#dueDate").removeAttr("readonly");
				}else{
					$("#amount").attr("readonly","readonly");
					$("#average").attr("readonly","readonly");
					$("#dueDate").attr("readonly","readonly");
				}
				$("#amount").val("");
				$("#average").val("");
				$("#dueDate").val("");
			    var data 	= $(this).val();
			    var ccy 	= data.split(";")[0];
			    var gross 	= data.split(";")[1];
			    $("#Gross").val(gross); 
			    // var PaymentRequestNo  = '<?php echo $queryRowInstall['PaymentRequestNo']; ?>';
			    // var PaymentRequestID  = '<?php echo $id; ?>';
			    // console.log(ccy+","+PaymentRequestNo+","+PaymentRequestID);
			    
		    });

			$('#amount').keyup(function(){
				var amount= document.getElementById('amount').value;
		   		var gross = document.getElementById('Gross').value;
		   		var ccy  = document.getElementById('ccy').value;
				var jumlah_persentase = amount / gross * 100;
				
				if (ccy == '-- Choose Currency --') {
					swal('PLEASE CHOOSE CURRENCY', '', 'warning');
					document.getElementById('amount').value = '';
					document.getElementById('average').value = '';
				} else{
					var totalIns = $("#totalInstallment").val();
					var gross_am = $("#Gross").val();
					if (isNaN(totalIns) == false) {
						totalIns = 0;
					}
					if (parseFloat(totalIns)+parseFloat(jumlah_harga) > gross_am ) {
						swal("INSTALLMENT MUSTN'T BE MORE THAN BALANCE", "", "warning").then(function() {
							// Redirect the user
								$('#amount').val("");
								$('#amount').focus();
								$('#average').val("");
							});
					}else{
						document.getElementById('average').value = jumlah_persentase.toFixed(2);
					}
				}
				
				
			});

			$('#average').keyup(function(){
				var amount 	= document.getElementById('amount').value;
				var average = document.getElementById('average').value;
		   		var gross 	= document.getElementById('Gross').value;
		   		var ccy  	= document.getElementById('ccy').value;
				var jumlah_harga =  average / 100 * gross;

				if (ccy == '-- Choose Currency --') {
					swal('PLEASE CHOOSE CURRENCY', '', 'warning');
					document.getElementById('amount').value = '';
					document.getElementById('average').value = '';
				}else{
					var totalIns = $("#totalInstallment").val();
					var gross_am = $("#Gross").val();
					if (isNaN(totalIns) == false) {
						totalIns = 0;
					}
					if (parseFloat(totalIns)+parseFloat(jumlah_harga) > gross_am ) {
						swal("INSTALLMENT MUSTN'T BE MORE THAN BALANCE", "", "warning").then(function() {
							// Redirect the user
								$('#average').val("");
								$('#average').focus();
								$('#amount').val("");
							});
					}else{
						document.getElementById('amount').value = jumlah_harga.toFixed(2);
					}
				}
				
				
			});
			$( '#amount' ).keydown( function ( event ) {


			var keyCode = event .keyCode || event .which;


			if (keyCode == 9) {

			return false;

			event .preventDefault();


			}

			});

			$("#amount,#average").keydown(function (e) {
		        // Allow: backspace, delete, tab, escape, enter and .
		        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
		             // Allow: Ctrl/cmd+A
		            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
		             // Allow: Ctrl/cmd+C
		            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
		             // Allow: Ctrl/cmd+X
		            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
		             // Allow: home, end, left, right
		            (e.keyCode >= 35 && e.keyCode <= 39)) {
		                 // let it happen, don't do anything
		                 return;
		        }
		        // Ensure that it is a number and stop the keypress
		        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		            e.preventDefault();
		        }
		    });

			var rowIdx = 0; 
			var tableInstallment = document.getElementById("detailInstallment"), sumVal = 0;
  
			// jQuery button click event to add a row. 
			$('#add-install').on('click', function () { 

			    // Adding a row inside the tbody. 
			    var ccy 	= $("#ccy").val().split(";")[0];
			    var amount 	= $('#amount').val();
			    var average = $("#average").val();
			    var dueDate = $("#dueDate").val();

			    if (amount.trim() == null || amount.trim() == "") {
			    	$("#amount").focus(); return false;
			    }

			    if (dueDate.trim() == null || dueDate.trim() == "") {
			    	$("#dueDate").focus(); return false;
			    }
			    // var remaining = $("#sisa").val();
			    // remaining = remaining - amount;
			    // $("#sisa").val(remaining);

			    $('#tbody').append(`
			        <tr id="R${++rowIdx}" class="row_to_clone"> 
			          <td class="row-index" style="text-align: center;">
			          <span class="index-value">${rowIdx}</span>
			          <input name='installmentItem[]' value='${rowIdx+";"+ccy+";"+amount+";"+average+";"+dueDate}' type=checkbox class='pilih hidden' checked>
			          </td>
			          <td style="text-align: center;">${ccy}</td>
			          <td style="text-align: right;" id="txtAmount">
			          	<span>${parseFloat(amount).toFixed(2)}</span>
			          	<input type="hidden" class="txt" value="${parseFloat(amount).toFixed(2)}" readonly></td>
			          <td style="text-align: center;">${average}</td>
			          <td style="text-align: center;">${dueDate}</td> 
			          <td style="text-align: center;"> 
			            <button class="btn btn-danger remove" type="button">Remove</button> 
			          </td> 
			        </tr>`); 

			    calculateSum();

			    $('#amount').val("");
			    $('#average').val("");
			    $('#dueDate').val("");

			    var totalInstallment = $("#totalInstallment").val();
			    var totalAmount = $("#Gross").val();
			    var tot = totalAmount-totalInstallment;

			    if (tot == 0) {
			    	$(this).addClass("hidden");
			    	$("#submit-installment").removeClass("hidden");
			    }

			});

			// Node.js program to demonstrate the 
			// Node.js filehandle.read() Method 
			  
			// jQuery button click event to remove a row 
			$('#tbody').on('click', '.remove', function () { 
			  
			    // Getting all the rows next to the  
			    // row containing the clicked button 
			    var child = $(this).closest('tr').nextAll(); 

			    calculateRemoveSum();
			  
			    // Iterating across all the rows  
			    // obtained to change the index 
			    child.each(function () { 
			          
			        // Getting <tr> id. 
			        var id = $(this).attr('id'); 
			  
			        // Getting the <p> inside the .row-index class. 
			        var idx = $(this).children('.row-index').children('.index-value'); 
			        console.log(idx);
			  
			        // Gets the row number from <tr> id. 
			        var dig = parseInt(id.substring(1)); 
			  
			        // Modifying row index. 
			        idx.html(`${dig - 1}`); 
			  
			        // Modifying row id. 
			        $(this).attr('id', `R${dig - 1}`); 
			    }); 
			  
			    // Removing the current row. 
			    $(this).closest('tr').remove(); 
			  
			    // Decreasing the total number of rows by 1. 
			    rowIdx--; 

			    var totalInstallment = $("#totalInstallment").val();
			    var totalAmount = $("#Gross").val();
			    var tot = totalAmount-totalInstallment;

			    if (tot != 0) {
			    	$("#add-install").removeClass("hidden");
			    	$("#submit-installment").addClass("hidden");
			    }

			}); 

		});

		function calculateSum() {
		  var sum = 0;
		  //iterate through each textboxes and add the values
		  $(".txt").each(function() {

		    //add only if the value is number
		    if (!isNaN(this.value) && this.value.length != 0) {
		      sum += parseFloat(this.value);
		    }

		  });
		  // console.log(sum.toFixed(2));
		  $("#totalInstallment").val(sum.toFixed(2));
		  $("#tdTotalInstallment").text(faiqFormatMoney(sum.toFixed(2),2,",","."));
		}

		function calculateRemoveSum() {
		  var sum = 0;
		  var kurang = 0;
		  //iterate through each textboxes and add the values
		  $(".txt").each(function() {

		    //add only if the value is number
		    if (!isNaN(this.value) && this.value.length != 0) {
		      sum -= parseFloat(this.value);
		    }

		    var total = (sum+parseFloat(this.value))*(0-1);

		    $("#totalInstallment").val(total.toFixed(2));
		  	$("#tdTotalInstallment").text(faiqFormatMoney(total.toFixed(2),2,",","."));

		  });
		  
		}

		
		</script>

<?php 
		// }
	}
	else
	{
		echo "eror";
		echo "<script>javascript:history.back()</script>";
	}
?>