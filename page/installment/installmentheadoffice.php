<?php
  $userGroup = mysqli_query($conn, "SELECT a.username, b.group_id FROM security_user a JOIN security_user_group b ON b.username = a.username WHERE a.username = '".$_SESSION['username']."'");
                 $groupidData = mysqli_fetch_array($userGroup);
?>

<div class="container">
  <div class="widget-content" style="overflow-x: auto;">
    <div class="tab-content">
      <div class="tab-pane active" id="formcontrols">
        <div class="widget widget-table action-table">
          <div class="widget-content">
            <h3>INSTALLMENT LIST</h3>
            <div class="table-responsive">
              <table id="table-installment-list-finance-paid" class="display nowrap cell-border table table-striped">
                <thead class="cf">
                  <tr>
                    <th>NO</th>
                    <th>PAYMENT REQUEST NO</th>
                    <th>INTERMEDIARY NAME</th>
                    <th>BENEFICIARY NAME</th>
                    <th>CCY</th>
                    <th>REQUEST DATE</th>
                    <th>REQUEST STATUS</th>
                    <th>PAID STATUS</th>
                    <th>NO VOUCHER</th>
                    <th>VOUCHER DATE</th>
                    <th>ACTION</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>   
      </div> 
    </div>
  </div>
</div>
<!-- ====================================== Lookup Edit No Voucher ===================================== -->
<div class="modal fade" id="lookup-installment-paid-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Installment Paid</h4>
          </div>
          <div class="modal-body" id="data-installment-paid" style="text-align: center;">
            <form action="fungsi/installment/financeActionInstallment.php" method="post">
              <input type="hidden" name="id" class="idPaid">
              <input type="text" name="voucherNo" class="form-control span2 voucheNo" placeholder="Voucher No" required>
              <input type="text" name="voucherDate" class="form-control date span2 voucherDate" placeholder="Voucher Date" required>
              <input name="btnPaidEdit" type="submit" class="btn-primary" value="Save">
            </form>
          </div>
      </div>
  </div>
</div>

<script>
  $(document).ready(function(){
    var tableInstallmentList = $("#table-installment-list-finance-paid").DataTable({
          "paging":   true,
          "ordering": true,
          "scrollY": '35vh',
          "scrollX": true,
          "processing": true,
          "serverSide": true,
          "ajax": "ajax/ajax.tableInstallmentListFinancePaid.php",
          "columnDefs":
          [
            {
              "targets": 0, 
              "data": null,
               // "defaultContent": "<button class='btn btn-success btn-xs tblEdit'>Edit / Delete</button>"
               render: function (data,type,row,meta){
                return meta.row + meta.settings._iDisplayStart + 1;
               }
            },
            {
              "targets":[0,1,4,5,6,7,9,10],
              "className" : "text-center"
            },
            {
              "targets":[5,9],
              render: function(data){
                let date = new Date(data);
                return date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()
              }
            },            
            {
              "targets" : 10,
              "width"   : "10%",
              render : function(data){
                var id                = data.split(";")[0];
                var PaymentRequestNo  = data.split(";")[1];
                var Installment_ke    = data.split(";")[2];
                var ccy               = data.split(";")[3];
                var voucherNo         = data.split(";")[4];
                var voucherDate       = data.split(";")[5];
                
                // var dataT = $.ajax({
                //       type  :   'POST',
                //       url   :   'ajax/ajaxButtonInstallment.php',
                //       data  :   {"id":id,"PaymentRequestNo":PaymentRequestNo},
                //       async: false,
                //   }).responseText;

                $(".btn-finance-detail").click(function(){
                    $(".idPaid").val($(this).attr('data')+";"+$(this).attr('data-ccy')+";"+$(this).attr('installment-ke'));
                    $(".voucheNo").val($(this).attr('voucherNo'));
                    $(".voucherDate").val($(this).attr('voucherDate'));
                });

                return "<a data='"+PaymentRequestNo+"' voucherNo='"+voucherNo+"' voucherDate='"+voucherDate+"' installment-ke='"+Installment_ke+"' data-ccy='"+ccy+"' data-toggle='modal' data-target='#lookup-installment-paid-edit' href='#!' title='Edit' class='btn-finance-detail'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a> || <a href='home?page=installmentPaidDetail&id="+id+"&installmentKe="+Installment_ke+"&ccy="+ccy+"' class='btn-finance-detail' title='Detail'><i class='fa fa-eye' aria-hidden='true'></i></a>"
              }
            }
          ],
          "language": {
            "lengthMenu": "",
            "zeroRecords": "Nothing found - sorry",
            "info": "Showing page _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }
        });

        tableInstallmentList.order([1,'asc']).draw();
  });
</script>
