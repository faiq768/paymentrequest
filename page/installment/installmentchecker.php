<?php
  $userGroup = mysqli_query($conn, "SELECT a.username, b.group_id FROM security_user a JOIN security_user_group b ON b.username = a.username WHERE a.username = '".$_SESSION['username']."'");
                 $groupidData = mysqli_fetch_array($userGroup);
?>

<div class="container">
  <div class="widget-content">
    <div class="tab-content">
      <div class="tab-pane active" id="formcontrols">
        <div class="widget widget-table action-table">
          <div class="widget-content">
            <h3>INSTALLMENT LIST</h3>
            <table id="query-table-installment-list" class="display cell-border" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>NO</th>
                  <th>PAYMENT REQUEST NUMBER </th>
                  <th>INTERMEDIARY NAME </th>
                  <th>BENEFICIARY NAME</th>
                  <th>STATUS</th>
                  <th>ACTION</th>

                  <!-- <?php 
                    if ($groupidData['group_id'] == 5) { // ACCOUNT FINANCE PUSAT
                  ?>

                  <th>No Voucher</th>
                  <th>Date</th>
                    <?php } ?> -->

                  <!-- <th></th> -->
                </tr>
              </thead>
              <tbody>
               
              </tbody>
            </table>
          </div>
        </div>   
      </div> 
    </div>
  </div>
</div>
<?php  include "page/lookup/branch.php"; ?>

<script type="text/javascript">
$(document).ready(function(){
  // var table = $('#query-table').DataTable();

  var tableInstallmentList = $("#query-table-installment-list").DataTable({
          "paging":   true,
          "ordering": true,
          "scrollY": '45vh',
          "scrollX": true,
          "processing": true,
          "ajax": "ajax/ajax.tableInstallmentList.php",
          "columnDefs":
          [
            {
              "targets": 0, 
              "data": null,
               // "defaultContent": "<button class='btn btn-success btn-xs tblEdit'>Edit / Delete</button>"
               render: function (data,type,row,meta){
                return meta.row + meta.settings._iDisplayStart + 1;
               }
            },
            {
              "targets":[0,1,4,5],
              "className" : "text-center"
            },
            
            {
              "targets" : 5,
              "width"   : "20%",
              "render" : function(data){
                var id                = data.split(";")[0];
                var PaymentRequestNo  = data.split(";")[1];
                
                var dataT = $.ajax({
                      type  :   'POST',
                      url   :   'ajax/ajaxButtonInstallment.php',
                      data  :   {"id":id,"PaymentRequestNo":PaymentRequestNo},
                      async: false,
                  }).responseText;
                
                // if (dataT.split(";")[0] == 0 && dataT.split(";")[1] != 0) {
                //   $("#button"+id).addClass("hidden");
                // }else if (dataT.split(";")[0] != 0 && dataT.split(";")[1] == 0) {
                //   $("#detail"+id).addClass("hidden");
                // }

                return "<a id='detail"+id+"' href='home?page=installmentListDetail&id="+id+"' class='btn btn-primary'>Detail</a>"
              }
            }
          ],
          "serverSide": true,
          "language": {
            "lengthMenu": "",
            "zeroRecords": "Nothing found - sorry",
            "info": "Showing page _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }
        });

        tableInstallmentList.order([1,'desc']).draw();
  

    $('#query-table-installment-list').on( 'click', 'button', function () {
      
      var data = table.row( $(this).parents('tr') ).data();
      var idx = data[0] - 1;
      var No = data[0];
      var ccy = data[3].replace(/\s/g, '').substr(22,3);
      var PayReqNo = data[1].substr(0,15);
      var InstallID = data[1].substr(18,19);
      var PayReqID = data[10];
      var CountAlert = 0;
      var dataRow = table.$('input, VoucherNo').serialize();

      var res = dataRow.split('&');
      var colVNo = res[((((idx+1)*2)-1)-1)];
      var colVDate = res[(((idx+1)*2)-1)];

      var res1 = colVNo.split('=');
      var colVNoValue = res1[1];
      
      var res2 = colVDate.split('=');
      var colVDateValue = res2[1];
      
      if(colVNoValue == '')
      {
        swal('Voucher No is Empty', '', 'error');
        return false;
      }
      else if(colVDateValue == '')
      {
        swal('Date is Empty', '', 'error');
        return false; 
      }
      else if(colVNoValue != '' && colVDateValue != '')
      {
        // console.log();
        $.ajax({
          type  :   'POST',
          url   :   'ajax/ajax_type2.php',
          data  :   'VoucherNoInstall='+colVNoValue,
          success : function(data){
            if(data != '')
            {
              if(!confirm(colVNoValue + ' Already Exists. Are you sure you want to continue ?')){
                return false;
              }
            }
            window.location.assign('fungsi/installment/financeActionInstallment.php?installID='+ InstallID +'&id='+ PayReqNo +'&ccy='+ ccy +'&VoucherNo='+ colVNoValue +'&VoucherDate='+ colVDateValue +' ');
          }
        });
        //window.location.assign('fungsi/paymentrequest/financeAction.php?id='+ PayReqID +'&VoucherNo='+ colVNoValue +'&VoucherDate='+ colVDateValue +' ');
      }

      
      
    });
});
</script>