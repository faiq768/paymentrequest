<?php
  $userGroup = mysqli_query($conn, "SELECT a.username, b.group_id FROM security_user a JOIN security_user_group b ON b.username = a.username WHERE a.username = '".$_SESSION['username']."'");
                 $groupidData = mysqli_fetch_array($userGroup);
?>

<div class="container">
  <div class="widget-content">
    <div class="tab-content">
      <div class="tab-pane active" id="formcontrols">
        <div class="widget widget-table action-table">
          <div class="widget-content">
            <h3>INSTALLMENT LIST</h3>
            <table id="query-table" class="display cell-border" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>NO</th>
                  <th>PAYMENT REQUEST NUMBER </th>
                  <th>INTERMEDIARY NAME </th>
                  <th>AMOUNT</th>
                  <th>Installment</th>

                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php
          $no    = 1;
          $branchCodeArray = explode(",", $_SESSION['branch_code']);
           
          
          for($i=0; $i < count($branchCodeArray); $i++){ // FOR SETIAP KODE BRANCH
                 

                 
                    $query = "SELECT a.CCY, a.Install_ID, a.PaymentRequestNo, a.Client, b.ID AS idHeader, SUM(a.Amount) AS Gross, a.Installment, b.STATUS_PR FROM tpaymentrequest_installment a INNER JOIN tpaymentrequestheader b ON b.PaymentRequestNo = a.PaymentRequestNo WHERE  a.PaymentRequestNo LIKE '".$branchCodeArray[$i]."%' GROUP BY a.Install_ID, CCY  ORDER BY a.CREATED_DATE DESC";
                 
                  $Data  = mysqli_query($conn, $query) or die(mysqli_error($conn));
                  
                  while ($queryRow =   mysqli_fetch_array($Data)) {

                ?>
                <tr align="center">
                  <th> <?php echo $no; ?> </th>
                  <td > <?php echo $queryRow['PaymentRequestNo']." / ".$queryRow['Install_ID']?> </td>
                  <td> <?php echo $queryRow['Client'] ?> </td>
                  
                  <td> 
                    <table>
                      
                        <tr>
                          <td><?php echo $queryRow['CCY']; ?> </td>
                          <?php

                            if ($queryRow['Install_ID'] == 1) {
                              echo "<td>".number_format($queryRow['Gross'] , 2 , '.' , ',' )."</td>";
                            }else{
                              $INSTALLMENT_ID = $queryRow['Install_ID'] - 1;
                              $QUERY_AMOUT = "SELECT RemainingAmount from tpaymentrequest_installment where PaymentRequestNo= '".$queryRow['PaymentRequestNo']."' AND Install_ID = '$INSTALLMENT_ID' ";
                            $SQL_AMOUNT = mysqli_query($conn, $QUERY_AMOUT);
                            $FETCH_AMOUNT = mysqli_fetch_array($SQL_AMOUNT); 
                              echo "<td>".number_format($FETCH_AMOUNT['RemainingAmount'] , 2 , '.' , ',' )."</td>";
                            }
                          ?>
                                              
                      </tr>
                    </table>
                  </td>
                  <td> 
                    <table>
                       <tr>
                            <td><?php echo $queryRow['CCY']; ?> </td>
                            <td><?php echo number_format($queryRow['Installment'] , 2 , '.' , ',' ); ?>
                            </td>
                       </tr>
                     </table> 
                  </td>

                  <td>
                    <a href="home?page=installmentListDetail&id=<?php echo $queryRow['idHeader'] ?>&idInstall=<?php echo $queryRow['Install_ID'] ?>&CCY=<?php echo $queryRow['CCY'] ?>" class="btn btn-primary">Detail</a>
                  </td>
                   
                </tr>
                <?php $no++;}
          }
        ?>
              </tbody>
            </table>
          </div>
        </div>   
      </div> 
    </div>
  </div>
</div>
<?php  include "page/lookup/branch.php"; ?>

<script type="text/javascript">
  $(function () {
        $("#query-table").DataTable({
          "paging":   true,
          "ordering": true,
          "scrollY": '45vh',
          "scrollX": true,
          "language": {
            "lengthMenu": "",
            "zeroRecords": "Nothing found - sorry",
            "info": "Showing page _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }
        });
    });
$(document).ready(function(){
  var table = $('#query-table').DataTable();
  
});
</script>