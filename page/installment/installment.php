<?php
  $userGroup = mysqli_query($conn, "SELECT a.username, b.group_id FROM security_user a JOIN security_user_group b ON b.username = a.username WHERE a.username = '".$_SESSION['username']."'");
                 $groupidData = mysqli_fetch_array($userGroup);
?>

<div class="container">
  <div class="widget-content">
    <div class="tab-content">
      <div class="tab-pane active" id="formcontrols">
        <div class="widget widget-table action-table">
          <div class="widget-content">
            <h3>INSTALLMENT</h3>
            <table id="query-table-installment" class="display cell-border" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>NO</th>
                  <th>PAYMENT REQUEST NUMBER </th>
                  <th>SOB</th>
                  <th>BENEFICIARY NAME</th>
                  <th>STATUS</th>
                  <th>ACTION</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>   
      </div> 
    </div>
  </div>
</div>
<?php  include "page/lookup/branch.php"; ?>

<script type="text/javascript">
  
  $(function () {
        var tableInstallment = $("#query-table-installment").DataTable({
          "paging":   true,
          "ordering": true,
          "scrollY": '45vh',
          "scrollX": true,
          "processing": true,
          "ajax": "ajax/ajax.tableInstallment.php",
          "columnDefs":
          [
            {
              "targets": 0, 
              "data": null,
               // "defaultContent": "<button class='btn btn-success btn-xs tblEdit'>Edit / Delete</button>"
               render: function (data,type,row,meta){
                return meta.row + meta.settings._iDisplayStart + 1;
               }
            },
            {
              "targets":[0,1,4,5],
              "className" : "text-center"
            },
            {
              "targets" : 4,
              "render" : function(data){
                var id                = data.split(";")[0];
                var PaymentRequestNo  = data.split(";")[1];
                
                var data4 = $.ajax({
                      type  :   'POST',
                      url   :   'ajax/ajax.statusInstallment.php',
                      data  :   {"id":id,"PaymentRequestNo":PaymentRequestNo},
                      async: false,
                  }).responseText;
                // console.log(data4);
                var row     = data4.split(";")[0];
                var status  = data4.split(";")[1];

                if (row == 0 && status == '') {
                  status = "Not Installment";
                }else {
                  status = status;
                }

                return status
              }
            },
            {
              "targets" : 5,
              "width"   : "20%",
              "render" : function(data){
                var id                = data.split(";")[0];
                var PaymentRequestNo  = data.split(";")[1];
                
                var dataT = $.ajax({
                      type  :   'POST',
                      url   :   'ajax/ajaxButtonInstallment.php',
                      data  :   {"id":id,"PaymentRequestNo":PaymentRequestNo},
                      async: false,
                  }).responseText;
                
                if (dataT.split(";")[0] == 0 && dataT.split(";")[1] != 0) {
                  $("#button"+id).addClass("hidden");
                }else if (dataT.split(";")[0] != 0 && dataT.split(";")[1] == 0) {
                  $("#detail"+id).addClass("hidden");
                }

                return "<a id='button"+id+"' href='home?page=addinstall&id="+id+"' class='btn btn-primary'>Installment</a> <a id='detail"+id+"' href='home?page=installmentdetail&id="+id+"' class='btn btn-primary'>Detail</a>"
              }
            }
          ],
          "serverSide": true,
          "language": {
            "lengthMenu": "",
            "zeroRecords": "Nothing found - sorry",
            "info": "Showing page _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }
        });

        tableInstallment.order([1,'desc']).draw();
    });
</script>

