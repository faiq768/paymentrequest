 <div class="modal fade" id="intermediaryCode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Intermediary Code</h4>
            </div>
            <div class="modal-body" id="data-intercode">
                <!-- <h2>Input Intermediary Type!!!</h2> -->
                <table id="dataIntermediaryCode" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>COMPANY CODE</th>
                            <th>FULL NAME</th>
                            <th>METHOD</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

 <script type="text/javascript">
  $(document).ready(function(){
        $('#dataIntermediaryCode').css({'cursor':'pointer'});
    });
//            jika dipilih, kode obat akan masuk ke input dan modal di tutup
    $(document).on('click', '.intermediaryCodePilih', function () {
        document.getElementById("code").value = $(this).attr('data-intermediaryCode');
        document.getElementById("name").value = $(this).attr('data-fullname');

        if ($(this).attr('data-paymethod') == 'TRNF') {
            $('.pay').css({'display' : 'inline-block'});
            //$('.ccy').removeAttr("readonly");
            $('.ccy').attr("readonly","readonly");
            $('#tccy').css({'display' : 'inline-block'});
            document.getElementById("mop").value = 'TRANSFER';
        }else{
            $('.pay').css({'display' : 'none'});
            $('.ccy').attr("readonly","readonly");
            $('#tccy').css({'display' : 'none'});
            document.getElementById("mop").value = 'CASH';
        };

        document.getElementById("ptc").value = '';
        document.getElementById("bename").value = '';
        document.getElementById("bank").value = '';
        document.getElementById("account").value = '';
        document.getElementById("branch").value = '';
        document.getElementById("cc").value = '';

        $('#intermediaryCode').modal('hide');


    });
    

</script>