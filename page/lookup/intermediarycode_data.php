<div class="modal fade" id="intermediary-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Intermediary</h4>
            </div>
            <div class="modal-body">
                <table id="intermediaryTable" class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
							<th>&nbsp;</th>
                            <th>Code</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
							$sobIdData = $dataUser['intermediary_type'];
							//echo "AAA ".$sobIdData;
							$sobDataArray = explode(",", $sobIdData);
						
						
                            $query = "SELECT intermediary_type_code,intermediary_type_desc FROM master_intermediary";
                            $data            =   mysqli_query($conn, $query) or die(mysqli_error()); 
                            while ($dataRow        =   mysqli_fetch_array($data)) 
                            {
                        ?>
                            <tr class="intermediaryPilih" data-intermediaryDesc="<?php echo $dataRow['intermediary_type_desc']; ?>" >
								
								<?php if (in_array($dataRow['intermediary_type_desc'], $sobDataArray)){ ?>
								
								<td><input type="checkbox" checked name="intermediaryCbk" class='pilihIntermediary' value="<?php echo $dataRow['intermediary_type_desc']; ?>"  /></td>
								<?php } else { ?>
								<td><input type="checkbox" name="intermediaryCbk" class='pilihIntermediary' value="<?php echo $dataRow['intermediary_type_desc']; ?>"  /></td>
								
								<?php } ?>
								
								<td><?php echo $dataRow['intermediary_type_code'] ; ?></td>
                                <td><?php echo $dataRow['intermediary_type_desc'] ; ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>  
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
        $('#intermediary-lookup').css({'cursor':'pointer'});
    });
		
	//jika dipilih, data akan masuk ke data
    $(document).on('click', '.intermediaryPilih', function (e) {	
        // document.getElementById("intermediaryCode").value = $(this).attr('data-intermediaryCode');
        document.getElementById("intermediaryName").value = $(this).attr('data-intermediaryDesc');
   });
	
	//tabel lookup 
    $(function () {
        $("#intermediaryTable").dataTable(
		"pageLength": 99,
		 "paging": false,
		 "retrieve": false	
		);
    });
</script>