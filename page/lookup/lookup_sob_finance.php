 <div class="modal fade" id="financeSOB" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">SOB</h4>
            </div>
            <div class="modal-body" id="data-intercode">
                <!-- <h2>Input Intermediary Type!!!</h2> -->
                <table id="tableSobFinance" class="table table-hover display" style="width:100%">
                    <thead>
                        <tr>
                            <th>CODE</th>
                            <th>DESCRIPTIONS</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $sqlSobFinance = mysqli_query($conn,"SELECT * From uw_lob");
                            while ($dataSobFinance = mysqli_fetch_array($sqlSobFinance)) {
                        ?>
                        <tr class="pilihSobFinance" attrKodeSob="<?php echo $dataSobFinance['uw_lines_of_business'] ?>" attrNamaSob="<?php echo $dataSobFinance['uw_lob_description'] ?>">
                            <td><?php echo $dataSobFinance['uw_lines_of_business'] ?></td>
                            <td><?php echo $dataSobFinance['uw_lob_description'] ?></td>
                        </tr>
                        <?php

                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

 <script type="text/javascript">
  $(document).ready(function(){

    $('#tableSobFinance').dataTable();

        $('.pilihSobFinance').css({'cursor':'pointer'});
    });
//            jika dipilih, kode obat akan masuk ke input dan modal di tutup
    $(document).on('click', '.pilihSobFinance', function () {
        document.getElementById("iCodeSob").value = $(this).attr('attrKodeSob');
        document.getElementById("iNameSob").value = $(this).attr('attrNamaSob');


        $('#financeSOB').modal('hide');


    });
    

</script>