<div class="modal fade" id="branch-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">BRANCH</h4>
            </div>
            <div class="modal-body">
                <table id="branchTable" class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
							<th>&nbsp;</th>
							<th>Code Branch</th>
                            <th>Branch Description</th>
                        </tr>
                    </thead>
                    <tbody>
					    <?php 
							$branchData = $dataUser['home_branch'];
							$branchCodeArray = explode(",", $branchData);
							
							//echo "BRANCH CODE ".$dataArray;
							

							$query = "SELECT branch_code, branch_name FROM master_branch";
                            $data                 =   mysqli_query($conn, $query) or die(mysqli_error()); 
                            while ($Branch        =   mysqli_fetch_array($data)) 
                            {
                        ?>
                            <tr class="branchPilih" data-branch-name="<?php echo $Branch['branch_name']; ?>" data-branch-code="<?php echo $Branch['branch_code']; ?>">
								<?php if (in_array($Branch['branch_code'], $branchCodeArray)){ ?>
								
								<td><input type="checkbox" checked name="branchCbk" class='pilihBranch' value="<?php echo $Branch['branch_code']; ?>"  /></td>
								<?php }else{ ?>
								<td><input type="checkbox"  name="branchCbk" class='pilihBranch' value="<?php echo $Branch['branch_code']; ?>"  /></td>
								
								<?php }?>
								
								<td><?php echo $Branch['branch_code'] ; ?></td>
                                <td><?php echo $Branch['branch_name'] ; ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>  
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#bussines-lookup').css({'cursor':'pointer'});
    });
//            jika dipilih, data akan masuk ke data
    $(document).on('click', '.branchPilih', function (e) {
        document.getElementById("ibranchcode").value = $(this).attr('data-branch-code');
        document.getElementById("ibranchname").value = $(this).attr('data-branch-name');

        //$('#branch-lookup').modal('hide');
    });

//            tabel lookup 
    $(function () {
        $("#branchTable").dataTable({
			"pageLength": 99,
    		 "paging": false,
    		 "retrieve": false
        });
    });

    
</script>