<form action="fungsi/change_password.php" onsubmit="return validateForm()" method="post" id="myform"> 
    <div class="modal fade" id="password-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">CHANGE PASSWORD</h4>
                </div>
                <div class="modal-body">
                    <center>
                    <table>
                        <tr>
                            <td>OLD PASSWORD</td><td>:</td>
                            <td>
                                <input type="password" id="iold_password" name="nold_password" class="form-control" id="old">
                            </td>
                        </tr>
                        <tr>
                            <td class="old_password"></td>
                        </tr>
                        <tr>
                            <td>NEW PASSWORD</td><td>:</td>
                            <td>
                                <input type="password" id="inew_password1" name="new_password1" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td class="new_password"></td>
                        </tr>
                        <tr>
                            <td>CONFIRM PASSWORD</td><td>:</td>
                            <td>
                                <input type="password" id="inew_password2" name="new_password2" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td class="confirm"></td>
                        </tr>
                    </table>
                    </center>
                    <input type="submit" class="btn btn-primary" id="ibtnsubmit" name="btn-save" value="CHANGE">

                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
function validateForm() {
    var iold_password = document.getElementById("iold_password").value;
    var inew_password1 = document.getElementById("inew_password1").value;
    var inew_password2 = document.getElementById("inew_password2").value;
	
    if (iold_password == "") {
        swal("Old Password must be Fill !!", "", "error");
        return false;
    }
    else if (inew_password1 == "") {
        swal("New Password must be Fill !!", "", "error");
        return false;
    }
    else if (inew_password1 == "") {
        swal("Confirm New Password must be Fill !!", "", "error");
        return false;
    }
    else if(inew_password1 != inew_password2)
    {
        swal("New Password must same with Confirm New Password !!", "", "error");
        return false;
    }
}

$(document).ready(function(){

  $('#ibtnsubmita').click(function(){
    var iold_password = document.getElementById("iold_password").value;
    var inew_password1 = document.getElementById("inew_password1").value;
    var inew_password2 = document.getElementById("inew_password2").value;

    if (iold_password == "") {
        swal("Old Password must be Fill !!", "", "error");
        return false;
    }
    else if (inew_password1 == "") {
        swal("New Password must be Fill !!", "", "error");
        return false;
    }
    else if (inew_password1 == "") {
        swal("Confirm New Password must be Fill !!", "", "error");
        return false;
    }
    else if(inew_password1 != inew_password2)
    {
        swal("New Password must same with Confirm New Password !!", "", "error");
        return false;
    }

    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        //window.location.assign("fungsi/change_password.php?opass="+iold_password+"&npass="+inew_password1+"&cpass="+inew_password2);

        // $.ajax({
        //     type  :   'POST',
        //     url   :   'fungsi/change_password.php',
        //     data  :   'opass='+iold_password+'npass='+inew_password1+'cpass='+inew_password2+,
        //     success : function(response){
        //       $('#data-menu').html(response);
        //     }
        //   });


      } else {
        return false;
      }
    });
  });


});
</script>