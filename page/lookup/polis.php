<div class="modal fade" id="lookup-polis" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">POLIS / NOTE</h4>
            </div>
            <div class="modal-body" id="data-policy">
                <table id="dataPolicy" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>NO POLICY</th>
                            <th>NO NOTE</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#dataPolicy').css({'cursor':'pointer'});

    });
    // Jika dipilih, value akan masuk ke input dan modal di tutup
    $(document).on('click', '.polisPilih', function (e) {
        document.getElementById("pnt").value = $(this).attr('polis-note');
        document.getElementById("type").value = $(this).attr('itd-type');
        document.getElementById("code").value = $(this).attr('itd-code');
        document.getElementById("name").value = $(this).attr('itd-name');
        document.getElementById("bussines").value = $(this).attr('lob');
        document.getElementById("ccy").value = $(this).attr('currency');
         document.getElementById("branch_code").value = $(this).attr('branch-code');
        document.getElementById("branch_name").value = $(this).attr('branch-name');

        var typecode = document.getElementById("code").value;
        typecode = typecode.substring(0, 1);
        //alert('type codee ' + typecode);
        document.getElementById("typecode").value = typecode;
        
        
        document.getElementById("noteno").value = $(this).attr('polis-noteno');

        //if ($(this).attr('data-paymethod') == 'TRNF') {
        if ($(this).attr('method-op') == 'TRNF') {
            $('.pay').css({'display' : 'inline-block'});
            $('.ccy').removeAttr("readonly");
            $('#tccy').css({'display' : 'inline-block'});
          document.getElementById("mop").value = 'TRANSFER';
        }else if ($(this).attr('data-paymethod') == 'TRNF'){
            $('.pay').css({'display' : 'none'});
            $('.ccy').attr("readonly","readonly");
            $('#tccy').css({'display' : 'none'});
            document.getElementById("mop").value = 'CASH';
        }else{
            $('.pay').css({'display' : 'none'});
            $('.ccy').attr("readonly","readonly");
            $('#tccy').css({'display' : 'none'});
            document.getElementById("mop").value = 'CASH';
        };
        $('#lookup-polis').modal('hide');
    });

//            tabel lookup obat
    $(function () {
        $("#polisTable").DataTable();
    });

    
</script>