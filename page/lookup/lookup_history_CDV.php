<div class="modal fade" id="lookup-History-CDV" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">LOG CDV</h4>
            </div>
            <div class="modal-body">
                <table id="ApprovalHistoryTable" class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Voucher No</th>
                            <th>Paid Date</th>
                            <th>Update At</th>
                        </tr>
                    </thead>
                    <tbody class="Show-Log">
                               
                    </tbody>
                </table>  
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $(".log-cdv").click(function(e) {
               var idy = $(this).attr("data-id");
                $.ajax({
                    type    :   'post',
                    url     :   'ajax/ajax_history_corection.php',
                    data    :   'idy='+idy,
                    success :   function(response){
                        // console.log(response);
                        $('.Show-Log').html(response);
                    },
                    beforeSend : function(){
                        $('.Show-Log').html('LOADING...');
                      }
                });
            });
    });
</script>
