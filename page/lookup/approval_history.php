<div class="modal fade" id="lookup-ApprovalHistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">APPROVAL HISTORY</h4>
            </div>
            <div class="modal-body">
                <center>History Payment Request</center><hr>
                <?php 
                        if (!empty($_SESSION['REQ_ID'])) 
                        {
                            $req_id = $_SESSION['REQ_ID'];
                            $sqlApprovalHistory   =   mysqli_query($conn, "SELECT * FROM thistory_approval WHERE REQUEST_ID=$req_id AND REQUEST_TYPE = 'PAYMENT_REQUEST' ORDER BY CREATED_DATE ") or die(mysqli_error()); 
                ?>
                <table id="ApprovalHistoryTable" class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>User</th>
                            <th>Action</th>
                            <th>Date</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            while ($data        =   mysqli_fetch_array($sqlApprovalHistory)) 
                            {
                        ?>
                            <tr>
                                <td><?php echo $data['CREATED_BY'] ; ?></td>
                                <td><?php echo $data['ACTION'] ; ?></td>
                                <td><?php echo $data['CREATED_DATE']; ?></td>
                                <td><?php echo $data['REMARKS'] ; ?></td>
                            </tr>
                        <?php 
                            }
                        ?>
                    </tbody>
                </table>  
                <?php
                    $sqlInstallmentHistory   =   mysqli_query($conn, "SELECT * FROM thistory_approval WHERE REQUEST_ID=$req_id AND REQUEST_TYPE Like 'INSTALLMENT%' ORDER BY CREATED_DATE ") or die(mysqli_error());
                    $SQL_NumRowInstallment = mysqli_num_rows($sqlInstallmentHistory);
                    if ($SQL_NumRowInstallment != 0) {
                ?>
                <center>History Installment</center><hr>
                <table id="InstallmentTable" class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>User</th>
                            <th>Deskripsi</th>
                            <th>Action</th>
                            <th>Date</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            while ($dataInstallment        =   mysqli_fetch_array($sqlInstallmentHistory)) 
                            {
                        ?>
                            <tr>
                                <td><?php echo $dataInstallment['CREATED_BY'] ; ?></td>
                                <td><?php echo $dataInstallment['REQUEST_TYPE'] ; ?></td>
                                <td><?php echo $dataInstallment['ACTION'] ; ?></td>
                                <td><?php echo $dataInstallment['CREATED_DATE']; ?></td>
                                <td><?php echo $dataInstallment['REMARKS'] ; ?></td>
                            </tr>
                        <?php 
                            }
                        ?>
                    </tbody>
                </table>  
                <?php
                    }
                    } 
                ?>
            </div>
        </div>
    </div>
</div>