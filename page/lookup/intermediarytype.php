 <div class="modal fade" id="intermediaryType" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Intermediary Type</h4>
                    </div>
                    <div class="modal-body">
                        <table id="intermediaryTypeTable" class="table table-bordered table-hover table-striped" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Intermediary Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
									
									$intermediaryCodeArray = explode(",", $_SESSION['intermediary_type']);
									$dataArray = "";
									for($i=0; $i < count($intermediaryCodeArray); $i++){
										$dataArray = $dataArray."'".$intermediaryCodeArray[$i]."',";
									}
									$dataArray = $dataArray."'1'";
									
                                    $sql            =   mysqli_query($conn, "SELECT * FROM master_intermediary where intermediary_type_desc in (".$dataArray.")"     ) or die(mysqli_error()); 
                                    
                                    while ($itdType        =   mysqli_fetch_array($sql)) {
                                 ?>
                                    <tr itd-code="<?php echo $itdType['intermediary_type_code']; ?>" class="intermediaryTypePilih" data-intermediaryType="<?php echo $itdType['intermediary_type_desc']; ?>">
                                        <td><?php echo $itdType['intermediary_type_code']; ?></td>
                                        <td><?php echo $itdType['intermediary_type_desc']; ?></td>
                                    </tr>
                                    <?php } ?>
                                    
                            </tbody>
                        </table>  
                    </div>
                </div>
            </div>
        </div>

         <script type="text/javascript">
            $(document).ready(function(){
                $('#intermediaryTypePilih,td').css({'cursor':'pointer'});

            });
//            jika dipilih, kode obat akan masuk ke input dan modal di tutup
            $(document).on('click', '.intermediaryTypePilih', function (e) {
                document.getElementById("typecode").value = $(this).attr('itd-code');
                document.getElementById("type").value = $(this).attr('data-intermediaryType');
                document.getElementById("code").value = '';
                document.getElementById("name").value = '';
                document.getElementById("bussines").value = '';
                document.getElementById("ccy").value = '';
                document.getElementById("mop").value = '';

                document.getElementById("ptc").value = '';
                document.getElementById("bename").value = '';
                document.getElementById("bank").value = '';
                document.getElementById("account").value = '';
                document.getElementById("branch").value = '';
                document.getElementById("cc").value = '';

                $('#intermediaryType').modal('hide');
            });

//            tabel lookup obat
            $(function () {
                $("#intermediaryTypeTable").DataTable({
                   

                });
            });

            
        </script>