 <div class="modal fade" id="typeofpayment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Type Of Payment</h4>
                    </div>
                    <div class="modal-body">
                        <table id="typeofpaymentTable" class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $sqltypeofpayment            =   mysqli_query($conn, "SELECT * FROM mtypeofpayment WHERE ISACTIVE=1") or die(mysqli_error()); 
                                    
                                    while ($typeofpayment        =   mysqli_fetch_array($sqltypeofpayment)) {
                                 ?>
                                    <tr class="typeofpaymentPilih" data-typeofpayment="<?php echo $typeofpayment['DESCRIPTION'] ; ?>">
                                        <td><?php echo $typeofpayment['CODE'] ; ?></td>
                                        <td><?php echo $typeofpayment['DESCRIPTION'] ; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>  
                    </div>
                </div>
            </div>
        </div>

         <script type="text/javascript">
            $(document).ready(function(){
                $('#typeofpayment').css({'cursor':'pointer'});
            });
//            jika dipilih, kode obat akan masuk ke input dan modal di tutup
            $(document).on('click', '.typeofpaymentPilih', function (e) {
                document.getElementById("top").value = $(this).attr('data-typeofpayment');
                $('#typeofpayment').modal('hide');
            });

//            tabel lookup obat
            $(function () {
                $("#typeofpaymentTable").dataTable();
            });

            
        </script>