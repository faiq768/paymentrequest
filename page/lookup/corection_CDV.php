 <div class="modal fade" id="corection-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Corection</h4>
            </div>
            <form action="fungsi/paymentrequest/function_corection.php" method="POST">
            <div class="modal-body">
                <div id="Show-Voucher"></div>
            </div>
            <div class="modal-footer">
                <a href="#lookup-History-CDV" data-toggle='modal' class="btn btn-sm btn-primary pull-left log-cdv">History</a>
                <input type="submit" name="Save" class="btn btn-sm btn-primary" value="Save">
            </div>
            </form>
        </div>
    </div>
</div>

<?php  include "page/lookup/lookup_history_CDV.php"; ?>

<script type="text/javascript">
    $(document).ready(function(){
        $(".koreksi").click(function(e) {
            $('.log-cdv').attr("data-id",$(this).attr("data-id"));
               var idx = $(this).attr("data-id");
                $.ajax({
                    type    :   'post',
                    url     :   'ajax/ajax_corection.php',
                    data    :   'idx='+idx,
                    success :   function(response){
                        $('#Show-Voucher').html(response);
                    },
                    beforeSend : function(){
                        $('#Show-Voucher').html('LOADING...');
                      }
                });
            });
    });
</script>
