 <div class="modal fade" id="user-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">User</h4>
            </div>
            <div class="modal-body">
                <table id='usersTable' class='table table-bordered table-hover table-striped'>
                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>FUll Name</th>
                            <th>Branch</th>
                        </tr>
                    </thead>
                    <tbody >
                        <?php
                            $dataUsers  =   mysqli_query($conn,  "SELECT a.*, b.branch_name
                                          FROM security_user a
                                          LEFT JOIN master_branch b ON b.branch_code = a.home_branch
                                          WHERE a.ISACTIVE=1 
                                          ORDER BY username ") or die(mysqli_error()); 
                            
                            while ($data        =   mysqli_fetch_array($dataUsers)) {
                         ?>
                            <tr class="usersPilih" data-username="<?php echo $data['username'] ; ?>">
                                <td><?php echo $data['username'] ; ?></td>
                                <td><?php echo $data['full_name'] ; ?></td>
                                <td><?php echo $data['branch_name'] ; ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

 <script type="text/javascript">
    $(document).ready(function(){
        $('#user-lookup').css({'cursor':'pointer'});
    });
//            jika dipilih, kode obat akan masuk ke input dan modal di tutup
    $(document).on('click', '.usersPilih', function (e) {
        document.getElementById("iusername").value = $(this).attr('data-username');

        var username = $(this).attr('data-username');

        $.ajax({
            type  :   'POST',
            url   :   'ajax/ajax_type.php',
            data  :   'username='+username,
            success : function(response){
              $('#data-group').html(response);
            }
        });

        $('#user-lookup').modal('hide');
    });

//            tabel lookup
    $(function () {
        $("#usersTable").dataTable();
    });
    
</script>