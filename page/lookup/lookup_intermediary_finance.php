 <div class="modal fade" id="financeIntermediary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Intermediary</h4>
            </div>
            <div class="modal-body" id="data-intercode">
                <!-- <h2>Input Intermediary Type!!!</h2> -->
                <table id="tableIntermediaryFinance" class="table table-hover display" style="width:100%">
                    <thead>
                        <tr>
                            <th>COMPANY CODE</th>
                            <th>FULL NAME</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $sqlIntermediaryFinance = mysqli_query($conn,"SELECT * From master_intermediary");
                            while ($dataIntermediaryFinance = mysqli_fetch_array($sqlIntermediaryFinance)) {
                        ?>
                        <tr class="pilihIntermediaryFinance" attrKodeIntermediary="<?php echo $dataIntermediaryFinance['intermediary_type_code'] ?>" attrNamaIntermediary="<?php echo $dataIntermediaryFinance['intermediary_type_desc'] ?>">
                            <td><?php echo $dataIntermediaryFinance['intermediary_type_code'] ?></td>
                            <td><?php echo $dataIntermediaryFinance['intermediary_type_desc'] ?></td>
                        </tr>
                        <?php

                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

 <script type="text/javascript">
  $(document).ready(function(){

    $('#tableIntermediaryFinance').dataTable();

        $('#dataIntermediaryCode').css({'cursor':'pointer'});
    });
//            jika dipilih, kode obat akan masuk ke input dan modal di tutup
    $(document).on('click', '.pilihIntermediaryFinance', function () {
        document.getElementById("iKodeIntermediary").value = $(this).attr('attrKodeIntermediary');
        document.getElementById("iNamaIntermediary").value = $(this).attr('attrNamaIntermediary');


        $('#financeIntermediary').modal('hide');


    });
    

</script>