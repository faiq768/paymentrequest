<div class="modal fade" id="department-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">DEPARTMENT</h4>
            </div>
            <div class="modal-body">
                <table id="departmentTable" class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
							<th>&nbsp;</th>
                            <th>Code</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
							$deptIdData = $dataUser['Dept_Id'];
							$deptIdCodeArray = explode(",", $deptIdData);
							
                            $query = "SELECT 0 as ID,'0' as Code, 'ADMIN' as Description
                                UNION ALL
                                SELECT ID, Code, Description FROM mdepartment";
                            $data            =   mysqli_query($conn, $query) or die(mysqli_error()); 
                            while ($dataRow        =   mysqli_fetch_array($data)) 
                            {
                        ?>
                            <tr class="departmentPilih" data-dept-id="<?php echo $dataRow['ID']; ?>" data-dept-code="<?php echo $dataRow['Code']; ?>" data-dept-name="<?php echo $dataRow['Description']; ?>" >
								<?php if (in_array($dataRow['ID'], $deptIdCodeArray)){ ?>
								<td><input type="checkbox" checked name="checkRetrive" class='pilihDepartment' value="<?php echo $dataRow['ID']; ?>"  /></td>
								<?php } 
								else{
									?>
								<td><input type="checkbox" name="checkRetrive" class='pilihDepartment' value="<?php echo $dataRow['ID']; ?>"  /></td>
								
								<?php } ?>
								<td><?php echo $dataRow['Code'] ; ?></td>
                                <td><?php echo $dataRow['Description'] ; ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>  
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	
    $(document).ready(function(){
        $('#department-lookup').css({'cursor':'pointer'});
    });


		
	//jika dipilih, data akan masuk ke data
    $(document).on('click', '.departmentPilih', function (e) {
		document.getElementById("nDeptId").value = $(this).attr('data-dept-id');
        document.getElementById("nDeptCode").value = $(this).attr('data-dept-code');
        document.getElementById("nDeptName").value = $(this).attr('data-dept-name');
   });
	
//tabel lookup 
    $(function () {
        $("#departmentTable").dataTable();
    });
</script>