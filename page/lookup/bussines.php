 <div class="modal fade" id="bussines-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Class Of Business</h4>
            </div>
            <div class="modal-body">
                <table id="bussinesTable" class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Class Of Business</th>
                            <th>Class Of Business Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $sqlBussines            =   mysqli_query($conn, "SELECT * FROM uw_lob") or die(mysqli_error()); 
                            
                            while ($Bussines        =   mysqli_fetch_array($sqlBussines)) {
                         ?>
                            <tr class="bussinesPilih" data-bussines="<?php echo $Bussines['uw_lines_of_business'] ; ?>">
                                <td><?php echo $Bussines['uw_lines_of_business'] ; ?></td>
                                <td><?php echo $Bussines['uw_lob_description'] ; ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>  
            </div>
        </div>
    </div>
</div>

 <script type="text/javascript">
    $(document).ready(function(){
        $('#bussines-lookup').css({'cursor':'pointer'});
    });
//            jika dipilih, kode obat akan masuk ke input dan modal di tutup
    $(document).on('click', '.bussinesPilih', function (e) {
        document.getElementById("bussines").value = $(this).attr('data-bussines');
        $('#bussines-lookup').modal('hide');
    });

//            tabel lookup obat
    $(function () {
        $("#bussinesTable").dataTable();
    });

    
</script>