<div class="container">
  <div class="widget-content">
    <div class="tab-content">
      <div class="tab-pane active" id="formcontrols">
        <h3>TYPE OF PAYMENT</h3>
        <br>
        <div class="widget widget-table action-table">
          
          <div class="widget-content">
            <div>
              <a href="home?page=mtypeofpaymentlistdetail" class="btn btn-primary">Add</a>
            </div>
            <table id="query-table" class="display cell-border" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th></th>
                  <th>CODE</th>
                  <th>DESCRIPTION</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php
                  $query = "SELECT * FROM mtypeofpayment where ISACTIVE=1 ORDER BY CODE DESC ";
                  $Data  = mysqli_query($conn, $query); 
                  $no    = 1;
                  while ($queryRow =   mysqli_fetch_array($Data)) {
                ?>
                <tr align="center">
                  <th><?php echo $no; ?> </th>
                  <td><?php echo $queryRow['CODE'] ?> </td>
                  <td><?php echo $queryRow['DESCRIPTION'] ?> </td>
                  <td><a href="home?page=mtypeofpaymentlistdetail&form=edit&code=<?php echo $queryRow['CODE'] ?>" class="btn btn-primary">Detail</a></td>
                </tr>
                <?php $no++;} ?>
              </tbody>
            </table>
          </div>MD01
        </div>   
      </div> 
    </div>
  </div>
</div>

<script type="text/javascript">
  $(function () {
        $("#query-table").DataTable({
          "paging":   false,
          "ordering": true,
          "scrollY": '45vh',
          "scrollX": true,
          "language": {
            "lengthMenu": "Display records per page",
            "zeroRecords": "Nothing found - sorry",
            "info": "Showing page _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }
        });
    });
</script>