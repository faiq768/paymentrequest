<div class="col-lg-12 main-form">
  <form action="fungsi/masterdata/muserssave.php" onsubmit="return validateForm()" method="post" id="myformUser"> 
    <div class="span12 step-1 aktif" style="margin-left:0 !important;">
      <div class="widget-content">
        <h3>USERS</h3>
        <div class="tab-content">
          <div class="tab-pane pane-1 active" id="formcontrols">
            <div id="musers-edit" class="form-horizontal">
              <fieldset class="span12">
                <?php 
				$dataUser = null;
                if (isset($_GET['form']) && !empty($_GET['form'])) 
                {
                  $form   = $_GET['form'];
                  if($form=='edit')
                  {
                    if (isset($_GET['user']) && !empty($_GET['user'])) 
                    {
                      $user = $_GET['user'];
						
						
                      $query    = "SELECT a.*
                                  FROM security_user a
                                  LEFT JOIN mdepartment c ON c.ID=a.Dept_Id
                                  WHERE ISACTIVE=1 AND username = '$user' ";
                      $data   = mysqli_query($conn, $query) or die(mysqli_error($conn));
                      $dataUser = mysqli_fetch_array($data);
					 
            					$homeBranchArray = explode(",", $dataUser['home_branch']);
            					$dataArray = "";
            					for($i=0; $i < count($homeBranchArray); $i++){
            						$dataArray = $dataArray."'".$homeBranchArray[$i]."',";
            					}
            					
            					$dataArray = substr($dataArray, 0, strrpos($dataArray, ','));
            					
            					
            					$query    = "SELECT a.*
                                              FROM master_branch a where branch_code in ( ".$dataArray.")";
            				  $branchDataName   = mysqli_query($conn, $query) or die(mysqli_error($conn));
            				  $branchData = mysqli_fetch_array($branchDataName);  
            				  
            				  //dept
            					if(!empty($dataUser['Dept_Id'])){
            						$query    = "SELECT a.*
            									  FROM mdepartment a where a.id in ( ".$dataUser['Dept_Id'].") ";
            					}else{
            						$query    = "SELECT a.*
            									  FROM mdepartment a where a.id in ( 1) ";
            						
            					}
            				   //echo "AAA ".$query;
            				   //die();
            				  $deptDataName   = mysqli_query($conn, $query) or die(mysqli_error($conn));
            				  $deptData = mysqli_fetch_array($deptDataName);  
            				 //intermediary 
            				 
            					$initermediaryArray = explode(",", $dataUser['intermediary_type']);
            					$dataArray = "";
            					for($i=0; $i < count($initermediaryArray); $i++){
            						$dataArray = $dataArray."'".$initermediaryArray[$i]."',";
            					}
            					
            					$dataArray = substr($dataArray, 0, strrpos($dataArray, ','));
            					$query    = "SELECT a.*
                                              FROM master_intermediary a where a.intermediary_type_desc in ( ".$dataArray.") ";
            					$sobName   = mysqli_query($conn, $query) or die(mysqli_error($conn));
            					$sobData = "";
            					while($fetchBranch	=	mysqli_fetch_array($sobName)) {
            						$sobData = $sobData.$fetchBranch['intermediary_type_desc'].",";
            					}
            					
            					$sobData = substr($sobData, 0, strrpos($sobData, ','));

					
                ?>

                <div class="control-group">
                  <label class="control-label size-16" for="users">Username</label>
                	<div class="controls">
                    <input type="text" value="<?php echo $dataUser['username']; ?>" name="nusername" class="span3" id="iusername" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()">                    
                	</div>
	              </div>

                <div class="control-group">
                  <label class="control-label size-16" for="users">Branch</label>
                  <div class="controls">
				  		<input type="hidden" name="nBranchCodeId" id="nBranchCodeId" value="<?php echo $dataUser['home_branch'] ?>" />
                    <input type="text" value="<?php echo $dataUser['home_branch']; ?>" name="nbranchcode" class="span1" id="ibranchcode" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>
                    <a href="#" data-toggle="modal" data-target="#branch-lookup" ><i class="fa fa-search" aria-hidden="true"></i></a>
                    <input type="text" value="<?php echo $branchData['branch_name']; ?>" name="nbranchname" class="span2" id="ibranchname" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label size-16" for="users">Full Name</label>
                  <div class="controls">
                    <input type="text" value="<?php echo $dataUser['full_name']; ?>" name="nfullname" class="span3" id="ifullname" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label size-16" for="users">Department</label>
                  <div class="controls">
                    <input type="hidden" name="nDeptId" id="nDeptId" value="<?php echo $dataUser['Dept_Id'] ?>" >
                    <input type="text" value="<?php echo $dataUser['Dept_Id']; ?>" name="nDeptCode" class="span1" id="nDeptCode" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>

                    <a href="#" data-toggle="modal" data-target="#department-lookup" ><i class="fa fa-search" aria-hidden="true"></i></a>
                    <input type="text" value="<?php echo $deptData['Description']; ?>" name="nDeptName" class="span3" id="nDeptName" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>
                  </div>
                </div>
                <div class="control-group">
					  <label class="control-label size-16" for="users">Intermediary</label>
					  <div class="controls">
						<input type="hidden" name="intermediaryId" id="intermediaryId" value="<?php echo $dataUser['intermediary_type'] ?>" >
						<!-- <input type="text" value="<?php echo $dataUser['intermediary_type'] ?>" name="intermediaryCode" class="span1" id="intermediaryCode" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly> -->
						<input type="text" value="<?php echo $dataUser['intermediary_type'] ?>" name="intermediaryName" class="span3" id="intermediaryName" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>
            <a href="#" data-toggle="modal" data-target="#intermediary-lookup" ><i class="fa fa-search" aria-hidden="true"></i></a>
            
					  </div>
                </div>
                
                <fieldset class="span10">
                  <hr>
                  <!-- <a href="#" id="save-mtop" class="btn btn-primary" style="float:left;" >Save</a> -->
                  <a href="#!" class="btn-musers-edit"><input type="submit" name="musers-edit" style="float:left;" class="btn btn-primary" value="SAVE"></a>
                  <a href="#!" class="btn-musers-delete"><input type="submit" name="musers-delete" style="float:right;" class="btn btn-primary" value="DELETE"></a>
                </fieldset>
                <?php 
                    }
                  }
                }
                else
                {
                ?>
                  <div class="control-group">
                    <label class="control-label size-16" for="users">Username</label>
                    <div class="controls">
                      <input type="text" value="" name="nusername" class="span3" id="iusername" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()">                    
                    </div>
                  </div>
                  
                  <div class="control-group">
                    <label class="control-label size-16" for="users">Branch</label>
                    <div class="controls">
						<input type="hidden" name="nBranchCodeId" id="nBranchCodeId" />
                      <input type="text" value="" name="nbranchcode" class="span1" id="ibranchcode" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>
                      <a href="#" data-toggle="modal" data-target="#branch-lookup" ><i class="fa fa-search" aria-hidden="true"></i></a>
                      <input type="text" value="" name="nbranchname" class="span2" id="ibranchname" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>
                    </div>
                  </div>

                  <div class="control-group">
                    <label class="control-label size-16" for="users">Full Name</label>
                    <div class="controls">
                      <input type="text" value="" name="nfullname" class="span3" id="ifullname" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()">
                    </div>
                  </div>

                  <div class="control-group">
					  <label class="control-label size-16" for="users">Department</label>
					  <div class="controls">
						<input type="hidden" name="nDeptId" id="nDeptId" >
						<input type="text" value="" name="nDeptCode" class="span1" id="nDeptCode" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>
						<a href="#" data-toggle="modal" data-target="#department-lookup" ><i class="fa fa-search" aria-hidden="true"></i></a>
						<input type="text" value="" name="nDeptName" class="span3" id="nDeptName" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>
					  </div>
                </div>
                
				 <div class="control-group">
					  <label class="control-label size-16" for="users">Intermediary</label>
					  <div class="controls">
						<input type="hidden" name="intermediaryId" id="intermediaryId" >
						<input type="text" value="" name="intermediaryCode" class="span1" id="intermediaryCode" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>
						<a href="#" data-toggle="modal" data-target="#intermediary-lookup" ><i class="fa fa-search" aria-hidden="true"></i></a>
						<input type="text" value="" name="intermediaryName" class="span3" id="intermediaryName" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>
					  </div>
                </div>
				  
                  <fieldset class="span10">
                    <hr>
                    <!-- <a href="#" id="save-mtop" class="btn btn-primary" style="float:left;" >Save</a> -->
                    <a href="#!" class="btn-musers">
                      <input type="submit" name="musers-save" style="float:left;" class="btn btn-primary" value="SAVE">
                    </a>
                  </fieldset>
                <?php
                }
                ?>
              </fieldset>
            </div> <!-- /form-horizontal -->
          </div> <!-- /tab-pane -->MD06
        </div> <!-- /tab-content -->
      </div> <!-- /widget-content -->
    </div> <!-- /span6 -->
  </form>
</div>

<?php 
	include "page/lookup/branch.php"; 
	include "page/lookup/department.php";
	include "page/lookup/intermediarycode_data.php";
?>

<script type="text/javascript">
function validateForm() {
    var username = document.forms["myformUser"]["nusername"].value;
	//var branchcode = document.forms["myformUser"]["nbranchcode"].value;
 	var fullname = document.forms["myformUser"]["nfullname"].value;

	var branchcode = '';
	var deptId = '';
	
	
	$('[name="branchCbk"]').each( function (){
		if($(this).prop('checked') == true){
			branchcode += $(this).val() + ',';
			
		}
	});
	
	document.forms["myformUser"]["nBranchCodeId"].value = branchcode;
	
	deptId = document.forms["myformUser"]["nDeptId"].value;
	//alert('tt  ' + deptId);
	
	var intermediaryCode = '';
	$('[name="intermediaryCbk"]').each( function (){
		if($(this).prop('checked') == true){
			intermediaryCode += $(this).val() + ',';			
		}
	});
	
	document.forms["myformUser"]["intermediaryId"].value = intermediaryCode;
		
	var dept = '';
	 $('[name="checkRetrive"]').each( function (){
            if($(this).prop('checked') == true){
                dept += $(this).val() + ',';
				
            }
        });
	
	document.forms["myformUser"]["nDeptId"].value = dept;
		
	if(deptId == ""){
		
		
        swal("Check Your Department Data Entry !", "", "error");
        return false;
      }
	
	
    if (username == "") {
      swal("Username must be Fill !!", "", "error");
      return false;
    }
    else if(branchcode == "")
    {
      swal("Branch must be Fill !!", "", "error");
      return false;
    }
    else if(fullname == "")
    {
      swal("Full Name must be Fill !!", "", "error");
      return false;
    }
    else if(dept == "")
    {
		dept = deptId;
      //swal("Department must be Fill !!", "", "error");
      //return false;
    }
	else if(intermediaryCode = "")
	{
	  swal("Intermediary must be Fill !!", "", "error");
      return false;
    }
}
</script>