<div class="container">
  <div class="widget-content">
    <div class="tab-content">
      <div class="tab-pane active" id="formcontrols">
        <h3>USERS</h3>
        <br>
        <div class="widget widget-table action-table">
          
          <div class="widget-content">
            <div>
              <a href="home?page=muserslistdetail" class="btn btn-primary">Add</a>
            </div>
            <table id="query-table" class="display cell-border" cellspacing="0" style="width: 100px !important;">
              <thead>
                <tr>
                  <th></th>
                  <th>NAME</th>
                  <th>FULL NAME</th>
                  <th>BRANCH</th>
                  <th>DEPARTMENT</th>
				          <th>Intermediary</th>
                  <th></th>
				          <th>Reset</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  $query = "SELECT a.username
                            , (a.home_branch) as branch
                            , a.full_name
                            , ( a.dept_id) as Dept_Name,
							             intermediary_type 
                          FROM security_user a
                          LEFT JOIN master_branch b ON b.branch_code=a.home_branch

                          WHERE a.ISACTIVE=1 
                          ORDER BY a.home_branch, a.Dept_Id, a.username ";
                  $data  = mysqli_query($conn, $query) or die(mysqli_error($conn)); 
                  $no    = 1;
                  while ($queryRow =   mysqli_fetch_array($data)) {
                ?>
                <tr align="center">
                  <th><?php echo $no; ?> </th>
                  <td><?php echo $queryRow['username'] ?> </td>
                  <td><?php echo $queryRow['full_name'] ?> </td>
				  <?php 
				  
				  $queryDataBranch = "SELECT branch_name from master_branch where branch_code in ( ".$queryRow['branch']." )"; 
				  
				  //echo "AAAA ".$queryDataBranch;
				  
				  $dataBranch  = mysqli_query($conn, $queryDataBranch); 
				  $branchName = '';
				  while ($queryRowBranch =   mysqli_fetch_array($dataBranch)) {
						$branchName = $queryRowBranch['branch_name'] . "," . $branchName ;
				   }	
				   $branchName = substr($branchName, 0, strrpos($branchName, ',')); 
				  ?>
				  <td>
            <?php echo $branchName == null ? "ADMINISTRATOR" : $branchName ?> 
          </td>
				  
				  
				 
          <?php 
				 
				  $queryData = "SELECT description from mdepartment where id in ('".$queryRow['Dept_Name']."')"; 
				  $dataDepartment  = mysqli_query($conn, $queryData) or die(mysqli_error($conn)); 
				  $departmentName = '';
				  while ($queryRowDepartment =   mysqli_fetch_array($dataDepartment)) {
						$departmentName = $queryRowDepartment['description'] . "," . $departmentName ;
				   }	
				   $departmentName = substr($departmentName, 0, strrpos($departmentName, ',')); 
				  ?>
				  <td>
            <?php echo $departmentName == null ? "ADMINISTRATOR" : $departmentName ?> 
          </td>
				  
				  <?php 
					$intermediaryTypeArray = explode(",", $queryRow['intermediary_type']);
					$dataArray = "";
					for($i=0; $i < count($intermediaryTypeArray); $i++){
						$dataArray = $dataArray."'".$intermediaryTypeArray[$i]."',";
					}
					$dataArray = $dataArray."'1'";
				
				  $queryDataIntermediary = "SELECT intermediary_type_desc from master_intermediary where intermediary_type_desc in ( ".$dataArray." )"; 
				  
				  $dataIntermediary  = mysqli_query($conn, $queryDataIntermediary); 
				  $interediaryName = '';
				  while ($queryRowIntermediary =   mysqli_fetch_array($dataIntermediary)) {
						$interediaryName = $queryRowIntermediary['intermediary_type_desc'] . "," . $interediaryName ;
				   }	
				   $interediaryName = substr($interediaryName, 0, strrpos($interediaryName, ',')); 
				  ?>
				  <td>
            <?php echo $interediaryName == null ? "ADMINISTRATOR" : $interediaryName ?> 
          </td>
				  
                  <td><a href="home?page=muserslistdetail&form=edit&user=<?php echo $queryRow['username'] ?>" class="btn btn-primary">Detail</a></td>
				  <td>
            <input type="button" value="Reset" class="btn btn-primary"  OnClick="OnResetData('<?php echo $queryRow['username'] ?>')" />
          </td>

			   </tr>
                <?php $no++;} ?>
              </tbody>
            </table>
          </div>MD05
        </div>   
      </div> 
    </div>
  </div>
</div>

<script type="text/javascript">
	function OnResetData(pUserName){
		if(confirm('Are you sure want to Reset password ?')){
			alert('Reset User Name ' + pUserName);
		$.ajax({
          type  :   'POST',
          url   :   'ajax/ajax_reset.php',
          data  :   'UserName='+pUserName,
          success : function(data){
            alert(data);
          }
        });			


			
		}
	}

  $(function () {
        $("#query-table").DataTable({
          "paging":   true,
          "ordering": true,
          "scrollY": '45vh',
          "scrollX": true,
          "language": {
            "lengthMenu": "",
            "zeroRecords": "Nothing found - sorry",
            "info": "Showing page _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }
        });
    });
</script>