<?php
	if (isset($_SESSION['notif'])) {
		switch ($_SESSION['notif']) {
			case 'CHANGEPASS-SAVE':
				echo "<script>swal('PASSWORD HAS BEEN CHANGES', '', 'success'); </script>";
				break;
			case 'CHANGEPASS-SAVEFAILED':
				echo "<script>swal('PASSWORD FAILED TO UPDATE', '', 'error'); </script>";
				break;
			case 'CHANGEPASS-VALIDATE1':
				echo "<script>swal('THE PASSWORD YOU ENTERED IS INCORRECT', '', 'error'); </script>";
				break;
			case 'CHANGEPASS-VALIDATE2':
				echo "<script>swal('NEW PASSWORD MUST SAME WITH CONFIRM NEW PASSWORD', '', 'error'); </script>";
				break;

			case 'LOGINFAILED':
				echo "<script>swal('USERNAME AND PASSWORD INVALID', '', 'error'); </script>";
				break;
			case 'LOGINSUCCESS':
				echo "<script>swal('LOGIN SUCCESS...', 'Login As ".$_SESSION['username']."', 'success'); </script>";
				break;
				
			case 'CHECKER':
				echo "<script>swal('THE PAYMENT REQUEST ALREADY', 'CHECKER', 'success'); </script>";
				break;
			case 'APPROVE':
				echo "<script>swal('THE PAYMENT REQUEST ALREADY', 'APPROVE', 'success'); </script>";
				break;
			case 'REVISE':
				echo "<script>swal('THE PAYMENT REQUEST IS REVISE', '', 'success'); </script>";
				break;
			case 'REJECT':
				echo "<script>swal('THE PAYMENT REQUEST IS REJECT', '', 'success'); </script>";
				break;
			case 'RE-SUBMIT':
				echo "<script>swal('THE PAYMENT REQUEST ALREADY', 'RE-SUBMIT', 'success'); </script>";
				break;
			
			case 'PAYREQSUBMIT':
				echo "<script>swal('INPUT SUCCESS', 'PAYMENT NUMBER ".$_SESSION['paymentnumber']."', 'success'); </script>";
				unset($_SESSION['paymentnumber']);
				break;
			case 'PAYREQSUBMITFAILED':
				echo "<script>swal('INPUT PAYMENT REQUEST FAILED', '', 'error'); </script>";
				break;

			case 'PRINT_PR-FAILED':
				echo "<script>swal('FAILED TO PRINT, Please Contact Your Administrator', 'PRINT', 'error'); </script>";
				break;				
			
			case 'HOPAID':
				echo "<script>swal('THE PAYMENT REQUEST ALREADY', 'PAID', 'success'); </script>";
				break;
			case 'HOPAIDFAILED':
				echo "<script>swal('THE PAYMENT REQUEST FAILED', 'PAID', 'success'); </script>";
				break;
			
			case 'MTOP-SAVE':
				echo "<script>swal('TYPE OF PAYMENT ALREADY', 'SAVE', 'success'); </script>";
				break;
			case 'MTOP-SAVEFAILED':
				echo "<script>swal('TYPE OF PAYMENT FAILED TO SAVE, Please Contact Your Administrator', '', 'error'); </script>";
				break;
			case 'MTOP-DELETE':
				echo "<script>swal('TYPE OF PAYMENT ALREADY', 'DELETE', 'error'); </script>";
				break;
			case 'MTOP-DELETEFAILED':
				echo "<script>swal('TYPE OF PAYMENT FAILED TO DELETE , Please Contact Your Administrator', '', 'error'); </script>";
				break;
			case 'MTOP-SAVEExists':
				echo "<script>swal('TYPE OF PAYMENT FAILED TO SAVE, Please Contact Your Administrator', '', 'error'); </script>";
				break;
			
			case 'MGROUP-SAVE':
				echo "<script>swal('GROUP ALREADY', 'SAVE', 'success'); </script>";
				break;
			case 'MGROUP-SAVEFAILED':
				echo "<script>swal('GROUP FAILED TO SAVE, Please Contact Your Administrator', '', 'error'); </script>";
				break;
			case 'MGROUP-DELETE':
				echo "<script>swal('GROUP ALREADY', 'DELETE', 'error'); </script>";
				break;
			case 'MGROUP-DELETEFAILED':
				echo "<script>swal('GROUP FAILED TO DELETE , Please Contact Your Administrator', '', 'error'); </script>";
				break;
			case 'MGROUP-SAVEExists':
				echo "<script>swal('GROUP FAILED TO SAVE, Please Contact Your Administrator', '', 'error'); </script>";
				break;

			case 'MUSERS-SAVE':
				echo "<script>swal('USER ALREADY', 'SAVE', 'success'); </script>";
				break;
			case 'MUSERS-SAVEFAILED':
				echo "<script>swal('USER FAILED TO SAVE, Please Contact Your Administrator', '', 'error'); </script>";
				break;
			case 'MUSERS-DELETE':
				echo "<script>swal('USER ALREADY', 'DELETE', 'error'); </script>";
				break;
			case 'MUSERS-DELETEFAILED':
				echo "<script>swal('USER FAILED TO DELETE , Please Contact Your Administrator', '', 'error'); </script>";
				break;
			case 'MUSERS-SAVEExists':
				echo "<script>swal('USER FAILED TO SAVE, Please Contact Your Administrator', '', 'error'); </script>";
				break;

			case 'GROUPMENU-SAVE':
				echo "<script>swal('GROUP MENU ALREADY', 'SAVE', 'success'); </script>";
				break;
			case 'GROUPMENU-SAVEFAILED':
				echo "<script>swal('GROUP MENU FAILED TO SAVE', '', 'error'); </script>";
				break;

			case 'USERGROUP-SAVE':
				echo "<script>swal('USER GROUP ALREADY', 'SAVE', 'success'); </script>";
				break;
			case 'USERGROUP-SAVEFAILED':
				echo "<script>swal('USER GROUP FAILED TO SAVE', '', 'error'); </script>";
				break;
				
			case 'NOTADMIN':
				echo "<script>swal('YOU DON'T HAVE ACCESS THIS MENU', '', 'error'); </script>";
				break;
			case 'INSTALLSUCCESS':
				echo "<script>swal('INSTALLMENT ALREADY', 'CREATE', 'success'); </script>";
				break;
			case 'INSTALLSUBMIT':
				echo "<script>swal('INSTALLMENT ALREADY', 'SUBMIT', 'success'); </script>";
				break;
			case 'INSTALLFAILED':
				echo "<script>swal('INSTALLMENT FAILED', '', 'error'); </script>";
				break;
			case 'SUKSES-KOREKSI':
				echo "<script>swal('CORECTION SUCCESS', '', 'success'); </script>";
				break;
			case 'GAGAL-KOREKSI':
				echo "<script>swal('CORECTION FAILED', '', 'error'); </script>";
				break;
			case 'NOT-KOREKSI':
				echo "<script>swal('CORECTION NOT FOUND', '', 'error'); </script>";
				break;
		}
		unset($_SESSION['notif']);
	}

?>

