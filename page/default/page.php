<?php 
	if (empty($_SESSION['username'])) {
		header('location:page/login.php');
	}
	else
	{
		if (empty($_GET['page'])) 
		{
			include "page/default/home.php";
		}
		else
		{
			switch ($_GET['page']) {
				// New PayReq
				case 'payreqnew':
					$role = 'payreqnew';
					// $query = "SELECT a.* FROM security_user_menu a 
					// 		JOIN  security_menu b ON b.ID=a.MenuID
					// 		WHERE b.ISACTIVE=1 
					// 			AND a.ISACTIVE=1 
					// 			AND a.username= '".$_SESSION['username']."' 
					// 			AND b.PAGE='$role' ";
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/paymentrequest/new-layout-1.php";
					}
					else
					{
						header('location:page/login.php');
						break;
					}
					//include "page/paymentrequest/new-layout-1.php";
					break;
				// List PayReq
				case 'payreqlist':
					$role = 'payreqlist';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
								
								
					//echo "SQL ".$query;
					//die();
					
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/paymentrequest/list.php";
					}
					else
					{
						header('location:page/login.php');
						//header('location:index.php');
						break;
					}
					//include "page/paymentrequest/list.php";
					break;
				// List PayReq => detail
				case 'payreqlistdetail':
					$role = 'payreqlist';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/paymentrequest/listdetail.php";
					}
					else
					{
						header('location:page/login.php');
						break;
					}
					//include "page/paymentrequest/listdetail.php";
					break;
				// Home (List Approval PayReq) => detail
				case 'payreqapprovalview':
					include "page/paymentrequest/approvalview.php";
					break;
				//masih belum diberesin karna hars cek per polis klo di uncek
				case 'payreqrevise':
					$role = 'payreqlist';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/paymentrequest/edit.php";
					}
					else
					{
						header('location:page/login.php');
						break;
					}
					//include "page/paymentrequest/edit.php";
					break;
				//finance pusat 
				case 'payreqho':
					$role = 'payreqho';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/paymentrequest/headofficelist.php";
					}
					else
					{
						header('location:page/login.php');
						break;
					}
					//include "page/paymentrequest/headofficelist.php";
					break;
				//finance pusat view detail
				case 'payreqhodetail':
					$role = 'payreqho';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/paymentrequest/headofficelistdetail.php";
					}
					else
					{
						header('location:page/login.php');
						break;
					}
					//include "page/paymentrequest/headofficelistdetail.php";
					break;
				//print
				case 'payreqreport':
					include "report/report_payment_request.php";
					break;
				case 'printpayreq':
					$role = 'printpayreq';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						$file = "page/report/payreq_PerPeriode.php";
						include $file; 
					}
					else
					{
						header('location:page/login.php');
						break;
					}
					//include "page/MasterData/typeofpaymentlist.php";
					break;

				case 'reportFinance':
					$role = 'reportFinance';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/report/report_finance.php";
					}
					else
					{
						header('location:page/login.php');
						break;
					}
					//include "page/paymentrequest/headofficelist.php";
					break;
				
				
				//MasterData
				case 'mtypeofpaymentlist':
					$role = 'mtypeofpaymentlist';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 AND a.Dept_Id=0
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/MasterData/typeofpaymentlist.php";
					}
					else
					{
						//header('location:page/login.php');
						$_SESSION['notif'] = 'NOTADMIN';
						break;
					}
					//include "page/MasterData/typeofpaymentlist.php";
					break;
				case 'mtypeofpaymentlistdetail':
					$role = 'mtypeofpaymentlist';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 AND a.Dept_Id=0
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/MasterData/typeofpaymentlistdetail.php";
					}
					else
					{
						//header('location:page/login.php');
						$_SESSION['notif'] = 'NOTADMIN';
						break;
					}
					//include "page/MasterData/typeofpaymentdetail.php";
					break;
				case 'mgrouplist':
					$role = 'mgrouplist';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 AND a.Dept_Id=0
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/MasterData/grouplist.php";
					}
					else
					{
						//header('location:page/default/home.php');
						$_SESSION['notif'] = 'NOTADMIN';
						break;
					}
					//include "page/MasterData/typeofpaymentlist.php";
					break;
				case 'mgrouplistdetail':
					$role = 'mgrouplist';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 AND a.Dept_Id=0
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/MasterData/grouplistdetail.php";
					}
					else
					{
						//header('location:page/login.php');
						$_SESSION['notif'] = 'NOTADMIN';
						break;
					}
					//include "page/MasterData/typeofpaymentdetail.php";
					break;


				case 'muserslist':
					$role = 'muserslist';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 AND a.Dept_Id=0
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/MasterData/userslist.php";
					}
					else
					{
						//header('location:page/login.php');
						$_SESSION['notif'] = 'NOTADMIN';
						break;
					}
					//include "page/MasterData/typeofpaymentlist.php";
					break;
				case 'muserslistdetail':
					$role = 'muserslist';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 AND a.Dept_Id=0
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/MasterData/userslistdetail.php";
					}
					else
					{
						//header('location:page/login.php');
						$_SESSION['notif'] = 'NOTADMIN';
						break;
					}
					//include "page/MasterData/typeofpaymentdetail.php";
					break;


				//SETUP
				case 'groupmenulistdetail':
					$role = 'groupmenulistdetail';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 AND a.Dept_Id=0
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/setup/groupmenulistdetail.php";
					}
					else
					{
						//header('location:page/login.php');
						$_SESSION['notif'] = 'NOTADMIN';
						break;
					}
					break;
				case 'usergrouplistdetail':
					$role = 'usergrouplistdetail';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 AND a.Dept_Id=0
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/setup/usergrouplistdetail.php";
					}
					else
					{
						header('location:page/login.php');
						$_SESSION['notif'] = 'NOTADMIN';
						break;
					}
					break;
				// List PayReq => Installment List
				case 'installment':
					$role = 'installment';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
							
					
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/installment/installment.php";
					}
					else
					{
						header('location:page/login.php');
						//header('location:index.php');
						break;
					}
					//include "page/paymentrequest/list.php";
				 	break;

                case 'installList':
                    $role = 'installList';
                    $query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";


                    $data  		= mysqli_query($conn, $query);
                    $totalrow	= mysqli_num_rows($data);
                    if($totalrow > 0)
                    {
                        include "page/installment/installmentchecker.php";
                    }
                    else
                    {
                        header('location:page/login.php');
                        //header('location:index.php');
                        break;
                    }
                    //include "page/paymentrequest/list.php";
                    break;
                    // INSTALLMENT PAID DETAIL
                case 'installmentListDetail':
					$role = 'installList';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/installment/installmentListDetail.php";
					}
					else
					{
						header('location:page/login.php');
						break;
					}
					//include "page/paymentrequest/listdetail.php";
					break;


				 	// installment detail
				 case 'installmentdetail':
					$role = 'installment';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/installment/installmentdetail.php";
					}
					else
					{
						header('location:page/login.php');
						break;
					}
					//include "page/paymentrequest/listdetail.php";
					break;

				// case 'installdetailapprove':
				// 	$role = 'installChecker';
				// 	$query = "SELECT a.* FROM security_user a 
				// 			JOIN security_user_group b ON b.username = a.username
				// 			JOIN security_group_menu c ON c.group_id = b.group_id
				// 			JOIN  security_menu d ON d.ID=c.menu_id
				// 			WHERE a.ISACTIVE=1 
				// 				AND a.username= '".$_SESSION['username']."' 
				// 				AND d.PAGE='$role' ";
				// 	$data  		= mysqli_query($conn, $query);
				// 	$totalrow	= mysqli_num_rows($data);
				// 	if($totalrow > 0)
				// 	{
				// 		include "page/installment/installdetailapprove.php";
				// 	}
				// 	else
				// 	{
				// 		header('location:page/login.php');
				// 		break;
				// 	}
				// 	//include "page/paymentrequest/listdetail.php";
				// 	break;

				

				case 'addinstall':
					$role = 'installList';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/installment/addinstallment.php";
					}
					else
					{
						header('location:page/login.php');
						break;
					}
					//include "page/paymentrequest/listdetail.php";
					break;
				// // List PayReq => detail
				// case 'installChecker':
				// 	$role = 'installChecker';
				// 	$query = "SELECT a.* FROM security_user a 
				// 			JOIN security_user_group b ON b.username = a.username
				// 			JOIN security_group_menu c ON c.group_id = b.group_id
				// 			JOIN  security_menu d ON d.ID=c.menu_id
				// 			WHERE a.ISACTIVE=1 
				// 				AND a.username= '".$_SESSION['username']."' 
				// 				AND d.PAGE='$role' ";
								
								
				// // 	//echo "SQL ".$query;
				// // 	//die();
					
				// 	$data  		= mysqli_query($conn, $query);
				// 	$totalrow	= mysqli_num_rows($data);
				// 	if($totalrow > 0)
				// 	{
				// 		include "page/installment/installmentchecker.php";
				// 	}
				// 	else
				// 	{
				// 		header('location:page/login.php');
				// 		//header('location:index.php');
				// 		break;
				// 	}
				// 	//include "page/paymentrequest/list.php";
				//  	break;
				 	// Installment Approve
				 case 'installmentListFinance':
					$role = 'installmentListFinance';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";	
					
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/installment/installmentListFinance.php";
					}
					else
					{
						header('location:page/login.php');
						//header('location:index.php');
						break;
					}
					//include "page/paymentrequest/list.php";
				 	break;
				 case 'installmentListPaid':
					$role = 'installmentListPaid';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
							
					
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/installment/installmentheadoffice.php";
					}
					else
					{
						header('location:page/login.php');
						//header('location:index.php');
						break;
					}
					//include "page/paymentrequest/list.php";
				 	break;
				 case 'installmentPaidDetail':
					$role = 'installmentListPaid';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/installment/installmentheadofficedetail.php";
					}
					else
					{
						header('location:page/login.php');
						break;
					}
					//include "page/paymentrequest/listdetail.php";
					break;
				case 'reportInstallment':
					$role = 'reportInstallment';
					$query = "SELECT a.* FROM security_user a 
							JOIN security_user_group b ON b.username = a.username
							JOIN security_group_menu c ON c.group_id = b.group_id
							JOIN  security_menu d ON d.ID=c.menu_id
							WHERE a.ISACTIVE=1 
								AND a.username= '".$_SESSION['username']."' 
								AND d.PAGE='$role' ";
					$data  		= mysqli_query($conn, $query);
					$totalrow	= mysqli_num_rows($data);
					if($totalrow > 0)
					{
						include "page/report/report_installment.php";
					}
					else
					{
						header('location:page/login.php');
						break;
					}
					//include "page/paymentrequest/listdetail.php";
					break;
			}
		}
		$timeout = 60; // Set timeout menit
		$logout_redirect_url = "index.php"; // Set logout URL
		$timeout = $timeout * 260; // Ubah menit ke detik
		if (isset($_SESSION['start_time'])) {    
			$elapsed_time = time() - $_SESSION['start_time'];    
			if ($elapsed_time >= $timeout) {        
				
				$LOGIN_QUERY 	 =	mysqli_query($conn, "UPDATE security_user SET Login=0 where username='".$_SESSION['username']."' ");
				session_destroy();        
				echo "<script>alert('Session Anda Telah Habis!'); window.location = '$logout_redirect_url'</script>";   
			}
		}
		$_SESSION['start_time'] = time();
	}
 ?>