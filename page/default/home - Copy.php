<?php 
if (empty($_SESSION['username'])) 
{
  header('location:page/login.php');
}
else
{
  $username = $_SESSION['username'];
  $group_id = $_SESSION['group_id'];
  $dept_id = $_SESSION['dept_id'];
  
	$query 		= "SELECT Value FROM mgeneral_table WHERE Code='BRANCH_HO'";
	$resultSet       = mysqli_query($conn, $query);
	$data  = mysqli_fetch_array($resultSet);
	$branchHOCode = $data['Value'];
	
	$query = "SELECT Value FROM mgeneral_table WHERE Code='DEPARTMENT_FINANCE_PUSAT'";
	$resultSet       = mysqli_query($conn, $query);
	$data  = mysqli_fetch_array($resultSet);
	
	$deptIdFinancePusat = $data['Value'];
	
	$branchCodeArray = explode(',', $_SESSION['branch_code']);
	
	$ketemu1 = false;
	for($i=0; $i < count($branchCodeArray); $i++){
		if($branchHOCode == $branchCodeArray[$i]){
			$ketemu1 = true;
			
		}
	}

	$deptIdDataArray = '';
	$ketemu2 = false;
	if($ketemu1){
		$deptIdArray = explode(',', $_SESSION['dept_id']);
		
		
		for($i=0; $i < count($deptIdArray); $i++){
			
			if($deptIdFinancePusat == $deptIdArray[$i]){
					$ketemu2 = true;
				
					//break;
			}
		
		}
	}
	
	$sobStr = $_SESSION['intermediary_codeStr'];
	$branchCodeStr = $_SESSION['branch_codeStr'];
	$deptIdStr = $_SESSION['dept_idStr'];
	
	
  
	/*
  $query      = 'SELECT * FROM security_user 
                  WHERE ISACTIVE=1
                    AND username='$username' 
                    AND home_branch = (SELECT Value FROM mgeneral_table WHERE Code='BRANCH_HO')
                    AND Dept_id IN (SELECT Value FROM mgeneral_table WHERE Code='DEPARTMENT_FINANCE_PUSAT')';

  $data       = mysqli_query($conn, $query);
  $dataRow    = $data->num_rows;
  */
  
  $dataRow = 0;
  
  if($ketemu1 || $ketemu2){
	
	$dataRow = 1;
  }
 
 
  //$dataRow = 0;

  $queryHome = "SELECT a.* FROM tpaymentrequestheader a
                        JOIN security_user b ON b.username=a.created_by
                        JOIN (
                            SELECT DISTINCT c.username,c.home_branch,c.dept_id,a.id AS group_id 
                                  , (CASE a.id 
                                      WHEN 2 THEN (SELECT Description FROM mgeneral_table WHERE Code='PaymentRequest_STATUS' AND Value=4) 
                                      WHEN 3 THEN (SELECT Description FROM mgeneral_table WHERE Code='PaymentRequest_STATUS' AND Value=1) 
                                      WHEN 4 THEN (SELECT Description FROM mgeneral_table WHERE Code='PaymentRequest_STATUS' AND Value=2) 
                                      WHEN 5 THEN (SELECT Description FROM mgeneral_table WHERE Code='PaymentRequest_STATUS' AND Value=5) 
                                      ELSE '' 
                                    END) AS STATUS
                            FROM security_group a 
                            JOIN security_user_group b on b.group_id=a.id
                            JOIN security_user c on c.username=b.username
                            WHERE c.ISACTIVE=1 
                              AND a.ID in (1,2,3,4,5)
                              AND c.username='".$_SESSION['username']."'
                            UNION ALL
                            SELECT DISTINCT c.username,c.home_branch,c.dept_id,a.id as group_id 
                              , (CASE a.id 
                                  WHEN 3 THEN (SELECT Description FROM mgeneral_table WHERE Code='PaymentRequest_STATUS' AND Value=6) 
                                  ELSE '' 
                                END) AS STATUS
                            FROM security_group a 
                            JOIN security_user_group b on b.group_id=a.id
                            JOIN security_user c on c.username=b.username
                            WHERE c.ISACTIVE=1 
                              AND a.ID in (1,3,4) 
                              AND c.username='".$_SESSION['username']."'
                        ) c ON (
                              (
                                
                                 c.group_id in (2) 
                                AND a.created_by=c.username
                              )
                              OR
                              (
                                c.STATUS = a.STATUS_PR 
                                AND c.group_id in (3)                                
                                AND a.CREATED_BY!=c.username
                              )
                               OR
                              (
                                c.STATUS = a.STATUS_PR 
                                AND c.group_id in (4) 
                              
                                AND a.CREATED_BY!=c.username
                              )
                              OR
                              (
                                c.STATUS = a.STATUS_PR 
                                AND c.group_id in (4) 
                               
                                AND a.CREATED_BY!=c.username
                              )
                              OR
                              (
                                c.STATUS = a.STATUS_PR 
                                AND a.STATUS_PAID = '' AND a.VoucherNo = ''
                                AND c.group_id = 5 
                               
                                AND a.CREATED_BY!=c.username
                              )  
                                
                                
                              
                            )
                        WHERE 1=1
                          AND a.ISACTIVE=1 
						  and a.intermediary_type_code in (".$sobStr.") and a.branch_code in (".$branchCodeStr.")
                        ORDER BY a.CREATED_DATE DESC";
	//echo $queryHome;
	//echo "EEEE ".$dataRow;
	//die();
	
  if($dataRow == 0)
  {
?>
<!--
<div class='container'>
	<button id='tutorial' class='btn-primary' name='tutorial'>Video tutorial</button>
</div>
-->

<div class='container'>
  <div class='widget-content'>
    <div class='tab-content'>
      <div class='tab-pane active' id='formcontrols'>
        <div class='widget widget-table action-table'>
          <div class='widget-content'>
            <h3>PENDING REQUEST</h3>
            <table id='pRequest-table' class='display cell-border' cellspacing='0' width='100%'>
              <thead>
                <tr>
                  <th>NO</th>
                  <th>PAYMENT REQ NO </th>
                  <th>INTERMEDIARY NAME </th>
                  <th>BANK</th>
                  <th>REQUEST DATE</th>
                  <th>REQUEST STATUS</th>
                  <th>PAID STATUS</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php
				//echo 'AAA '.$queryHome;
				//die();
                    $dataHome = mysqli_query($conn, $queryHome);
                    $no       =   1;
                   // echo $dataHome->num_rows;
					while ($homeRow = mysqli_fetch_array($dataHome)) {
                ?>
                <tr align='center'>
                  <th><?php echo $no; ?> </th>
                  <td><?php echo $homeRow['PaymentRequestNo'] ?> </td>
                  <td><?php echo $homeRow['BeneficiaryName'] ?> </td>
                  <td><?php echo $homeRow['Bank'] ?> </td>
                  <td><?php echo $homeRow['CREATED_DATE'] ?> </td>
                  <td><?php echo $homeRow['STATUS_PR']; ?> </td>
                  <td><?php echo $homeRow['STATUS_PAID']; ?></td>
                  <td>
                    <a href='?page=payreqapprovalview&id=<?php echo $homeRow['ID'] ?>' class='btn btn-primary'>Detail</a>
                  </td>
                </tr>
                <?php $no++;} ?>
              </tbody>
            </table>
          </div>PR05
        </div> 
      </div> 
    </div>
  </div>
</div>              
<?php 
  } 
  else 
  {
?>
<div class='container'>
  <div class='widget-content'>
    <div class='tab-content'>
      <div class='tab-pane active' id='formcontrols'>
        <div class='widget widget-table action-table'>
          <div class='widget-content'>
            <h3>PENDING REQUEST</h3>
            <table id='pReqtable' class='display cell-border' cellspacing='0' width='100%'>
              <thead>
                <tr>
                  <th>NO</th>
                  <th>PAYMENT REQ NO </th>
                  <th>INTERMEDIARY NAME </th>
                  <th>BANK</th>
                  <th>REQUEST DATE</th>
                  <th>REQUEST STATUS</th>
                  <th>PAID STATUS</th>
                  <th>NO VOUCHER</th>
                  <th>DATE</th>
                  <th></th>
                  <th style='display:none;'></th>
                </tr>
              </thead>
              <tbody>
                <?php
                    $dataHome = mysqli_query($conn, $queryHome);
                    $no       =   1;
                    while ($homeRow = mysqli_fetch_array($dataHome)) {
                ?>
                <tr align='center'>
                  <th><?php echo $no; ?> </th>
                  <td><?php echo $homeRow['PaymentRequestNo'] ?> </td>
                  <td><?php echo $homeRow['BeneficiaryName'] ?> </td>
                  <td><?php echo $homeRow['Bank'] ?> </td>
                  <td><?php echo $homeRow['CREATED_DATE'] ?> </td>
                  <td><?php echo $homeRow['STATUS_PR']; ?> </td>
                  <td><?php echo $homeRow['STATUS_PAID']; ?></td>
                  <td>
                    <input type='text' maxlength='30' name='VoucherNo' class='span1' id='VoucherNo' style='width:100px; font-size:13px !important;'>
                  </td>
                  <td>
                    <input type='text' name='VoucherDate' class='date span1' id='VoucherDate' style='width:70px;font-size:13px !important;' >
                  </td>
                  <td>
                    <button class='btn-primary'>PAID</button>
                    <!-- <input type='button' name='finance-paid' id='finance-paid' style='float:right;' class='btn btn-primary' value='PAID'> -->
                    <!-- <button type='button' id='finance-paid' class='btn btn-primary'>PAID</button> -->
                    <!-- <a href='' data-PayReqID='<?php echo $homeRow['ID']; ?>' class='btn btn-primary' id='btn-finance-paid' name='btn-finance-paid'>PAID</a> -->
                    <!-- <a href='?page=payreqapprovalview&id=<?php echo $homeRow['ID'] ?>' class='btn btn-primary'>Detail</a> -->
                  </td>
                  <td style='display:none;'>
                    <?php echo $homeRow['ID'] ?>
                  </td>
                </tr>
                <?php $no++;} ?>
              </tbody>
            </table>
          </div> PR05
        </div> 
		
      </div> 
    </div>
  </div>
</div>
<?php
  }
}
?>


<script type='text/javascript'>
	$(function () {
      $('#pRequest-table').DataTable({
      	'paging':   false,
      	'ordering': true,
      	'scrollY': '45vh',
     		'scrollX': true,
     		'language': {
          'lengthMenu': 'Display records per page',
          'zeroRecords': 'Nothing found - sorry',
          'info': 'Showing page _PAGE_ of _PAGES_',
          'infoEmpty': 'No records available',
          'infoFiltered': '(filtered from _MAX_ total records)',
      }
      });
      $('#pReqtable').DataTable({
        'paging':   false,
        'ordering': false,
        'scrollY': '45vh',
        'scrollX': true,
        'language': {
          'lengthMenu': 'Display records per page',
          'zeroRecords': 'Nothing found - sorry',
          'info': 'Showing page _PAGE_ of _PAGES_',
          'infoEmpty': 'No records available',
          'infoFiltered': '(filtered from _MAX_ total records)',
      }
      });
  });
</script>

<script type='text/javascript'>
function setModalTitle(data) {

    if (data.length === 0 ) {
    alert('a');
    }else{
    alert('b');
    alert(data);
    }
    // do something different
    
}
$(document).ready(function(){
  var table = $('#pReqtable').DataTable();
  

    $('#pReqtable tbody').on( 'click', 'button', function () {
      
      var data = table.row( $(this).parents('tr') ).data();
      var idx = data[0] - 1;
      var No = data[0];
      var PayReqNo = data[1];
      var PayReqID = data[10];
      var CountAlert = 0;
      var dataRow = table.$('input, VoucherNo').serialize();

      var res = dataRow.split('&');
      var colVNo = res[((((idx+1)*2)-1)-1)];
      var colVDate = res[(((idx+1)*2)-1)];

      var res1 = colVNo.split('=');
      var colVNoValue = res1[1];
      
      var res2 = colVDate.split('=');
      var colVDateValue = res2[1];
      
      if(colVNoValue == '')
      {
        swal('Voucher No is Empty', '', 'error');
        return false;
      }
      else if(colVDateValue == '')
      {
        swal('Date is Empty', '', 'error');
        return false; 
      }
      else if(colVNoValue != '' && colVDateValue != '')
      {
        $.ajax({
          type  :   'POST',
          url   :   'ajax/ajax_type2.php',
          data  :   'VoucherNo='+colVNoValue,
          success : function(data){
            if(data != '')
            {
              if(!confirm(colVNoValue + ' Already Exists. Are you sure you want to continue ?')){
                return false;
              }
            }
            window.location.assign('fungsi/paymentrequest/financeAction.php?id='+ PayReqID +'&VoucherNo='+ colVNoValue +'&VoucherDate='+ colVDateValue +' ');
          }
        });
        //window.location.assign('fungsi/paymentrequest/financeAction.php?id='+ PayReqID +'&VoucherNo='+ colVNoValue +'&VoucherDate='+ colVDateValue +' ');
      }

      
      
    });
});
</script>