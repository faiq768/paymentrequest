<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>LOGIN - PT ASURANSI DAYIN MITRA TBK</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
    
	<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="../css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />

	<link href="../css/font-awesome.min.css" rel="stylesheet">
	    
	    
	<link href="../css/style.css" rel="stylesheet" type="text/css">
	<link href="../css/pages/signin.css" rel="stylesheet" type="text/css">
	<script src="../js/jquery-3.1.1.min.js"></script> 
	<script src="../js/sweetalert.min.js"></script>
	
</head>
<style type="text/css">
	body {
		background: url('../img/bg.jpg') !important;
		 background-repeat: no-repeat !important;
		background-size: 100% auto !important;
	}
	.navbar-inner{
		background: rgba(0,0,0,0.5) !important;
	}
	.account-container{
		background: rgba(0,0,0,0.3);
	}
	.login-fields input{
		background-color: rgba(0,0,0,0.2);
		color: #fff;
	}
	.content, .content h1{
		color: #fff;
	}
	.btn-log-in{
		background: rgba(5, 174, 254, 0.5);
		border: 1px solid #fff;
	}
	.btn-log-in:hover{
		background: rgba(5, 174, 254, 0.9);
	}
</style>

<body>
	
 <div class="modal fade" id="warningDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Testing Server</h4>
            </div>
            <div class="modal-body" id="data-intercode">
                <h2><font color="red">This is was Testing server , not using real data. for production server , please contact MIS</font></h2>
            </div>
        </div>
    </div>
</div>
 
	<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container">
			
			
			<a class="brand" href="#">
				PT ASURANSI DAYIN MITRA TBK				
			</a>		
			
				
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->


<div class="account-container">
<?php session_start();
	include "../page/default/notif.php"; 
?>
	<div class="content clearfix">
		
		<form action="../fungsi/login.php" method="post">
		
			<h1>PAYMENT REQUEST</h1>		
			
			<div class="login-fields">
				
				<p>Please provide your details</p>
				
				<div class="field">
					<label for="usname">Username</label>
					<input type="text" autocomplete="off" onkeyup="this.value = this.value.toUpperCase()" id="usname" name="username" value="<?php if (isset($_SESSION['username'])) echo $_SESSION['username'] ?>" placeholder="Username" class="login username-field" autofocus />
				</div> <!-- /field -->
				
				<div class="field">
					<label for="password">Password:</label>
					<input type="password" onkeyup="this.value = this.value.toUpperCase()" id="password" name="password" value="" placeholder="Password" class="login password-field"/>
				</div> <!-- /password -->
				
			</div> <!-- /login-fields -->
			
			<div class="login-actions">
				<button type="submit" name="login" class="btn-log-in button btn btn-success btn-large">Log In</button>
				
			</div> <!-- .actions -->

		</form>
		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->
<?php unset($_SESSION['username']); ?>
<script src="../js/jquery-1.7.2.min.js"></script>
<script src="../js/bootstrap.js"></script>

<script src="../js/signin.js"></script>
<script>

		  $('#warningDiv').modal('show');
	
</script>
</body>

</html>
