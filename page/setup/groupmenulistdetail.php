<?php 
/*
** Detail                     **
** Created By Hockey          **
** Created Date 20180402      **
** Setup Menu per Group       **
** START                      **
*/
?>

<div class="col-lg-12 main-form">
  <form action="fungsi/setup/groupmenu.php" method="post" id="myform"> 
    <div class="span12 step-1 aktif" style="margin-left:0 !important;">
      <div class="widget-content">
        <h3>GROUP MENU</h3>
        <div class="tab-content">
          <div class="tab-pane pane-1 active" id="formcontrols">
            <div id="groupmenu-edit" class="form-horizontal">
              <fieldset class="span11">
                
                <div class="control-group">                     
                  <label class="control-label size-16" for="type">Group</label>
                  <div class="controls">
                    <?php 
                      $query = "SELECT * FROM security_group WHERE ISACTIVE=1 ";

                      $data       = mysqli_query($conn, $query);
                      
                      $select= '<select name="group" onchange="dropdown_change(this)">';
                      $select.='<option value=0>--SELECTED--</option>';
                      while($rs = mysqli_fetch_array($data)){
                        $select.='<option value="'.$rs['ID'].'">'.$rs['NAME'].'</option>';
                      }
                      $select.='</select>';
                      echo $select;
                    ?>
                  </div> <!-- /controls -->       
                </div> <!-- /control-group -->

                <div class="control-group">                     
                  <label class="control-label size-16" for="type">Menu</label>
                  <div class="controls" id="data-menu" name="data-menu">
                    
                  </div>
                </div>

                <div class="control-group">
                  <hr>
                  <a href="#!" class="btn-groupmenu"><input type="submit" name="groupmenu-save" style="float:left;" class="btn btn-primary" value="SAVE"></a>
                </div>
              </fieldset>
              
            </div> <!-- /form-horizontal -->
          </div> <!-- /tab-pane -->
        </div> <!-- /tab-content -->ST01
      </div> <!-- /widget-content -->
    </div> <!-- /span6 -->
  </form>
</div>

<script>
  //the function that takes care of the updating
  function dropdown_change(el){
      var elValue = el.value;
      //ajax
      $.ajax({
        type  :   'POST',
        url   :   'ajax/ajax_type.php',
        data  :   'group_id='+elValue,
        success : function(response){
          $('#data-menu').html(response);
        }
      });
  }
</script>