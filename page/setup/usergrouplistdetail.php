<?php 
/*
** Detail                     **
** Created By Hockey          **
** Created Date 20180403      **
** Setup User Group           **
** START                      **
*/
?>

<div class="col-lg-12 main-form">
  <form action="fungsi/setup/usergroup.php" method="post" id="myformData" onsubmit="return validateForm()"> 
    <div class="span12 step-1 aktif" style="margin-left:0 !important;">
      <div class="widget-content">
        <h3>USER GROUP</h3>
        <div class="tab-content">
          <div class="tab-pane pane-1 active" id="formcontrols">
            <div id="musergroup" class="form-horizontal">
              <fieldset class="span11">

                <div class="control-group">                     
                  <label class="control-label size-16" for="code">Username</label>
                  <div class="controls">
                    <input type="text" name="username" id="iusername" class="span3" onkeyup="this.value = this.value.toUpperCase()" style="font-size:13px !important;" readonly>
                    <a href="#" data-toggle="modal" data-target="#user-lookup" ><i class="fa fa-search" aria-hidden="true"></i></a>
                  </div> <!-- /controls -->       
                </div> <!-- /control-group -->

                <div class="control-group">                     
                  <label class="control-label size-16" for="type">Group</label>
                  <div class="controls" id="data-group" name="data-group">
                    
                  </div>
                </div>

                <fieldset class="span10">
                  <hr>
                  <!-- <a href="#" id="save-mtop" class="btn btn-primary" style="float:left;" >Save</a> -->
                  <a href="#!" class="btn-usergroup-save"><input type="submit" name="usergroup-save" style="float:left;" class="btn btn-primary" value="SAVE"></a>
                </fieldset>
                
              </fieldset>
            </div> <!-- /form-horizontal -->
          </div> <!-- /tab-pane -->
        </div> <!-- /tab-content -->
        ST02
      </div> <!-- /widget-content -->
    </div> <!-- /span6 -->
  </form>
</div>

<?php include "page/lookup/users.php"; ?>
<script>
	function validateForm(){
		var groupChecked = '';
		var pesan = '';
		$('[name="group[]"]').each( function (){
			if($(this).prop('checked') == true){
				groupChecked += $(this).val();
				if(groupChecked == 42){
					pesan = "Maker cannot be Approval Data Entry !";
				
				}
				
				if(groupChecked == 32){
					 pesan = "Maker cannot be Checked Data Entry !";
				
				}	
				
				
			}
		});
		
		if(pesan != ''){
			swal(pesan, "", "error");
			return false;
		}
		return true;
	}
</script>

