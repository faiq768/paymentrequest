<div class="container">
  <div class="widget-content">
    <div class="tab-content">
      <div class="tab-pane active" id="formcontrols">
        <div class="widget widget-table action-table">
          <div class="widget-content">
            <h3>REPORT PAYMENT REQUEST</h3>
            <fieldset class="span6">
              <?php
                $query = "SELECT a.*, b.branch_name 
                        FROM security_user a
                        LEFT JOIN master_branch b ON b.branch_code=a.home_branch
                        LEFT JOIN mdepartment c ON c.ID=a.Dept_Id
                        WHERE a.username='".$_SESSION['username']."' 
                            
                        ";
        
                $data = mysqli_query($conn, $query);
                $dataRow = $data->num_rows;
                ?>
                <div class="control-group">
                  <label class="control-label size-16 span2" for="users">Branch</label>
                  <div class="controls">
                    <input type="text" value="<?php echo $row['home_branch']; ?>" name="nbranchcode" class="span1" id="ibranchcode" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>
                    <a href="#" data-toggle="modal" data-target="#branch-lookup" ><i class="fa fa-search" aria-hidden="true"></i></a>
                    <input type="text" value="<?php echo $row['branch_name']; ?>" name="nbranchname" class="span2" id="ibranchname" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>
                    
                  </div>
                </div>
              <div class="control-group">
                  <label class="control-label size-16 span2" for="users">Intermediary</label>
                  <div class="controls">
                    <input type="text" value="<?php echo $row['home_branch']; ?>" name="iKodeIntermediary" class="span1" id="iKodeIntermediary" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>
                    <a href="#" data-toggle="modal" data-target="#financeIntermediary" ><i class="fa fa-search" aria-hidden="true"></i></a>
                    <input type="text" value="<?php echo $row['branch_name']; ?>" name="iNamaIntermediary" class="span2" id="iNamaIntermediary" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>
                    
                  </div>
                </div>
              <div class="control-group">
                  <label class="control-label size-16 span2" for="users">Currency</label>
                  <div class="controls">
                    <input type="text" value="<?php echo $row['home_branch']; ?>" name="icodeCurrency" class="span1" id="icodeCurrency" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>
                    <a href="#" data-toggle="modal" id="financeCurrency" data-target="#currencyFinance" ><i class="fa fa-search" aria-hidden="true"></i></a>
                    <input type="text" value="<?php echo $row['branch_name']; ?>" name="inameCurrency" class="span2" id="inameCurrency" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>
                    
                  </div>
                </div>
              <div class="control-group">                     
                <label class="control-label size-16 span2" for="period">Production Period</label>
                <div class="controls">
                  <input type="text" name="periode1" class=" date periode-1 span1" id="period-1" style="width : 80px;font-size:13px !important;" required>
                  &nbsp;S / D&nbsp;
                  <input type="text" name="periode2" class=" date periode-2 span1" id="period-2" style="width : 80px;font-size:13px !important;" required>
                </div>     
              </div> 
              <div class="control-group">                     
                <label class="control-label size-16 span2" for="period">Payment Date</label>
                <div class="controls">
                  <input type="text" name="payment1" class=" date periode-1 span1" id="payment-1" style="width : 80px;font-size:13px !important;" required>
                  &nbsp;S / D&nbsp;
                  <input type="text" name="payment2" class=" date periode-2 span1" id="payment-2" style="width : 80px;font-size:13px !important;" required>
                </div>     
              </div> 

              <div class="control-group">
                  <label class="control-label size-16 span2" for="users">Status</label>
                  <div class="controls">
                    <input type="radio" value="PAID" name="statusPaid" id="status" checked> PAID &nbsp;
                    <input type="radio" value="NULL" name="statusPaid" id="status"> UNPAID
                    
                  </div>
              </div>
              <div class="control-group">
                <select class="form-control" id="typePrint">
                  <option value="pdf">PDF</option>
                  <option value="xls">EXCEL</option>
                </select>
                <select class="form-control" id="typeReport">
                  <option value="RPT">REPORT PAYMENT REQUEST</option>
                  <option value="SRPT">SUMMARY REPORT PAYMENT REQUEST</option>
                </select>
              </div>

              <a href="#!" id="btn-print" class="btn btn-primary" style="float:left;display: none;"></i> PRINT</a> 
              <a href="#!" id="btn-print-summary" class="btn btn-primary" style="float:left;display: none;"></i> PRINT</a>
            </fieldset>
          </div>RPT01
        </div>   
      </div> 
    </div>
  </div>
</div>

<?php include "page/lookup/branchAddAll.php"; ?>
<?php include "page/lookup/lookup_intermediary_finance.php"; ?>
<?php include "page/lookup/lookup_sob_finance.php"; ?>
<?php include "page/lookup/lookup_currency_finance.php"; ?>

<script type="text/javascript">
  $(document).ready(function(){
    
    $('#typeReport').click(function(){
      if ($('#typeReport').val() == 'RPT') {
        $('#btn-print-summary').hide('fast');
        $('#btn-print').show('fast');
      }else if ($('#typeReport')) {
        $('#btn-print').hide('fast');
        $('#btn-print-summary').show('fast');
      }
    });

    $('#btn-print').click(function(){
      var datefrom = document.getElementById("period-1").value;
      var dateto = document.getElementById("period-2").value;

      var datePaymentFrom = document.getElementById("payment-1").value;
      var datePaymentto = document.getElementById("payment-2").value;

      var branch_code = document.getElementById("ibranchcode").value;
      var Intermediary = document.getElementById("iNamaIntermediary").value;
      // var SOB = document.getElementById("iCodeSob").value;
      var CCY = document.getElementById("icodeCurrency").value;
      var status = $('input[name="statusPaid"]:checked').val();

      
      if(datefrom == '')
      {
        swal('Periode From is Empty', '', 'error');
        return false;
      }
      else if(dateto == '')
      {
        swal('Periode To is Empty', '', 'error');
        return false;
      }
      else 
      if (datefrom != '' && dateto != '')
      {
        if(datefrom > dateto)
        {
          swal('Periode From must be equal to or greater than Periode To', '', 'error');
          return false;
        }
      }

      if ($('input[name="statusPaid"]:checked').val() == '') {
        swal('Status is Empty', '', 'error');
          return false;
      }

     
      var link = "report/report_installment.php?bc="+ branch_code +"&intermediary="+ Intermediary +"&ccy="+ CCY +"&datefrom="+ datefrom +"&dateto=" + dateto + "&datePayfrom="+datePaymentFrom+"&datePayto="+datePaymentto+"&status="+status;
      window.location.assign(link);
    });

    $('#btn-print-summary').click(function(){
      var datefrom = document.getElementById("period-1").value;
      var dateto = document.getElementById("period-2").value;
      var paymentfrom = document.getElementById("payment-1").value;
      var paymentto = document.getElementById("payment-2").value;

      var branch_code = document.getElementById("ibranchcode").value;
      var Intermediary = document.getElementById("iNamaIntermediary").value;
      // var SOB = document.getElementById("iCodeSob").value;
      var CCY = document.getElementById("icodeCurrency").value;
      var status = $('input[name="statusPaid"]:checked').val();
      var typePrint = document.getElementById("typePrint").value;
      
      if(datefrom == '')
      {
        swal('Periode From is Empty', '', 'error');
        return false;
      }
      else if(dateto == '')
      {
        swal('Periode To is Empty', '', 'error');
        return false;
      }
      else 
      if (datefrom != '' && dateto != '')
      {
        if(datefrom > dateto)
        {
          swal('Periode From must be equal to or greater than Periode To', '', 'error');
          return false;
        }
      }

      if ($('input[name="statusPaid"]:checked').val() == '') {
        swal('Status is Empty', '', 'error');
          return false;
      }
      // ========================================= START DATE =====================================
      startDT = datefrom.split("-");
      var tempStart = new Date(startDT[2] , startDT[1], startDT[0]);
      var day = tempStart.getDate();
      var month = tempStart.getMonth() ;
      month = month + "";

      if (month.length == 1)
      {
          month = "0" + month;
      }

      day = day + "";

      if (day.length == 1)
      {
          day = "0" + day;
      }
      var startDATE = tempStart.getFullYear() + '-' + month + '-' + day;
      // ========================================= END DATE ========================================
      endDT = dateto.split("-");
      var tempEnd = new Date(endDT[2] , endDT[1], endDT[0]);
      var dayto = tempEnd.getDate();
      var monthto = tempEnd.getMonth() ;
      monthto = monthto + "";

      if (monthto.length == 1)
      {
          monthto = "0" + monthto;
      }

      dayto = dayto + "";

      if (dayto.length == 1)
      {
          dayto = "0" + dayto;
      }
      var endDATE = tempEnd.getFullYear() + '-' + monthto + '-' + dayto;
      // ========================================== start payment date ==================================
      paymentDT = paymentfrom.split("-");
      var tempPay = new Date(paymentDT[2] , paymentDT[1], paymentDT[0]);
      var dayPay = tempPay.getDate();
      var monthPay = tempPay.getMonth() ;
      monthPay = monthPay + "";

      if (monthPay.length == 1)
      {
          monthPay = "0" + monthPay;
      }

      dayPay = dayPay + "";

      if (dayPay.length == 1)
      {
          dayPay = "0" + dayPay;
      }
      var PaymentDATE = tempPay.getFullYear() + '-' + monthPay + '-' + dayPay;
      // ============================================= END payment Date ====================================
      endPaymentDT = paymentto.split("-");
      var tempEndPayment = new Date(endPaymentDT[2] , endPaymentDT[1], endPaymentDT[0]);
      var daytoPay = tempEndPayment.getDate();
      var monthtoPay = tempEndPayment.getMonth() ;
      monthtoPay = monthtoPay + "";

      if (monthtoPay.length == 1)
      {
          monthtoPay = "0" + monthtoPay;
      }

      daytoPay = daytoPay + "";

      if (daytoPay.length == 1)
      {
          daytoPay = "0" + daytoPay;
      }
      var endPaymentDATE = tempEndPayment.getFullYear() + '-' + monthtoPay + '-' + daytoPay;
      
      if (branch_code != '') {
        branch_code = "&&pBranch="+branch_code;
      }
      if (Intermediary != '') {
        Intermediary = "&&pIntermediary="+Intermediary;
      }
      if (CCY != '') {
        CCY = "&&pCurrency="+CCY;
      }


      var link = "http://admappl2:7001/report/m/f/a/i/q/"+typePrint+"?pStart="+startDATE+"&&pEnd="+endDATE+"&&pStatus="+status+branch_code+Intermediary+CCY;
      window.location.assign(link);
    });
  });
</script>