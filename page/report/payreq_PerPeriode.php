<div class="container">
  <div class="widget-content">
    <div class="tab-content">
      <div class="tab-pane active" id="formcontrols">
        <div class="widget widget-table action-table">
          <div class="widget-content">
            <h3>REPORT PAYMENT REQUEST</h3>
            <fieldset class="span11">
            	<?php
                $query = "SELECT a.*, b.branch_name 
                        FROM security_user a
                        LEFT JOIN master_branch b ON b.branch_code=a.home_branch
                        LEFT JOIN mdepartment c ON c.ID=a.Dept_Id
                        WHERE a.username='".$_SESSION['username']."' 
                            
                        ";
				
			
				
				
                $data = mysqli_query($conn, $query);
                $dataRow = $data->num_rows;
                if($dataRow > 0)
                {
                  while ($row = mysqli_fetch_array($data)) {
              ?>
                <div class="control-group">
                  <label class="control-label size-16 span2" for="users">Branch</label>
                  <div class="controls">
                    <input type="text" value="<?php echo $row['home_branch']; ?>" name="nbranchcode" class="span1" id="ibranchcode" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>
                    <a href="#" data-toggle="modal" data-target="#branch-lookup" ><i class="fa fa-search" aria-hidden="true"></i></a>
                    <input type="text" value="<?php echo $row['branch_name']; ?>" name="nbranchname" class="span2" id="ibranchname" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>
                    
                  </div>
                </div>
              <?php 
                  } 
                }
                else
                { 
              ?>
                <div class="control-group">
                  <label class="control-label size-16 span2" for="users">Branch</label>
                  <div class="controls">
                    <input type="text" value="<?php echo $_SESSION['branch_code'] ?>" name="nbranchcode" class="span1" id="ibranchcode" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>
                    <input type="text" value="<?php echo $_SESSION['branch_name'] ?>" name="nbranchname" class="span2" id="ibranchname" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>

                  </div>
                </div>
              <?php } ?>
            	<div class="control-group">                     
                <label class="control-label size-16 span2" for="period">Period</label>
                <div class="controls">
                  <input type="text" name="periode1" class=" date periode-1 span1" id="period-1" style="width : 80px;font-size:13px !important;" >
                  &nbsp;S / D&nbsp;
                  <input type="text" name="periode2" class=" date periode-2 span1" id="period-2" style="width : 80px;font-size:13px !important;" >
                </div> <!-- /controls -->       
              </div> <!-- /control-group -->

              <a href="#" id="btn-print" class="btn btn-primary" style="float:left;"></i> PRINT</a>
            </fieldset>
          </div>RPT01
        </div>   
      </div> 
    </div>
  </div>
</div>

<?php include "page/lookup/branchAddAll.php"; ?>

<script type="text/javascript">
  $(document).ready(function(){
    

    $('#btn-print').click(function(){
      var datefrom = document.getElementById("period-1").value;
      var dateto = document.getElementById("period-2").value;

      var branch_code = document.getElementById("ibranchcode").value;

      if(datefrom == '')
      {
        swal('Periode From is Empty', '', 'error');
        return false;
      }
      else if(dateto == '')
      {
        swal('Periode To is Empty', '', 'error');
        return false;
      }
      else if (datefrom != '' && dateto != '')
      {
        if(datefrom > dateto)
        {
          swal('Periode From must be equal to or greater than Periode To', '', 'error');
          return false;
        }
      }

      if(branch_code == 'ALL')
      {
       branch_code = ''; 
      }

      var link = "report/report_payreq_periode.php?bc="+ branch_code +"&datefrom="+ datefrom +"&dateto=" + dateto;
      window.location.assign(link);
    });


  });
</script>