<p>Kepada Yth,</p>
<p>Bagian Branch Co / Bagian Finance</p>
<br>
<p>Mohon diberikan persetujuan untuk pembayaran sebagai berikut :</p>
<br>

<table class="form">
	<tr>
		<td>PAY TO / CLIENT NO</td>
		<td>:</td>
		<td>
			<?php if(!isset($dataHeader['BeneficiaryName']) || strlen($dataHeader['BeneficiaryName']) != 0) {?>	
				
			<input type="text" class="span2" name="iname" id="iname" value="<?php echo $dataHeader['BeneficiaryName'] ?>"> / 
			<input type="text" class="span1" name="pntc" id="pntc" value="<?php echo $dataHeader['PayTo'] ?>">
			
			<?php }else{?>	
			
			<input type="text" class="span2" name="iname" id="iname" value="<?php echo $dataHeaderBank['BANK_ACCOUNT_NAME'] ?>"> / 
			<input type="text" class="span1" name="pntc" id="pntc" value="<?php echo $dataHeaderBank['COMPANY_CODE'] ?>">
			
			<?php } ?>	
				
			
		</td>
	</tr>
	<tr>
		<td>BENEFICIARY NAME</td>
		<td>:</td>
		<td>
			<?php if(!isset($dataHeader['BeneficiaryName']) || strlen($dataHeader['BeneficiaryName']) != 0) {?>	
			
		<input type="text" class="span2" name="bn" id="bn" value="<?php echo $dataHeader['BeneficiaryName'] ?>">
		
		<?php }else{?>	
		<input type="text" class="span2" name="bn" id="bn" value="<?php echo $dataHeaderBank['BANK_ACCOUNT_NAME'] ?>">
			
		<?php } ?>	
		</td>
	</tr>
	<tr>
		<td>BANK</td>
		<td>:</td>
		<td>
			<?php if(!isset($dataHeader['BeneficiaryName']) || strlen($dataHeader['BeneficiaryName']) != 0) {?>	
		
		<input type="text" class="span2" name="bank" id="bank" value="<?php echo $dataHeader['Bank'] ?>">
		
		<?php }else{?>	
		
		<input type="text" class="span2" name="bank" id="bank" value="<?php echo $dataHeaderBank['BANK_NAME'] ?>">
		
		<?php } ?>
		
		</td>
	
	</tr>
	<tr>
		<td>DESTINATION ACCOUNT NUMBER</td>
		<td>:</td>
		<td>
			<?php if(!isset($dataHeader['BeneficiaryName']) || strlen($dataHeader['BeneficiaryName']) != 0) {?>	
		<input type="text" class="span2" name="an" id="an" value="<?php echo $dataHeader['DestinationAccountNumber'] ?>">
		<?php }else{?>	
		<input type="text" class="span2" name="an" id="an" value="<?php echo $dataHeaderBank['BANK_ACCOUNT_NO'] ?>">
		<?php } ?>
		</td>
	</tr>
	<tr>
		<td>BRANCH </td>
		<td>:</td>
		<td>
			<?php if(!isset($dataHeader['BeneficiaryName']) || strlen($dataHeader['BeneficiaryName']) != 0) {?>	
		<input type="text" class="span2" name="branch" value="<?php echo $dataHeader['Branch'] ?>">
		<?php }else{?>	
		<input type="text" class="span2" name="branch" value="<?php echo $dataHeaderBank['BRANCH_NAME'] ?>">		
		<?php } ?>
		</td>
	
	
	</tr>
	<tr>
		<td>CITY</td>
		<td>:</td>
		<td>
		<?php if(!isset($dataHeader['BeneficiaryName']) || strlen($dataHeader['BeneficiaryName']) != 0) {?>	
		<input type="text" class="span2" name="cc" value="<?php echo $dataHeader['City'] ?>">
		<?php }else{?>			
		<input type="text" class="span2" name="cc" value="<?php echo $dataHeaderBank['BANK_CITY'] ?>">
		<?php } ?>
		</td>
	</tr>
	<tr>
		<td>PERSENTASE FEE</td>
		<td>:</td>
		<td>
		
		<input type="text" class="span1" id="rate" name="rate" value="<?php echo $dataHeader['Rate'] ?>">
	
		</td>
	</tr>
	<tr>
		<td>TYPE OF PAYMENT</td>
		<td>:</td>
		<td>
			<input type="text" name="top" class="span3" id="top" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" value="<?php echo $dataHeader['TypeOfPayment'] ?>" readonly>
				<a href="#" data-toggle="modal" data-target="#typeofpayment" ><i class="fa fa-search" aria-hidden="true"></i></a>
		</td>
	</tr>
	<tr>
		<td>Memo</td>
		<td>:</td>
		<td>
			<textarea id="imemo" name="nmemo" value="<?php echo $dataHeader['Memo'] ?>" class="span4" style="margin: 0px; width: 360px; height: 131px;" ></textarea>
		</td>	
	</tr>
</table>