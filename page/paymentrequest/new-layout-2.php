
<fieldset class="span5">
      <div class="control-group">                     
        <label class="control-label size-16" for="mop">Methods Of Payment</label>
        <div class="controls">
          <input type="text" name="mop" class="span2"  id="mop" style="font-size:13px !important;" readonly>
          <span id="loading"></span>
        </div> <!-- /controls -->       
      </div> <!-- /control-group -->
      <div class="control-group">                     
        <label class="control-label size-16" for="top">Type Of Payment</label>
        <div class="controls">
          <input type="text" name="top" class="span3" id="top" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>
          <a href="#" data-toggle="modal" data-target="#typeofpayment" ><i class="fa fa-search" aria-hidden="true"></i></a>
        </div> <!-- /controls -->       
      </div>
      <div class="control-group">                     
        <label class="control-label size-16" for="ccy">CCY</label>
        <div class="controls">
          <input type="text" name="ccy" class="span1 ccy"  id="ccy" style="font-size:13px !important;" onkeyup="this.value = this.value.toUpperCase()" readonly>
          <a href="#" id="tccy" data-toggle="modal" data-target="#currency"><i class="fa fa-search" aria-hidden="true"></i></a>
        </div> <!-- /controls -->       
      </div> <!-- /control-group -->
      
      <div class="control-group">                     
        <label class="control-label size-16" for="ptc">Pay To / Client No</label>
        <div class="controls">
          <input type="text" name="ptcn" class="span2" id="ptc" style="font-size:13px !important;" readonly>
          <a href="#" class="pay" data-toggle="modal" data-target="#payto" ><i class="fa fa-search" aria-hidden="true"></i></a>
        </div> <!-- /controls -->       
      </div> <!-- /control-group -->

      <hr>
      
      <div class="control-group">                     
        <label class="control-label size-16" for="bename">Beneficiary Name</label>
        <div class="controls">
          <input type="text" name="bn" class="span3" id="bename" style="font-size:13px !important;" readonly>
        </div> <!-- /controls -->       
      </div> <!-- /control-group -->

      <div class="control-group">                     
        <label class="control-label size-16" for="bank">Bank</label>
        <div class="controls">
          <input type="text" name="bank" class="span3" id="bank" style="font-size:13px !important;" readonly>
        </div> <!-- /controls -->       
      </div> <!-- /control-group -->

      <div class="control-group">                     
        <label class="control-label size-16" for="account">Account Number</label>
        <div class="controls">
          <input type="text" name="an" class="span3" id="account" style="font-size:13px !important;" readonly>
        </div> <!-- /controls -->       
      </div> <!-- /control-group -->

      <div class="control-group">                     
        <label class="control-label size-16" for="branch">Branch</label>
        <div class="controls">
          <input type="text" name="branch" class="span3" id="branch" style="font-size:13px !important;" readonly>
        </div> <!-- /controls -->       
      </div> <!-- /control-group -->

      <div class="control-group">                     
        <label class="control-label size-16" for="cc">City / Country</label>
        <div class="controls">
          <input type="text" name="cc" class="span3" id="cc" style="font-size:13px !important;" readonly>
        </div> <!-- /controls -->       
      </div> <!-- /control-group -->
      
</fieldset>