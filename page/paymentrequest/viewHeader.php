<p>Kepada Yth,</p>
<p>Bagian Branch Co / Bagian Finance</p>
<br>
<p>Mohon diberikan persetujuan untuk pembayaran sebagai berikut :</p>
<br>
<table class="form">
	<tr>
		<td>PAY TO / CLIENT NO</td>
		<td>:</td>
		<td> <?php echo $dataHeader['BeneficiaryName'] ?> / <?php echo $dataHeader['PayTo'] ?></td>
	</tr>
	<tr>
		<td>BENEFICIARY NAME</td>
		<td>:</td>
		<td><?php echo $dataHeader['BeneficiaryName'] ?></td>
	</tr>
	<tr>
		<td>BANK</td>
		<td>:</td>
		<td><?php echo $dataHeader['Bank'] ?></td>
	</tr>
	<tr>
		<td>DESTINATION ACCOUNT NUMBER</td>
		<td>:</td>
		<td><?php echo $dataHeader['DestinationAccountNumber'] ?></td>
	</tr>
	<tr>
		<td>BRANCH </td>
		<td>:</td>
		<td><?php echo $dataHeader['Branch'] ?></td>
	</tr>
	<tr>
		<td>CITY</td>
		<td>:</td>
		<td><?php echo $dataHeader['City'] ?></td>
	</tr>
	<tr>
		<td>PERSENTASE FEE</td>
		<td>:</td>
		<td><?php echo $dataHeader['Rate'] ?></td>
	</tr>
	<tr>
		<td>TYPE OF PAYMENT</td>
		<td>:</td>
		<td><?php echo $dataHeader['TypeOfPayment'] ?></td>
	</tr>
	<tr>
		<td style="vertical-align: top">Memo</td>
		<td style="vertical-align: top">:</td>
		<td width="800px"><?php echo $dataHeader['Memo'] ?></td>
	</tr>
</table>