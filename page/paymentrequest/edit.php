<?php 
	if (isset($_GET['id']) && !empty($_GET['id'])) {
		$id 	=	$_GET['id'];
		$_SESSION['REQ_ID'] = $id;
		include "page/lookup/approval_history.php";
		include "page/lookup/typeofpayment.php";
  	    	
		$queryHeader    = "SELECT * FROM tpaymentrequestheader WHERE ISACTIVE=1 AND ID = '$id' ";
  		$data   = mysqli_query($conn, $queryHeader) or die(mysqli_error($conn));
  		$dataHeader = mysqli_fetch_array($data);
		$dataHeaderBank = null;

		$createWhoData	 =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$dataHeader[CREATED_BY]'");
		$checkerWhoData	 =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$dataHeader[CHECKER_BY]'");
		$approvalWhoData =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$dataHeader[APPROVAL_BY]'");
		
		$createWho = mysqli_fetch_array($createWhoData);
		$checkerWho = mysqli_fetch_array($checkerWhoData);
		$approvalWho = mysqli_fetch_array($approvalWhoData);	
		
		if(!isset($dataHeader['BeneficiaryName']) || strlen($dataHeader['BeneficiaryName']) == 0 ) {
			$queryHeader1    = "select * from master_company_bank_accounts where company_code = '".$dataHeader['IntermediaryCode']."' and default_others_currency_flag = 'Y' ";
			$data1   = mysqli_query($conn, $queryHeader1) or die(mysqli_error($conn));
			$dataHeaderBank = mysqli_fetch_array($data1);
			
		//echo "TEST".$dataHeaderBank["BANK_ACCOUNT_NAME"];
		}

		
?>
<!-- For Maker Edit Form, Revise -->
<form action="fungsi/paymentrequest/save.php" method="post">
<div class="print" style="font-size : 11px;">
	<div class="container">
		<div class="head">
			<h3>PAYMENT REQUEST</h3>
			<p> <?php echo $dataHeader['PaymentRequestNo']; ?></p>
			<input type="hidden" name="id"  value="<?php echo $dataHeader['ID']; ?>">
			<input type="hidden" name="PaymentNo" id="PaymentRequestNoID" value="<?php echo $dataHeader['PaymentRequestNo']; ?>">
		</div>
		<div class="isi">
			<?php include "page/paymentrequest/editHeader.php"; ?>
			<br>
			<a href="#!" class="btn btn-primary" id='polis-add-retrive' data-toggle='modal' data-target='#lookup-add-polis'>Add Policy</a>
			<a href="#!" class="btn btn-warning" id="reset-add-retrive" onclick="reloadHalaman()">Reset</a>
			<br>
			<?php include "page/paymentrequest/editContentPerCurr.php"; ?>
			<br>
			<h2 id="label">New Policy</h2>
			<br>
			<?php include "page/paymentrequest/tabelAddPolis.php"; ?>
			
		</div>
		<br>
		<?php include "page/paymentrequest/viewNoteDetail.php"; ?>
		<br>
		<?php include "page/paymentrequest/viewFooter.php"; ?>
	</div>
	<br>
	<br>
	<div class="bottom">
		<a href="#" onclick="javascript:history.back()" class="btn btn-danger">EXIT</a>
		<a href="#" id="approvalhistory" data-toggle="modal" data-target="#lookup-ApprovalHistory" class="btn btn-primary">View History</a>
		<input type="submit" name="re_submit" style="float:right;" class="btn btn-primary btn-save-3" value="SAVE">
	</div>PR04
</div>
</form>

<?php 
	include "page/lookup/AddPolisRetrive.php";
?>
<script type="text/javascript">
function reloadHalaman() {
	$('#reset-add-retrive').css({'display':'none'});
    location.reload();
}
$(document).ready(function(){
		

	$("#pilihsemua").click(function () { 
	
	  $('.pilih').attr('checked', this.checked);
	});
	 
	$(".pilih").click(function(){
		
	  if($(".pilih").length == $(".pilih:checked").length) {
		
		  
		  
	    $("#pilihsemua").attr("checked", "checked");

	  } else {
	    $("#pilihsemua").removeAttr("checked");
	    
	  }
	});
});
</script>

<?php 
}else{
	echo "eror-data";
}
 ?>