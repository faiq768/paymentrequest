<div class="container">
  <div class="widget-content">
    <div class="tab-content">
      <div class="tab-pane active" id="formcontrols">
        <div class="widget widget-table action-table">
          <div class="widget-content">
            <h3>PAYMENT REQUEST LIST</h3>
            <table id="query-table" class="display cell-border" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>NO</th>
                  <th>PAYMENT REQUEST NUMBER </th>
                  <th>INTERMEDIARY NAME </th>
                  <th>BANK</th>
                  <th>REQUEST DATE</th>
                  <th>REQUEST STATUS</th>
                  <th>PAID STATUS</th>
                  <th>VOUCHER NO</th>
                  <th>DATE</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php
					$no    = 1;
					$branchCodeArray = explode(",", $_SESSION['branch_code']);
					
					//for($i=0; $i < count($branchCodeArray); $i++){
						
						
				
                  $query = "SELECT * FROM tpaymentrequestheader where CREATED_BY = '".$_SESSION['username']."' ORDER BY CREATED_DATE DESC ";
                  $Data  = mysqli_query($conn, $query); 
                  
                  while ($queryRow =   mysqli_fetch_array($Data)) {
                ?>
                <tr align="center">
                  <th> <?php echo $no; ?> </th>
                  <td > <?php echo $queryRow['PaymentRequestNo'] ?> </td>
                  <td> <?php echo $queryRow['BeneficiaryName'] ?> </td>
                  <td> <?php echo $queryRow['Bank'] ?> </td>
                  <td> <?php echo $queryRow['CREATED_DATE'] ?> </td>
                  <td> <?php echo $queryRow['STATUS_PR']; ?> </td>
                  <td><?php echo $queryRow['STATUS_PAID']; ?></td>
                  <td><?php echo $queryRow['VoucherNo']; ?></td>
                  <td><?php echo $queryRow['VoucherDate']; ?></td>
                  <td><a href="home?page=payreqlistdetail&id=<?php echo $queryRow['ID'] ?>&status=<?php echo $queryRow['STATUS_PR'] ?>" class="btn btn-primary">Detail</a></td>
                </tr>
                <?php $no++;}
					//}
				?>
              </tbody>
            </table>
          </div>PR02
        </div>   
      </div> 
    </div>
  </div>
</div>
<?php  include "page/lookup/branch.php"; ?>

<script type="text/javascript">
  $(function () {
        $("#query-table").DataTable({
          "paging":   true,
          "ordering": true,
          "scrollY": '45vh',
          "scrollX": true,
          "language": {
            "lengthMenu": "",
            "zeroRecords": "Nothing found - sorry",
            "info": "Showing page _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }
        });
    });
</script>