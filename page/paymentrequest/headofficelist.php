<div class="container">
  <div class="widget-content">
    <div class="tab-content">
      <div class="tab-pane active" id="formcontrols">
        <div class="widget widget-table action-table">
          <div class="widget-content">
            <h3>PAYMENT REQUEST LIST</h3>
            <table id="query-table" class="display cell-border" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>NO</th>
                  <th>PAYMENT REQUEST NUMBER </th>
                  <th>INTERMEDIARY NAME </th>
                  <th>BANK</th>
                  <th>REQUEST DATE</th>
                  <th>REQUEST STATUS</th>
                  <th>PAID STATUS</th>
				  <th>NO VOUCHER</th>
				  <th>PAID DATE</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php
                  $query = "SELECT * FROM tpaymentrequestheader WHERE status_paid = 'PAID' and ISACTIVE=1 ORDER BY CREATED_DATE DESC ";
                  $Data  = mysqli_query($conn, $query); 
                  $no    = 1;
                  while ($queryRow =   mysqli_fetch_array($Data)) {
                ?>
                <tr align="center">
                  <th> <?php echo $no; ?> </th>
                  <td > <?php echo $queryRow['PaymentRequestNo'] ?> </td>
                  <td> <?php echo $queryRow['BeneficiaryName'] ?> </td>
                  <td> <?php echo $queryRow['Bank'] ?> </td>
                  <td> <?php echo $queryRow['CREATED_DATE'] ?> </td>
                  <td> <?php echo $queryRow['STATUS_PR']; ?> </td>
                  <td><?php echo $queryRow['STATUS_PAID']; ?></td>
				          <td><?php echo $queryRow['VoucherNo']; ?></td>
                  <td><?php echo date('d-m-Y', strtotime($queryRow['PAY_PAID_DATE'])); ?></td>
                  <td><a href="home?page=payreqhodetail&id=<?php echo $queryRow['ID'] ?>" class="btn btn-primary">Detail</a><br><br>
                    <a href="#corection-lookup" class="btn btn-warning koreksi" id="koreksi" data-id="<?php echo $queryRow['ID'] ?>" data-toggle='modal'>Edit</a>
                  </td>
                </tr>
                
                <?php $no++;} ?>
              </tbody>
            </table>
          </div>PR06
        </div>   
      </div> 
    </div>
  </div>
</div>
<?php  include "page/lookup/corection_CDV.php"; ?>
<?php  include "page/lookup/branch.php"; ?>


<script type="text/javascript">
  $(function () {
        $("#query-table").DataTable({
          "paging":   false,
          "ordering": true,
          "scrollY": '45vh',
          "scrollX": true,
          "language": {
            "lengthMenu": "Display records per page",
            "zeroRecords": "Nothing found - sorry",
            "info": "Showing page _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }
        });
        // var dataTable =  $('#query-table').DataTable( {
        // serverSide: true,
        // ajax:{
        //         url :"ajax/ajax.serverside.headofficelist.php", // json datasource
        //         type: "post",  // method  , by default get
        //         error: function(){  // error handling
        //             $(".employee-grid-error").html("");
        //             $("#query-table").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
        //             $("#employee-grid_processing").css("display","none");
        //         }
        //     },
        // dom: "frtiS",
        // scrollY: 200,
        // deferRender: true,
        // scroller: {
        //     loadingIndicator: true
        // }
        // } );

        });
</script>