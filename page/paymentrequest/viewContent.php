<table class="tabel" border="1">
	<thead>
		<tr>
			<th rowspan="2">NO</th>
			<th rowspan="2">POLIS NUMBER</th>
			<th rowspan="2">CORR</th>
			<th rowspan="2">INSURED NAME</th>
			<th colspan="6">SETTLEMENT REFFERENCE</th>
			<th rowspan="2">CCY</th>
			<th rowspan="2">GROSS PREMIUM</th>
			<th rowspan="2">AMOUNT</th>
			<!-- <th rowspan="2">TYPE OF PAYMENT</th> -->
		</tr>
		<tr>
			<th>NOTE NO</th>
			<th>STATUS</th>
			<th style="">DATE</th>
			<th style="">BANK</th>
			<th style="">ACCOUNT</th>
			<th style="">VOUCHER</th>
		</tr>

	</thead>
	<tbody>
		<?php
			$printQuery2	=	mysqli_query($conn, "SELECT * FROM tpaymentrequestdetail where PaymentRequestID = '$id' "); 
		 	$totalQuery		=	mysqli_query($conn, "SELECT SUM(Amount) AS total from tpaymentrequestdetail where PaymentRequestID = '$id' ");
		 	$ccy = mysqli_query($conn, "SELECT DISTINcT CCY FROM tpaymentrequestdetail where PaymentRequestID = '$id' ");
		 	$totalrow		=	mysqli_fetch_array($totalQuery);
		 	$number 		=	1;
		 	while ($dataPrint2 	=	mysqli_fetch_array($printQuery2)) {
		 		
		 		$length	=	strlen($dataPrint2['InsuredName']);
		 		if ($length > 35) {
		 			$insured 	=	substr($dataPrint2['InsuredName'], 0,35).'...';
		 		}else{
		 			$insured 	=	$dataPrint2['InsuredName'];
		 		}
		 		?>
		<tr>
			<td width="25px"; align="center"><?php echo $number; ?></td>
			<td width="200px"; ><?php echo $dataPrint2['PolisNo']; ?></td>
			<td width="50px"; ><?php echo $dataPrint2['Corr']; ?></td>
			<td width="300px"; ><?php echo $insured; ?></td>
			<td width="200px"; ><?php echo $dataPrint2['NotaNo']; ?></td>
			<td width="50px"; align="center"><?php echo $dataPrint2['StatusSettlement']; ?></td>
			<td width="60px"; align="center"><?php echo $dataPrint2['DateSettlement']; ?></td>
			<td width="50px"; align="center"><?php echo $dataPrint2['BankSettlement']; ?></td>
			<td width="50px"; align="center"><?php echo $dataPrint2['AccountSettlement']; ?></td>
			<td width="120px"; align="center"><?php echo $dataPrint2['VoucherSettlement']; ?></td>
			<td width="30px"; align="center"><?php echo $dataPrint2['CCY']; ?></td>
			<td width="100px"; align="right"><?php echo number_format( $dataPrint2['GrossPremium'] , 2 , '.' , ',' );?></td>
			<td width="100px"; align="right"><?php echo number_format( $dataPrint2['Amount'] , 2 , '.' , ',' ); ?></td>
		</tr>
		<?php $number++;} 
			while($fetchccy	=	mysqli_fetch_array($ccy)){
				$Amountccy	=	mysqli_query($conn, "SELECT SUM(Amount) AS total from tpaymentrequestdetail where PaymentRequestID = '$id' AND CCY = '".$fetchccy['CCY']."' ");
				$fetchamout =	mysqli_fetch_array($Amountccy);
		?>
		<tr class="garis">
			<td colspan="12" align="right" style="padding-right:10px;">TOTAL PAYMENT <?php echo $fetchccy['CCY'] ?></td>
			<td align="right"><?php echo number_format( $fetchamout['total'] , 2 , '.' , ',' ); ?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>