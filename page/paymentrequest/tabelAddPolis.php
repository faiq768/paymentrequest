<table class="tabel css-serial" border="1" id="EDIT_TABLE_ADD">
	<thead>
		<tr>
			<th rowspan="2"></th>
			<th rowspan="2">No</th>
			<th rowspan="2">POLIS NUMBER</th>
			<th rowspan="2">CORR</th>
			<th rowspan="2">INSURED NAME</th>
			<th colspan="6">SATTLEMENT REFFERENCE</th>
			<th rowspan="2">CCY</th>
			<th rowspan="2">GROSS PREMIUM</th>
			<th rowspan="2">AMOUNT</th>
		</tr>
		<tr>
			<th>NOTA NO</th>
			<th >STATUS</th>
			<th style="">DATE</th>
			<th style="">BANK</th>
			<th style="">ACCOUNT</th>
			<th style="">VOUCHER</th>
		</tr>

	</thead>
	<tbody>
	</tbody>
</table>

<script type="text/javascript">
$(document).ready(function(){
var abc = $('#PaymentRequestNoID').val();
    table = $('#dataPolicyAdd').DataTable( { 
           'processing': true,
           'serverSide': true,
           'retrieve': true,
           'destroy' : true,
           'ajax':  'ajax/ajax_add_polis_retrive.php?PaymentNo='+abc,
           'columnDefs': [
                 {
                    'targets': 0,
                    'checkboxes': {
                       'selectRow': true
                    }
                 }
              ],
              'select': {
                 'style': 'multi'
              },
              'order': [[1, 'asc']]
      });

// $('#polis-add-retrive').click(function(){

      $('#addToTable').on( 'click', function () {
        $('#EDIT_TABLE_ADD, #label').css({'display' : 'block'});
        $('#reset-add-retrive').css({'display' : 'inline-block'});
       // var daP = table.row( $(this).parents('tr') ).data();
          var form = this;
          var rows_selected = table.column(0).checkboxes.selected();
          // var rows_selected = daP[].checkboxes.selected();

          // Iterate over all selected checkboxes
          
          $.each(rows_selected, function(index, rowId){
             
             
            $.ajax({
                  type  :   'POST',
                  url   :   'page/paymentrequest/addPolisRetrive.php',
                  data  :   'Nama='+rowId,
                  dataType : 'json',
                  success : function(response){
                    
                    function tambah_baris()
                        {
                            html='<tr>'
                            + '<td><input type="checkbox" name="Tambahbaru[]" value="'+response.empat+'" checked> </td>'
                            + '<td width="25px" id="nomor" align="center"></td>'
                            + '<td width="200px">'+response.satu+'</td>'
                            + '<td width="50px">'+response.dua+'</td>'
                            + '<td width="280px">'+response.tiga+'</td>'
                            + '<td width="180px" align="center">'+response.empat+'</td>'
                            + '<td width="50px" align="center">'+response.lima+'</td>'
                            + '<td width="80px" align="center">'+response.enam+'</td>'
                            + '<td width="50px" align="center">'+response.tujuh+'</td>'
                            + '<td width="100px" align="center">'+response.delapan+'</td>'
                            + '<td width="120px" align="center">'+response.sembilan+'</td>'
                            + '<td width="30px" align="center">'+response.sepuluh+'</td>'
                            + '<td width="100px" align="right">'+response.sebelas+'</td>'
                            + '<td width="100px" align="right">'+response.sebelas*$('#rate').val()/100+'</td>'
                            + '</tr>';
                            $('#EDIT_TABLE_ADD tbody').append(html);
                        }
                    return tambah_baris();
                  }
                 
                });
          });
          
          $('#lookup-add-polis').modal('hide');

      } );
});

</script>