<?php 
  if (isset($_GET['id']) && !empty($_GET['id'])) {
  	$id 	=	$_GET['id'];
    $_SESSION['REQ_ID'] = $id;
    include "page/lookup/approval_history.php";
  
      	
    $queryHeader    = "SELECT * FROM tpaymentrequestheader WHERE ISACTIVE=1 AND ID = '$id' ";
    $data   = mysqli_query($conn, $queryHeader) or die(mysqli_error($conn));
    $dataHeader = mysqli_fetch_array($data);
	
		$createWhoData	 =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$dataHeader[CREATED_BY]'");
		$checkerWhoData	 =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$dataHeader[CHECKER_BY]'");
		$approvalWhoData =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$dataHeader[APPROVAL_BY]'");
		
		$createWho = mysqli_fetch_array($createWhoData);
		$checkerWho = mysqli_fetch_array($checkerWhoData);
		$approvalWho = mysqli_fetch_array($approvalWhoData);
	
	
?>

<div class="print" style="font-size : 11px;">
	<div class="container">
		<div class="head">
			<h3>PAYMENT REQUEST</h3>
			<p> <?php echo $dataHeader['PaymentRequestNo']; ?></p>
		</div>
		<div class="isi">
			<?php include "page/paymentrequest/viewHeader.php"; ?>
			<br>
			<?php include "page/paymentrequest/viewContentPerCurr.php"; ?>			
		</div>
		<br>
    <?php include "page/paymentrequest/viewNoteDetail.php"; ?>
		<br>
		<?php include "page/paymentrequest/viewFooter.php"; ?>
		<br>
    <?php 
      $username = $_SESSION['username'];
      $query      = "SELECT * FROM security_user 
                  WHERE ISACTIVE=1
                    AND username='$username' 
                    AND home_branch = (SELECT Value FROM mgeneral_table WHERE Code='BRANCH_HO')
                    AND Dept_id IN (SELECT Value FROM mgeneral_table WHERE Code='DEPARTMENT_FINANCE_PUSAT')";
      $data       = mysqli_query($conn, $query);
      $dataRow    = $data->num_rows;
      if($dataRow > 0 && $dataHeader['STATUS_PAID'] == '')
      {
    ?>
      <div align="left">
        <table border="0">
          <tr>
              <td>Voucher No</td>
              <td>
                <input type="text" maxlength="30" name="VoucherNo" class="span1" id="VoucherNo" style="width:100px; font-size:13px !important;">
              </td>
          </tr>
          <tr>
              <td>Date</td>
              <td>
                <input type="text" name="VoucherDate" class="date span1" id="VoucherDate" style="width:70px;font-size:13px !important;" >
              </td>
          </tr>
        </table>
      </div>
    <?php } ?>
		<br>
    <?php 
      include "page/paymentrequest/approvalbtn.php"; 
    ?>
	</div>PR08
</div>

<script type="text/javascript">
$(document).ready(function(){
  $('#btn-approve-checker').click(function(){
    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons:{
      	cancel: true,
	    confirm: "APPROVE",
  		},
    })
    .then((willDelete) => {
      if (willDelete) {
        window.location.assign("fungsi/paymentrequest/approvalaction.php?function=CHECKER&id=<?php echo $id; ?>");
      } else {
        return false;
      }
    });
  });

$('#btn-approve-approve').click(function(){
    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.location.assign("fungsi/paymentrequest/approvalaction.php?function=APPROVE&id=<?php echo $id; ?>");
      } else {
        return false;
      }
    });
  });
$('#btn-reject-approve').click(function(){
    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
       swal("Write remarks something here:", {
				  content: "input",
				})
				.then((value) => {
				  var comment = `${value}`;
          if(comment.length > 0)
          {
				    window.location.assign("fungsi/paymentrequest/approvalaction.php?function=REJECTAPPROVE&id=<?php echo $id;?>&remarks="+comment+" ");
          }
          else
          {
            swal('Remarks must fill', '', 'error');
          }
				});
      } else {
        return false;
      }
    });
  });
$('#btn-revise-approve').click(function(){
    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        swal("Write remarks something here:", {
          content: "input",
        })
        .then((value) => {
          var comment = `${value}`;
          if(comment.length > 0)
          {
            window.location.assign("fungsi/paymentrequest/approvalaction.php?function=REVISEAPPROVE&id=<?php echo $id; ?>&remarks="+comment+"");
          }
          else
          {
            swal('Remarks must fill', '', 'error');
          }
        });

        
      } else {
        return false;
      }
    });
  });
$('#btn-reject-checker').click(function(){
    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
       swal("Write remarks something here:", {
          content: "input",
        })
        .then((value) => {
          var comment = `${value}`;
          if(comment.length > 0)
          {
            window.location.assign("fungsi/paymentrequest/approvalaction.php?function=REJECTCHECKER&id=<?php echo $id;?>&remarks="+comment+" ");
          }
          else
          {
            swal('Remarks must fill', '', 'error');
          }
        });
      } else {
        return false;
      }
    });
  });
$('#btn-revise-checker').click(function(){
    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        swal("Write remarks something here:", {
          content: "input",
        })
        .then((value) => {
          var comment = `${value}`;
          if(comment.length > 0)
          {
            window.location.assign("fungsi/paymentrequest/approvalaction.php?function=REVISECHECKER&id=<?php echo $id; ?>&remarks="+comment+"");
          }
          else
          {
            swal('Remarks must fill', '', 'error');
          }
        });
        
      } else {
        return false;
      }
    });
  });
$('#btn-paid').click(function(){
	alert('test');
	
    var VoucherNo = document.getElementById("VoucherNo").value;
		alert('voucher no ');
	var VoucherDate = document.getElementById("VoucherDate").value;
	alert('ttt');
    if(VoucherNo == '')
    {
      swal('Voucher No is Empty', '', 'error');
      return false;
    }
    else if(VoucherDate == '')
    {
      swal('Date is Empty', '', 'error');
      return false; 
    } 
    else
    {
      swal({
        title: "Are you sure?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          window.location.assign("fungsi/paymentrequest/financeAction.php?VoucherNo="+VoucherNo+"&VoucherDate="+VoucherDate+"&id=<?php echo $id; ?>");
        } else {
          return false;
        }
      });
    }
  });

});
</script>

<?php 
}else{
	echo "eror";
}
 ?>