<?php 
	$queryCCY = "SELECT DISTINcT CCY FROM tpaymentrequestdetail where PaymentRequestID = '$id' ";
	$ccy = mysqli_query($conn, $queryCCY);
	while($CurrTable	=	mysqli_fetch_array($ccy)){
		$CurrTable['CCY'];
		$queryCCYAmount = "SELECT SUM(Amount) AS total from tpaymentrequestdetail where PaymentRequestID = '$id' AND CCY = '".$CurrTable['CCY']."' ";
		$dataCCYAmount	=	mysqli_query($conn, $queryCCYAmount);
		$fetchCCYAmout 	=	mysqli_fetch_array($dataCCYAmount);
		
		$queryCCYAmount2 = "SELECT SUM(GrossPremium) AS total from tpaymentrequestdetail where PaymentRequestID = '$id' AND CCY = '".$CurrTable['CCY']."' ";
		$dataCCYAmount2	=	mysqli_query($conn, $queryCCYAmount2);
		$fetchCCYAmout2 	=	mysqli_fetch_array($dataCCYAmount2);
?>
<br>
<table border="1" width="1200px">
	<thead>
		<tr>
			<th rowspan="2">NO</th>
			<th rowspan="2" style="width: 150px;">POLIS NUMBER</th>
			<th rowspan="2" style="width: 30px;">CORR</th>
			<th rowspan="2" style="width: 120px;">INSURED NAME</th>
			<th colspan="6">SETTLEMENT REFFERENCE</th>
			<th rowspan="2" style="width: 20px;">CCY</th>
			<th rowspan="2" style="width: 80px;">GROSS PREMIUM</th>
			<th rowspan="2" style="width: 80px;">AMOUNT</th>
			<th rowspan="2" style="width: 80px;">DESC</th>
		</tr>
		<tr>
			<th style="width: 120px;">NOTE NO</th>
			<th style="width: 30px;">STATUS</th>
			<th style="width: 70px;">DATE</th>
			<th style="width: 100px;">BANK</th>
			<th style="width: 100px;">ACCOUNT</th>
			<th style="width: 100px;">VOUCHER</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$queryPayReqDetailPerCurr 	= "SELECT a.*,(CASE WHEN DATEDIFF(b.PAID_DATE, b.DUE_DATE) > 0 THEN 'OVERDUE' ELSE 'DUE' END) AS GROUP_TYPE FROM tpaymentrequestdetail a JOIN stg_pmt_req_datamart b ON b.NOTE_NO = a.NotaNo where PaymentRequestID = '$id' AND CCY = '".$CurrTable['CCY']."' ";
			$dataPayReqDetailPerCurr	=	mysqli_query($conn, $queryPayReqDetailPerCurr); 
		 	
		 	// $totalQuery		=	mysqli_query($conn, "SELECT SUM(Amount) AS total from tpaymentrequestdetail where PaymentRequestID = '$id' AND CCY = '".$CurrTable['CCY']."' ");
		 	// $totalrow		=	mysqli_fetch_array($totalQuery);
		 	$number 		=	1;
		 	while ($PayReqDetailPerCurr 	=	mysqli_fetch_array($dataPayReqDetailPerCurr)) 
		 	{
		?>
		<tr>
			<td width="25px" align="center"><?php echo $number; ?></td>
			<td><?php echo $PayReqDetailPerCurr['PolisNo']; ?></td>
			<td align="center"><?php echo $PayReqDetailPerCurr['Corr']; ?></td>
			<td ><?php echo $PayReqDetailPerCurr['InsuredName']; ?></td>
			<td><?php echo $PayReqDetailPerCurr['NotaNo']; ?></td>
			<td align="center"><?php echo $PayReqDetailPerCurr['StatusSettlement']; ?></td>
			<td align="left"><?php echo $PayReqDetailPerCurr['DateSettlement']; ?></td>
			<td align="left"><?php echo $PayReqDetailPerCurr['BankSettlement']; ?></td>
			<td align="left"><?php echo $PayReqDetailPerCurr['AccountSettlement']; ?></td>
			<td align="center"><?php echo $PayReqDetailPerCurr['VoucherSettlement']; ?></td>
			<td align="center"><?php echo $PayReqDetailPerCurr['CCY']; ?></td>
			<td align="right"><?php echo number_format( $PayReqDetailPerCurr['GrossPremium'] , 2 , '.' , ',' );?></td>
			<td align="right"><?php echo number_format( $PayReqDetailPerCurr['Amount'] , 2 , '.' , ',' ); ?></td>
			<td align="center"><?php echo $PayReqDetailPerCurr['GROUP_TYPE']; ?></td>
		</tr>
		<?php $number++;} ?>
		<tr class="garis">
			<td colspan="11" align="right" style="padding-right:10px;">TOTAL  <?php echo $CurrTable['CCY'] ?></td>
			<td align="right"><?php echo number_format( $fetchCCYAmout2['total'] , 2 , '.' , ',' ); ?></td>
			<td align="right"><?php echo number_format( $fetchCCYAmout['total'] , 2 , '.' , ',' ); ?></td>
		</tr>
	</tbody>
</table>
<?php } ?>