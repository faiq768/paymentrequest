<div class="previewClass">
  <div class="preview">
    
  </div>
  <div class="btn-preview" align="center">
    <a href="#!" id="close_priview" class="btn btn-primary">CLOSE</a>
  </div>
</div>
<div class='col-lg-12 main-form'>
  <form preview='fungsi/paymentrequest/preview.php' action='fungsi/paymentrequest/save.php' onsubmit='return validateForm()' method='post' id='myformData'> 
    <div class='span12 step-1 aktif' style='margin-left:0 !important;'>
      <div class='widget-content'>
        <h3>PAYMENT REQUEST</h3>
        <br>
        <div class='tab-content'>
          <div class='tab-pane pane-1 active' id='formcontrols'>
            <div id='edit-profile' class='form-horizontal'>
              <fieldset class='span11'>
                
				<div class='control-group'>                     
                  <label class='control-label size-16' for='pnt'>Policy / Note Type</label>
                  <div class='controls'>
                    <input type='hidden' name='noteno' id='noteno' >
                    <input type='text' name='pnt' class='span3' id='pnt' style='font-size:13px !important;' onkeyup='this.value = this.value.toUpperCase()' readonly>
                    <a href='#' id='polis' data-toggle='modal' data-target='#lookup-polis' ><i class='fa fa-search' aria-hidden='true'></i></a>
                    <!-- <input type='text' name='noteno' class='span3' id='noteno' style='font-size:13px !important;' placeholder='Note No' readonly> -->
                  </div>
                </div>
				<div class='control-group'>                     
                  <label class='control-label size-16' for='pnt'>Branch</label>
                  <div class='controls'>
                    <input type='hidden' name='branch_code' id='branch_code' >
                    <input type='text' name='branch_name' class='span3' id='branch_name' style='font-size:13px !important;' onkeyup='this.value = this.value.toUpperCase()' readonly>
                    <a href='#' id='polis' data-toggle='modal' data-target='#branch-lookup' ><i class='fa fa-search' aria-hidden='true'></i></a>
                    <!-- <input type='text' name='noteno' class='span3' id='noteno' style='font-size:13px !important;' placeholder='Note No' readonly> -->
                  </div>
                </div>
				
				
              </fieldset>
              <div class='span11'><hr></div>
              <fieldset class='span6'>
                <div class='control-group'>                     
                  <label class='control-label size-16' for='type'>Intermediary Type</label>
                  <div class='controls'>
                    <input type='hidden' name='typecode' id='typecode' >
                    <input type='text' name='intertype' class='span2'  id='type' style='font-size:13px !important;' readonly>
                    <a href='#' data-toggle='modal' data-target='#intermediaryType' ><i class='fa fa-search' aria-hidden='true'></i></a>
                  </div> <!-- /controls -->       
                </div> <!-- /control-group -->
                
                <div class='control-group'>                     
                  <label class='control-label size-16' for='code'>Intermediary Code</label>
                  <div class='controls'>
                    <input type='text' name='intercode' class='span1' onkeyup='this.value = this.value.toUpperCase()' id='code' style='font-size:13px !important;' readonly>
                    <a href='#' id='tcode' data-toggle='modal' data-target='#intermediaryCode' ><i class='fa fa-search' aria-hidden='true'></i></a>
                    <input type='text' name='intername' class='span3' id='name' style='font-size:13px !important;' placeholder='INTERMEDIARY NAME' readonly>
                  </div> <!-- /controls -->       
                </div> <!-- /control-group -->
                
                <div class='control-group'>                     
                  <label class='control-label size-16' for='bussines'>Class Of Business</label>
                  <div class='controls'>
                    <input type='text' name='cob' class='span1' id='bussines' style='font-size:13px !important;' readonly >
                    <a href='#' data-toggle='modal' data-target='#bussines-lookup'><i class='fa fa-search' aria-hidden='true'></i></a>
                    &nbsp;<span class="size-16" style="color: #333">Currency</span> &nbsp;<input type='text' name='Curren' class='span1' id='Curr' style='font-size:13px !important;' onkeyup='this.value = this.value.toUpperCase()'>
                    <a href="#" id="Curr-button" data-toggle="modal" data-target="#currency-first"><i class="fa fa-search" aria-hidden="true"></i></a> 
                  </div> <!-- /controls -->       
                </div> <!-- /control-group -->

                <div class='control-group'>                     
                  <label class='control-label size-16' for='period'>Production Period</label>
                  <div class='controls'>
                    <input type='text' name='periode1' class=' date periode-1' id='period-1' style='width : 80px;font-size:13px !important;' >
                    &nbsp;S / D&nbsp;
                    <input type='text' name='periode2' class=' date periode-2' id='period-2' style='width : 80px;font-size:13px !important;' >
                  </div> <!-- /controls -->       
                </div> <!-- /control-group -->
                

                <div class='control-group'>                     
                  <label class='control-label size-16' for='payment'>Payment Date</label>
                  <div class='controls'>
                    <input type='text' name='payment1' class=' date payment-1' id='payment-1' style='width : 80px;font-size:13px !important;' >
                    &nbsp;S / D&nbsp;
                    <input type='text' name='payment2' class=' date payment-2' id='payment-2' style='width : 80px;font-size:13px !important;' >
                  </div> <!-- /controls -->       
                </div> <!-- /control-group -->
               
                <div class='control-group'>
                  <label class='control-label size-16' for='rate'>Persentase Fee</label>
                  <div class='controls'>
                    <input type='text' name='rate' class='span1' id='rate' style='font-size:13px !important;'> &nbsp;&nbsp;(Example : 0.00)
                  </div> <!-- /controls -->       
                </div> <!-- /control-group -->

                <div class='control-group'>
                  <label class='control-label size-16' for='memo'>Memo</label>
                  <div class='controls'>
                    <textarea id='imemo' name='nmemo' class='span4' style='margin: 0px; width: 360px; height: 131px;' ></textarea>
                  </div> <!-- /controls -->       
                </div> <!-- /control-group -->
                
              </fieldset>
              <?php 
                // =============== PAGE METHOD OF PAYMENT
                include 'page/paymentrequest/new-layout-2.php';
              ?>
              <div class='span11'><hr></div>                
              <fieldset class='span11'>
                <div align='center'>
                  <a href='#' class='btn btn-primary btn-show' align='center' style='float:center; position: absolute; '>Retrive</a>
                  <a href='#' class='btn btn-primary r-trive' align='center' style='float:center; position: absolute; '>Retrive</a>
                </div>
                <div align='right'>
                  <a href="#!" name="preview" id="previewID" class="btn btn-primary">PREVIEW</a>&nbsp;
                  <a href='#!' id='btn-save-1' class='btn-save-1111'><input type='submit' name='save' id="btn-save-data-polis" style='float:right;' class='btn btn-primary' value='SAVE'></a>
                </div>
                
              </fieldset>

              
            </div> <!-- /form-horizontal -->
          </div> <!-- /tab-pane -->
        </div> <!-- /tab-content -->
        PR01
      </div> <!-- /widget-content -->
    </div> <!-- /span6 -->

  
  <div class='retrive'>
    <div class='retrive-main1'>
      <div class='retrive-main'>
      
          <div class='data'></div>
          <a href='#' class='btn btn-danger cancel'>CANCEL</a>
          <a href='#' class='btn btn-primary ok'>OK</a>
        

      </div>
    </div>
  </div>
  <?php 
    // ============== LOOKUP POLIS / NOTE
    include 'page/lookup/polis.php';
    // ============== LOOKUP INTERMEDIARY TYPE
    include 'page/lookup/intermediarytype.php';
    // ============== LOOKUP INTERMEDIARY CODE
    include 'page/lookup/intermediarycode.php';
    // ============== LOOKUP BUSINESS
    include 'page/lookup/bussines.php';
    // ============== LOOKUP FIRST CURRENCY
    include 'page/lookup/currency_first.php';
    // ============== LOOKUP CURRENCY
    include 'page/lookup/currency.php';
    // ============== LOOKUP TYPE OF PAYMENT
    include 'page/lookup/typeofpayment.php';
    // ============== LOOKUP PAY TO
     include 'page/lookup/payto.php';
	
	 include 'page/lookup/branch_data.php';
     
   ?>

  </form>
</div>

<script type='text/javascript'>
  $(document).ready(function() {
    $("#rate").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});
function validateForm() {
	var rate = document.forms['myformData']['rate'].value;
    var top = document.forms['myformData']['top'].value;
    var noteno = document.forms['myformData']['noteno'].value;
    var mop = document.forms['myformData']['mop'].value;
    var ptc = document.forms['myformData']['ptc'].value;
    var ccy = document.forms['myformData']['ccy'].value;
	var branchCode = document.forms['myformData']['branch_code'].value;

	if(branchCode == ''){
		swal('Branch Code must be Fill !!', '', 'error');
      return false;
	}
    if (rate == '') {
      swal('Persentase fee must be Fill !!', '', 'error');
      return false;
    }
    else if (top == ''){
      swal('Type Of Payment must be Fill !!', '', 'error');
      return false; 
    }
    if (noteno == '') 
    {
      if($('.pilih:checked').length == 0){
        swal('Check Your Detail Data Entry !', '', 'error');
        return false;
      }
      if(mop=='TRANSFER'){
        if(ccy == ''){
          swal('Currency must be Fill!', '', 'error');
          return false;
        }
        else if (ptc == '') {
          swal('Pay To / Client No must be Fill!', '', 'error');
          return false;
        };
      };
    };
}

</script>