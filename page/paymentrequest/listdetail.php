<?php 
	if (isset($_GET['id']) && !empty($_GET['id'])) 
	{
		$id 	=	$_GET['id'];
		$status =	$_GET['status'];
		$_SESSION['REQ_ID']	=	$id;
		include "page/lookup/approval_history.php";
	
		
		$queryHeader 		= "SELECT * FROM tpaymentrequestheader WHERE ISACTIVE=1 AND ID = '$id' ";
		$data		= mysqli_query($conn, $queryHeader) or die(mysqli_error($conn));
		$dataHeader	= mysqli_fetch_array($data);
		
		$createWhoData	 =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$dataHeader[CREATED_BY]'");
		$checkerWhoData	 =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$dataHeader[CHECKER_BY]'");
		$approvalWhoData =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$dataHeader[APPROVAL_BY]'");
		
		$createWho = mysqli_fetch_array($createWhoData);
		$checkerWho = mysqli_fetch_array($checkerWhoData);
		$approvalWho = mysqli_fetch_array($approvalWhoData);	
		
		
?>

<div class="print" style="font-size : 11px;">
	<div class="container">
		<div class="head">
			<h3>PAYMENT REQUEST</h3>
			<p> <?php echo $dataHeader['PaymentRequestNo']; ?></p>
		</div>
		<div class="isi">
			<?php include "page/paymentrequest/viewHeader.php"; ?>
			<br>
			<?php include "page/paymentrequest/viewContentPerCurr.php"; ?>			
		</div>
		<br>
		<?php include "page/paymentrequest/viewNoteDetail.php"; ?>
		<br>
		<?php 
			$sqlreject 		=	mysqli_query($conn, "SELECT REMARKS from thistory_approval WHERE REQUEST_ID = '$id' AND ACTION = (select Description from mgeneral_table where Code='PaymentRequest_STATUS' AND Value=3) ");
			$fetchreject 	=	mysqli_fetch_array($sqlreject);
			$numrow			=	mysqli_num_rows($sqlreject);
			if ($numrow > 0) 
			{
		 ?>
			<span style="color:red;">NOTE :
				<p><?php echo $fetchreject['REMARKS']; ?></p>
			</span>
		<?php }else{ 
			include "page/paymentrequest/viewFooter.php";
			}
		?>
		
		<br>
		<br>
		<div class="bottom">
			<a href="#" onclick="javascript:history.back()" class="btn btn-danger">EXIT</a>
			<a href="#" id="approvalhistory" data-toggle="modal" data-target="#lookup-ApprovalHistory" class="btn btn-primary">View History</a>
			<a href="report/report_payment_request.php?id=<?php echo $id; ?>" class="btn btn-primary" style="float:right;"></i> PRINT</a>
			<!-- <a href="report/report_payment_request2.php?id=<?php echo $id; ?>" class="btn btn-primary" style="float:right;"></i> PRINT2</a> -->
			<?php if ($status == 'REVISE'): ?>
				<a href="?page=payreqrevise&id=<?php echo $id; ?>&status=<?php echo $status; ?>" class="btn btn-primary" style="float:right;"></i> EDIT</a>
			<?php endif ?>
		</div>
	</div>PR03
</div>



<?php 
}
else
{
	echo "eror";
	echo "<script>javascript:history.back()</script>";
}
?>