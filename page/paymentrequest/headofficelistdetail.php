<?php 
  if (isset($_GET['id']) && !empty($_GET['id'])) 
  {
    $id   = $_GET['id'];
    $_SESSION['REQ_ID'] = $id;
    include "page/lookup/approval_history.php";
    
    $queryHeader    = "SELECT * FROM tpaymentrequestheader WHERE ISACTIVE=1 AND ID = '$id' ";
    $data   = mysqli_query($conn, $queryHeader) or die(mysqli_error($conn));
    $dataHeader = mysqli_fetch_array($data);
?>

<div class="print" style="font-size : 11px;">
  <div class="container">
    <div class="head">
      <h3>PAYMENT REQUEST</h3>
      <p> <?php echo $dataHeader['PaymentRequestNo']; ?></p>
    </div>
    <div class="isi">
      <?php include "page/paymentrequest/viewHeader.php"; ?>
      <br>
      <?php include "page/paymentrequest/viewContentPerCurr.php"; ?>      
    </div>
    <br>
    <br>
<!--     <?php 
      $sqlreject    = mysqli_query($conn, "SELECT a.REMARKS from thistory_approval a join mgeneral_table b on a.ACTION = b.Value where a.REQUEST_ID = '$id' AND a.ACTION = '3' ");
      $fetchreject  = mysqli_fetch_array($sqlreject);
      $numrow     = mysqli_num_rows($sqlreject);
      if ($numrow > 0) 
      {
     ?>
      <span style="color:red;">NOTE :
        <p><?php echo $fetchreject['REMARKS']; ?></p>
      </span>
    <?php } ?> -->
    <?php 
      include "page/paymentrequest/viewFooter.php";
    ?>
    
    <br>
    <br>
    <div class="bottom">
      <a href="report/report_payment_request_headoffice.php?id=<?php echo $id ?>" style="float: right;" class="btn btn-primary">Print</a>
      <a href="#" onclick="javascript:history.back()" class="btn btn-danger">EXIT</a>
      <a href="#" id="approvalhistory" data-toggle="modal" data-target="#lookup-ApprovalHistory" class="btn btn-primary">View History</a>

    </div>
  </div>PR07
</div>

<?php 
}
else
{
  echo "eror";
}
?>