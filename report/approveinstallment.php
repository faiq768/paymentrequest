<?php 
/*
** save-resubmit 				**
** Created By Faiq				**
** Modified By Hockey        	**
** Modified Date 20180313	 	**
** Update Header Data 			**
** Insert History Data 			**
** START 						**
*/

session_start();
include "../../koneksi/koneksi.php";
//Approaval Checker dan Approval Action
	if (isset($_GET['id']) && !empty($_GET['id'])) {
		$id			=	$_GET['id'];

		try
		{
			switch ($_GET['function']) 
			{
				case 'CHECKER':
					$remarks		=	'';
					$queryPayReq	=	"UPDATE tpaymentrequest_installment_detail SET MOD_DATE=now(), MOD_BY = '".$_SESSION['username']."', CHECKER_BY = '".$_SESSION['username']."' , STATUS_INS = (SELECT Description FROM mgeneral_table WHERE Code='PaymentRequest_STATUS' and Value=2) WHERE PaymentRequestNo = '$id' AND CHECKER_BY = ''";
					//$queryHistory 	=	"INSERT INTO thistory_approval values('PAYMENT_REQUEST','$id', (SELECT Description FROM mgeneral_table WHERE Code='PaymentRequest_STATUS' AND Value=2), NOW(),'".$_SESSION['username']."',3,'$remarks')";
					$_SESSION['notif'] = 'CHECKER';
					break;
				case 'REVISECHECKER':
					$remarks	=	$_GET['remarks'];
					$queryPayReq	=	"UPDATE tpaymentrequest_installment_detail SET MOD_DATE=now(), MOD_BY = '".$_SESSION['username']."', CHECKER_BY = '".$_SESSION['username']."', STATUS_INS = (SELECT Description FROM mgeneral_table WHERE Code='PaymentRequest_STATUS' and Value=4) WHERE PaymentRequestNo = '$id' AND CHECKER_BY='' ";
					//$queryHistory 	=	"INSERT INTO thistory_approval values('PAYMENT_REQUEST','$id',(SELECT Description FROM mgeneral_table WHERE Code='PaymentRequest_STATUS' AND Value=4), NOW(),'".$_SESSION['username']."',3,'$remarks')";
					$_SESSION['notif'] = 'REVISE';
					break;
				case 'REJECTCHECKER':
					$remarks	=	$_GET['remarks'];
					$queryPayReq 	=	"UPDATE tpaymentrequest_installment_detail SET MOD_DATE=now(), MOD_BY = '".$_SESSION['username']."', CHECKER_BY = '".$_SESSION['username']."', STATUS_INS = (SELECT Description FROM mgeneral_table WHERE Code='PaymentRequest_STATUS' and Value=3) WHERE PaymentRequestNo = '$id' AND CHECKER_BY = '' ";
					//$queryHistory 	=	"INSERT INTO thistory_approval values('PAYMENT_REQUEST','$id', (SELECT Description FROM mgeneral_table WHERE Code='PaymentRequest_STATUS' AND Value=3), NOW(),'".$_SESSION['username']."',3,'$remarks')";
					$_SESSION['notif'] = 'REJECT';
					break;

				case 'APPROVE':
					$remarks		=	'';
					$queryPayReq	=	"UPDATE tpaymentrequest_installment_detail SET MOD_DATE=now(), MOD_BY = '".$_SESSION['username']."', APPROVAL_BY = '".$_SESSION['username']."', STATUS_INS = (SELECT Description FROM mgeneral_table WHERE Code='PaymentRequest_STATUS' and Value=5) WHERE PaymentRequestNo = '$id' AND APPROVAL_BY = '' ";
					//$queryHistory 	=	"INSERT INTO thistory_approval values('PAYMENT_REQUEST','$id',(SELECT Description FROM mgeneral_table WHERE Code='PaymentRequest_STATUS' AND Value=5), NOW(),'".$_SESSION['username']."',4,'$remarks')";
					$_SESSION['notif'] = 'APPROVE';
					break;
				
			}

			if(isset($queryPayReq))
			{
				// Set autocommit to off
				mysqli_autocommit($conn,FALSE);
				mysqli_begin_transaction($conn, MYSQLI_TRANS_START_READ_WRITE);

				$dataPayReq = mysqli_query($conn, $queryPayReq)  or die(mysqli_error($conn));
				//$dataHistory = mysqli_query($conn, $queryHistory)  or die(mysqli_error($conn));

				// Commit transaction
				mysqli_commit($conn);
			}
		}
		catch (exception $e)
		{
			// Rollback transaction
			mysqli_rollback($conn);
		}
		finally
		{
			// close connection
			mysqli_close($conn);
		}
		header('location:../../installChecker');
	}
 ?>

/*
** END 						**
*/