<?php 
/*
** Created By Faiq				**
** Modified By Hockey        	**
** Modified Date 20180322	 	**
** Validation Created 			**
** Rapiin tampilan	 			**
*/ 
session_start();
ob_start();
//koneksi
include "../koneksi/koneksi.php";
// $conn = mysqli_connect('localhost','root','') or die("cannot connect");
// 		mysqli_select_db($conn, 'paymentrequest');

if (empty($_SESSION['username'])) 
{
	echo "FAILED";
}
else
{
	$user	= $_SESSION['username'];
	$datefrom 		= 	$_GET['datefrom'];
	$dateto	 		= 	$_GET['dateto'];
	$branch_code	= 	$_GET['bc'];
	$status	 		=	$_GET['status'];

	$intermediary 	=	$_GET['intermediary'];
	$ccy 			=	$_GET['ccy'];

	$datePayfrom 	=	$_GET['datePayfrom'];
	$datePayto		=	$_GET['datePayto'];

	$datefrom1 = date_create($datefrom);
	$datefrom2 = date_format($datefrom1,"Y-m-d");
	
	$dateto1 = date_create($dateto);
	$dateto2 = date_format($dateto1,"Y-m-d");

	$datePayfrom1 = date_create($datePayfrom);
	$datePayfrom2 = date_format($datePayfrom1,"Y-m-d");
	
	$datePayto1 = date_create($datePayto);
	$datePayto2 = date_format($datePayto1,"Y-m-d");



	if ($branch_code != '') {
		$branch_code = " AND SUBSTRING(a.PaymentRequestNo,1,2) IN (".$branch_code.")";
	}

	if ($intermediary != '') {
		$intermediary = " AND a.INTERMEDIARY_TYPE='".$intermediary."'";
	}
	if ($ccy != '') {
		$ccy = " AND b.CCY='".$ccy."'";
	}
	if ($status == 'PAID') {
		$status = " AND PAID_STATUS='".$status."' ";
	}else{
		$staus = " AND PAID_STATUS='".$status."' ";
	}
	if ($datePayfrom != '' && $datePayto != '') {
		$datePay = " AND b.PAY_PAID_DATE >='".$datePayfrom2."' AND b.PAY_PAID_DATE <='".$datePayto2."'";
		$datePayPer = $datePayfrom ."S/D". $datePayto;
	}else{
		$datePay = '';
		$datePayPer = "";
	}

	
// 	{
		
		// echo $fetch['PaymentRequestNo'];

		$content = ob_get_clean();

		$content = '
		<html>
		<head>
		<style type="text/css">
			p {
				line-height:1.5 !important;
			}
			.two{
				font-size : 10.5px;
				border-collapse : collapse;
			}
			.two thead tr th{
				text-align : center;
				padding : 5px;
			}
			.two tbody tr td{
				padding : 4px;
			}
			.table{
				border-collapse : collapse;
			}
			.table thead tr th{
				text-align : center;
				padding : 5px;
			}
			.ttd{
				width :450px;
				height :100px;
				overflow: hidden;
				margin-left : 623px;

			}
			.ttd table{
				border-collapse : collapse;
			}
			.ttd table tr th{
				padding : 5px 20px 5px 20px;
			}
			.ttd table tr td{
				padding : 40px;
				height : 40px;
				text-align : center;
			}
			.footer{
				width :450px;
				height :100px;
				overflow: hidden;
				margin-left : 623px;
				text-align : center;
			}
		</style>
		</head>
		<body>
		';
// // Tanggal Print, jika tidak diinginkan hapus salah satu
// //d = tanggal, m = bulan, Y = tahun, h = jam , i = menit, s=detik, a= am/pm
		date_default_timezone_set('Asia/Jakarta');
		$tgl = date("dmY H:i:s");
		$content .= "
				<div style='text-align : center' padding-left: 8px;>
					<h3>PAYMENT REQUEST INSTALLMENT</h3>
					<p> $fetch[PaymentRequestNo]</p>
					<hr>
				</div>
			<page_footer>
			
				<div align='left'>
					$_SESSION[username] - $tgl
				</div>
				
				<div align='right'>
					[[page_cu]]/[[page_nb]]
				</div>
			</page_footer>
			
			<div>
				<h3>LAPORAN PEMBAYARAN</h3>
				<table>
					<tr>
						<td>INTERMEDIARY</td>
						<td>:</td>
						<td>$_GET[intermediary]</td>
					</tr>
					<tr>
						<td>CURRENCY</td>
						<td>:</td>
						<td>$_GET[ccy]</td>
					</tr>
					<tr>
						<td>INSTALLMENT PERIODE</td>
						<td>:</td>
						<td>$datefrom &nbsp; S/D &nbsp; $dateto</td>
					</tr>
					<tr>
						<td>PAYMENT PERIODE</td>
						<td>:</td>
						<td>$datePayfrom &nbsp; S/D &nbsp; $datePayto</td>
					</tr>
				</table>
				
				<br>
				
			</div>
			<br/>

			<table border='1' class='table'>
				<thead>
					<tr>
						<td>NO</td>
						<td>PAYMENT REQUEST NO</td>
						<td>CLIENT</td>
						<td>CCY</td>
						<td>GROSS PREMIUM</td>
						<td>AMOUNT PAID</td>
						<td>AVERAGE</td>
						<td>NO. VOUCHER</td>
						<td>PAID DATE</td>
					</tr>
				</thead>
				<tbody>
			";

		$query = "SELECT a.PaymentRequestNo,b.Installment_ke,a.Client,b.CCY,b.Amount,b.Installment,b.Average,b.VoucherNo,DATE_FORMAT(b.PAY_PAID_DATE, '%d-%m-%Y') AS PAY_PAID_DATE FROM tpaymentrequest_installment a JOIN tpaymentrequest_installment_detail b ON b.installment_id = a.id where a.CREATED_DATE >= '$datefrom2' AND a.CREATED_DATE <= '$dateto2' $intermediary $ccy $datePay $branch_code $status ";
		$data = mysqli_query($conn, $query) or die(mysqli_error($conn));
			$no = 1;
			while($fetch = mysqli_fetch_array($data)){
				$content .= "
					<tr>
						<td style='text-align:center;'>$no</td>
						<td style='text-align:center;'>$fetch[PaymentRequestNo] / $fetch[Installment_ke]</td>
						<td style='width:150px;'>$fetch[Client]</td>
						<td style='text-align:center;'>$fetch[CCY]</td>
						<td style='text-align:right;'>".number_format($fetch[Amount] , 2 , '.' , ',' )."</td>
						<td style='text-align:right;'>".number_format($fetch[Installment] , 2 , '.' , ',' )."</td>
						<td style='text-align:center;'>$fetch[Average]%</td>
						<td style='text-align:center;'>$fetch[VoucherNo]</td>
						<td style='text-align:center;'>$fetch[PAY_PAID_DATE]</td>
					</tr>
				";
			$no++;
		}

		$content .= "
				</tbody>
			</table>
			<script src='../js/jquery-1.7.2.min.js'></script>
			<script src='../js/jquery.rowspanizer.js-master/jquery.rowspanizer.min.js'></script>
		</body>
		</html>
		";

		require_once('../html2pdf/html2pdf.class.php');
		//$html2pdf = new HTML2PDF('L','A4','en', array('6','10','6','10'));
		$html2pdf = new HTML2PDF('L', 'A4', 'en', true, 'UTF-8', array('20','10','20','10'));
		$html2pdf -> writeHTML($content);
		$html2pdf -> Output('Report Paymet Request.pdf');
		
}
?>