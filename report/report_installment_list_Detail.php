<?php 
/*
** Created By Faiq				**
*/ 
session_start();
ob_start();
//koneksi
include "../koneksi/koneksi.php";
// $conn = mysqli_connect('localhost','root','') or die("cannot connect");
// 		mysqli_select_db($conn, 'paymentrequest');

if (empty($_GET['id']) || empty($_SESSION['username'])) 
{
	echo "FAILED";
}
else
{
	$id 				=	$_GET['id'];
	$status 			=	$_GET['status'];
	// $_SESSION['REQ_ID']	=	$id;

	// $idInstallment	= 	$_GET['ID_Install'];
	// $id 			= 	$_GET['id'];
	$user			= 	$_SESSION['username'];
	// $CCY    		= 	$_GET['CCY'];

	$queryHeader 		= "SELECT * FROM tpaymentrequestheader WHERE ISACTIVE=1 AND ID = '$id' ";
	$data				= mysqli_query($conn, $queryHeader) or die(mysqli_error($conn));
	$dataHeader			= mysqli_fetch_array($data);
	
	$createWhoData	 	= mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$dataHeader[CREATED_BY]'");
	$checkerWhoData	 	= mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$dataHeader[CHECKER_BY]'");
	$approvalWhoData 	= mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$dataHeader[APPROVAL_BY]'");
	
	$createWho 			= mysqli_fetch_array($createWhoData);
	$checkerWho 		= mysqli_fetch_array($checkerWhoData);
	$approvalWho 		= mysqli_fetch_array($approvalWhoData);

		$content = ob_get_clean();

		$content = '
		<style type="text/css">
			p {
				line-height:1.5 !important;
			}
			.two{
				font-size : 10.5px;
				border-collapse : collapse;
			}
			.two thead tr th{
				text-align : center;
				padding : 5px;
			}
			.two tbody tr td{
				padding : 4px;
			}
			.table-installment{
				padding-left : 100px;
			}
			.ttd{
				width :450px;
				height :100px;
				overflow: hidden;
				margin-left : 623px;

			}
			.ttd table{
				border-collapse : collapse;
			}
			.ttd table tr th{
				padding : 5px 20px 5px 20px;
			}
			.ttd table tr td{
				padding : 40px;
				height : 40px;
				text-align : center;
			}
			.footer{
				width :450px;
				height :100px;
				overflow: hidden;
				margin-left : 623px;
				text-align : center;
			}
		</style>
		';
// Tanggal Print, jika tidak diinginkan hapus salah satu
//d = tanggal, m = bulan, Y = tahun, h = jam , i = menit, s=detik, a= am/pm
		date_default_timezone_set('Asia/Jakarta');
		$tgl = date("dmY H:i:s");
		$content .= "
		<page>

				<div style='text-align : center' padding-left: 8px;>
					<h3>PAYMENT REQUEST INSTALLMENT</h3>
					<p>".$dataHeader['PaymentRequestNo']."</p>
					<hr>
				</div>
			<page_footer style='font-size:8px; margin-top:5px; bottom:5px;'>
				<table style='width:100%'>
					<tr>
						<td style='width:50%'>$_SESSION[username] - $tgl</td>
						<td style='text-align: right; width:50%'>[[page_cu]]/[[page_nb]]</td>
					</tr>
				</table>
				
			</page_footer>

			
			<div style='font-size:10px;'>
				<p>Kepada Yth,</p>
				<p>Bagian Branch Co / Bagian Finance</p>
				<br>
				<p>Mohon diberikan persetujuan untuk pembayaran sebagai berikut :</p>
				<br>
				<table class='first'>
					<tr>
						<td>PAY TO / CLIENT NO</td>
						<td>:</td>
						<td>".$dataHeader['IntermediaryName']." / ".$dataHeader['PayTo']." </td>
					</tr>
					<tr>
						<td>BENEFICIARY NAME</td>
						<td>:</td>
						<td> ".$dataHeader['BeneficiaryName']." </td>
					</tr>
					<tr>
						<td>BANK</td>
						<td>:</td>
						<td> ".$dataHeader['Bank']." </td>
					</tr>
					<tr>
						<td>DESTINATION ACCOUNT NUMBER</td>
						<td>:</td>
						<td> ".$dataHeader['DestinationAccountNumber']." </td>
					</tr>
					<tr>
						<td>BRANCH </td>
						<td>:</td>
						<td> ".$dataHeader['Branch']." </td>
					</tr>
					<tr>
						<td>CITY</td>
						<td>:</td>
						<td> ".$dataHeader['City']." </td>
					</tr>
					<tr>
						<td>RATE</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr>
						<td>TYPE OF PAYMENT</td>
						<td>:</td>
						<td> ".$dataHeader['Rate']." </td>
					</tr>
					<tr>
						<td style='vertical-align: top'>Memo</td>
						<td style='vertical-align: top'>:</td>
						<td width='800' style='word-wrap: break-word;'> ".$dataHeader['Memo']." </td>
					</tr>
				</table>
			</div><h5 class='h5-installment'>Installment Detail :</h5>";

		$SQLperCcy = mysqli_query($conn, "SELECT a.CCY 
		 									FROM tpaymentrequest_installment_detail a
											JOIN tpaymentrequest_installment b ON b.id = a.installment_id 
		 									where b.PaymentRequestNo = '".$dataHeader['PaymentRequestNo']."' GROUP BY CCY ");
		while ($fetchPerCcy = mysqli_fetch_array($SQLperCcy)) {
			$ccy = $fetchPerCcy["CCY"];
			
			$content .= "
			<div class='table-installment'>
			<table class='two' border='1px' cellpadding='1' cellspacing='0' style='font-size:9px;'>
				<thead>
					<tr>
						<th>NO</th>
						<th>NO.PAYMENT REQUEST</th>
						<th>SOB</th>
						<th>CLIENT</th>
						<th>CCY</th>
						<th>GROSS PREMIUM</th>
						<th>JUMLAH YANG DIBAYARKAN(%)</th>
						<th>AVERAGE(%)</th>
						<th>DUE DATE</th>
					</tr>
				</thead>
				<tbody>";
			$SQL_INSTALLMENT = "SELECT b.PaymentRequestNo, a.installment_ke, b.INTERMEDIARY_TYPE, b.Client, a.CCY, a.Amount, a.Installment, a.Average, a.VoucherNo, DATE_FORMAT(a.Due_Date, '%d-%m-%Y') AS Due_Date 
											FROM tpaymentrequest_installment_detail a
											JOIN tpaymentrequest_installment b ON b.id = a.installment_id
											WHERE b.PaymentRequestNo = '".$dataHeader['PaymentRequestNo']."' 
											AND a.CCY = '$ccy'";
			$view = mysqli_query($conn, $SQL_INSTALLMENT);
			$number =1;
			while($sql = mysqli_fetch_array($view)){
				$PaymentRequestNo 	= $sql['PaymentRequestNo'];
				$installment_ke 	= $sql['installment_ke'];
				$INTERMEDIARY_TYPE 	= $sql['INTERMEDIARY_TYPE'];
				$Client 			= $sql['Client'];
				$CCY_I				= $sql['CCY'];
				$AMOUNT 			= number_format($sql['Amount'] , 2 , '.' , ',' );
				$INSTALLMENT 		= number_format($sql['Installment'] , 2 , '.' , ',' );
				$AVERAGE 			= $sql['Average'];
				$DUE_DATE 			= $sql['Due_Date'];

			$content .="
					<tr>
						<td style='text-align: center;'>$number</td>
						<td>$PaymentRequestNo / $installment_ke</td>
						<td>$INTERMEDIARY_TYPE</td>
						<td>$Client</td>
						<td>$CCY_I</td>
						<td style='text-align: right;'>$AMOUNT</td>
						<td style='text-align: right;'>$INSTALLMENT</td>
						<td style='text-align: center;'>$AVERAGE</td>
						<td>$DUE_DATE</td>
					</tr>";
			$number++;}
			$SQLtotal  = mysqli_query($conn ,"SELECT a.CCY, SUM(a.Installment) AS Installment, 
																SUM(a.Average) AS Average
												FROM tpaymentrequest_installment_detail a
												JOIN tpaymentrequest_installment b ON b.id = a.installment_id 
												where b.PaymentRequestNo = '".$dataHeader['PaymentRequestNo']."' AND a.CCY = '".$fetchPerCcy['CCY']."' ");
				$fetchTotal = mysqli_fetch_array($SQLtotal);

			$content .= "
					<tr>
						<td colspan='4' style='text-align: right;'><b>TOTAL</b></td>
					<td><b>".$fetchTotal['CCY']."</b></td>
					<td colspan='2' style='text-align: right;'><b>".number_format($fetchTotal['Installment'] , 2 , '.' , ',' )."</b></td>
					<td style='text-align: center;'><b>".$fetchTotal['Average']."%</b></td>
					</tr>
				</tbody>
			</table>
			</div>
			<br>
				";

			
			// 	

				// $content .= "";
			// $number++}
			
		}

		$content .= "
			<div>
				<span style='font-size:9px;'>
					Note :  <ul>
								<li>This application already approve by system</li>
							</ul> 
						   
				</span>
			</div>
			<div>
				<table border='1px' cellpadding='1' cellspacing='1' style='width :450px; height :100px; overflow: hidden;margin-left : 500px; text-align : center; align:center; font-size:10px;'>
					
						<tr>
							<th>DIBUAT</th>
							<th>DIPERIKSA</th>
							<th>DISETUJUI</th>
						</tr>
						<tr>
							<td style='height:100px; width:150px; text-align:justify; padding:0;'><span>&nbsp;</span></td>
							<td style='height:100px; width:150px; text-align:justify; padding:0;'><span>&nbsp;</span></td>
							<td style='height:100px; width:150px; text-align:justify; padding:0;'><span>&nbsp;</span></td>
						</tr>
						<tr>
							<td><span>$createWho[full_name]</span></td>
							<td><span>$checkerWho[full_name]</span></td>
							<td><span>$approvalWho[full_name]</span></td>
						</tr>
					
				</table>
			</div>
		</page>
		";

		require_once('../html2pdf/html2pdf.class.php');
		//$html2pdf = new HTML2PDF('L','A4','en', array('6','10','6','10'));
		$html2pdf = new HTML2PDF('L', 'A4', 'en', true, 'UTF-8', array('20','20','20','10'));
		$html2pdf -> writeHTML($content);
		$html2pdf -> Output('Installment List Paymet Request.pdf');
		
	
}
?>