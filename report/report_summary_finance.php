<?php
/*
** Created By Hockey			**
** Created Date 20180404	 	**
** Print PayReq Periode			**
*/ 
session_start();
ob_start();
//koneksi
include "../koneksi/koneksi.php";

	$datefrom 		= 	$_GET['datefrom'];
	$dateto	 		= 	$_GET['dateto'];
	$branch_code	= 	$_GET['bc'];
	$intermediary	=	$_GET['intermediary'];
	$CCY 			=	$_GET['ccy'];
	$status			=	$_GET['status'];

	if ($branch_code != "") {
		$branch = "  a.PaymentRequestNo LIKE '$branch_code%' and ";
	}else{
		$branch = "";
	}
	
	
	if ($CCY != "") {
		$CCYD = " b.CCY = '$CCY' and ";
	}else{
		$CCYD = "";
	}

	if ($status == 'PAID') {
		$judul = '( PAID )';
	}elseif ($status == 'NULL') {
		$judul = '( UNPAID )';
	}

	$datefrom1 = date_create($datefrom);
	$datefrom2 = date_format($datefrom1,"Y-m-d");
	
	$dateto1 = date_create($dateto);
	$dateto2 = date_format($dateto1,"Y-m-d");

?>
		
			<!-- // $id 	= $rows['ID'];
			// $payreqNo 	= $rows['PaymentRequestNo'];
								

			date_default_timezone_set('Asia/Jakarta');
			$tgl = date("dmY H:i:s");
			
			$content = ob_get_clean();
				
			$content .= " -->
			<style type="text/css">
			  table.page_header {width: 1020px; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 2mm }
			  table.page_footer {width: 1020px; border: none; background-color: #DDDDFF; border-top: solid 1mm #AAAADD; padding: 2mm}
			  h1 {color: #000033}
			  h2 {color: #000055}
			  h3 {color: #000077}
			</style>
	<page backtop="14mm" backbottom="14mm" backleft="1mm" backright="10mm">
  		<page_header>
			<table class="page_header">
			      <tr>
			        <td style="text-align: left;    width: 30%">PAYMENT REQUEST</td>
			        <td style="text-align: center;    width: 40%">LAPORAN PEMBAYARAN <?php echo $judul; ?></td>
			        <td style="text-align: right;    width: 30%"><?php echo date('d/m/Y'); ?></td>
			      </tr>
			    </table>
			  </page_header>
			  <!-- Setting Footer -->
			  <page_footer>
			    <table class="page_footer">
			      <tr>
			        <td style="width: 50%; text-align: left">
			         Dicetak oleh: <?php echo $_SESSION['username']; ?>
			        </td>
			        <td style="width: 50%; text-align: right">
			          Halaman [[page_cu]]/[[page_nb]]
			        </td>
			      </tr>
			    </table>
			  </page_footer>
			<style type='text/css'>
			.tabel2 {

			    border-collapse: collapse;
			  }
			  .tabel2 th, .tabel2 td {
			      padding: 5px 5px;
			      border: 1px solid #000;
			      font-size : 5px !important;
			  }
			.two{
				font-size : 9px;
				border-collapse : collapse;
			}
			.two thead tr th{
				text-align : center;
				padding : 5px;
			}
			.two tbody tr td{
				padding : 4px;
			}
			
			.footer{
				width :450px;
				height :100px;
				overflow: hidden;
				margin-left : 623px;
				text-align : center;
			}
			</style>

				<div class='kertas'>
					<br>
					<table class='tabel2' border='1' cellpadding='2' cellspacing='0' style='font-size:8px;'>
						
							<tr>
								<th style="width: 5px; text-align: center;">NO</th>
								<th>SOB</th>
								<th>CLIENT</th>
								<th>PERIODE PRODUKSI</th>
								<th>CCY</th>
								<th>GROSS PREMIUM</th>
								<th>TOTAL PEMBAYARAN</th>
								<th>AVERAGE (%)</th>
							</tr>
						
<?php 
		if ($status == 'PAID') {
			$data2	=	mysqli_query($conn, "
				SELECT a.IntermediaryType,a.IntermediaryName,a.typeofpayment,b.CCY
				from tpaymentrequestheader a
				inner join tpaymentrequestdetail b on a.id=b.paymentrequestid
				where 
				 ifnull(a.voucherNo,'')<>'' and date_format(a.created_date,'%Y-%m-%d') between '$datefrom2' AND '$dateto2'
				Group by a.IntermediaryType
				");
		}elseif ($status == 'NULL') {
			$data2	=	mysqli_query($conn, "
				SELECT a.IntermediaryType,a.IntermediaryName,a.typeofpayment,b.CCY
				from tpaymentrequestheader a
				inner join tpaymentrequestdetail b on a.id=b.paymentrequestid
				where 
				ifnull(a.voucherNo,'')='' and STATUS_PR='APPROVE' and date_format(a.created_date,'%Y-%m-%d') between '$datefrom2' AND '$dateto2'
				Group by a.IntermediaryType
				");
		}
			$no = 1;
			while ($while = mysqli_fetch_array($data2)) 
			{
				// $GrossPremium 	= 	number_format( $while['GrossPremium'] , 2 , '.' , ',' );
				// $Amount 		=	number_format( $while['Amount'] , 2 , '.' , ',' );
				
				// $bankSettlementData = wordwrap($while[BankSettlement], 12, '<br />', true);
				// $accountSettlementData = wordwrap($while[AccountSettlement], 12, '<br />', true);
				?>
				
						<tr>
							<td align='center' width='5'><?php echo $no ?></td>
							<td style='width: 50px; word-wrap: break-word;' align='center'><?php echo $while['IntermediaryType']; ?></td>
							
					 <?php
						 if ($intermediary != "") {
							$intermediaryD = " a.IntermediaryType = '$intermediary' and";
						}else{
							$intermediaryD = " a.IntermediaryType = '".$while['IntermediaryType']."' ";
						}
						$sqlIntermediaryName	=	mysqli_query($conn, "SELECT DISTINCT a.intermediaryName,a.ID
											from tpaymentrequestheader a
											inner join tpaymentrequestdetail b on a.id=b.paymentrequestid
											where $intermediaryD and
											ifnull(a.voucherNo,'')<>'' and date_format(a.created_date,'%Y-%m-%d') between '$datefrom2' AND '$dateto2'
											Group by a.IntermediaryName
											");
							while ($IntermediaryNameData=	mysqli_fetch_array($sqlIntermediaryName)) {
								$QUERY_ROW_CCY = mysqli_query($conn, "SELECT DISTINCT CCY from tpaymentrequestdetail where PaymentRequestID = '".$IntermediaryNameData['ID']."' ");
								$ROW_CCY = mysqli_num_rows($QUERY_ROW_CCY);

								if ($ROW_CCY > 1) {
									$BR = '<br><br><br>';
								}else{
									$BR = '';
								}

								echo "<td style='width: 250px; word-wrap: break-word;'>". $IntermediaryNameData[intermediaryName] .$BR."<hr style='border: none; border-top: 1px solid #333 !important;'>";
							}
					?> 

							</td>
							<td align='center' style="width: 90px;">
						<!-- <?php
								if ($intermediary != "") {
									$intermediaryD = " a.IntermediaryType = '$intermediary' and";
								}else{
									$intermediaryD = " a.IntermediaryType = '".$while['IntermediaryType']."' ";
								}
								$sqlDate	=	mysqli_query($conn, "SELECT DISTINCT a.intermediaryName,a.ID
													from tpaymentrequestheader a
													inner join tpaymentrequestdetail b on a.id=b.paymentrequestid
													where $intermediaryD and
													ifnull(a.voucherNo,'')<>'' and date_format(a.created_date,'%Y-%m-%d') between '$datefrom2' AND '$dateto2'
													Group by a.IntermediaryName");
									while ($InteDate=	mysqli_fetch_array($sqlDate)) {

								$QUERY_ROW_CCY = mysqli_query($conn, "SELECT DISTINCT CCY from tpaymentrequestdetail where PaymentRequestID = '".$InteDate['ID']."' ");
								$ROW_CCY = mysqli_num_rows($QUERY_ROW_CCY);

								if ($ROW_CCY > 1) {
									$BR = '<br><br><br>';
								}else{
									$BR = '';
								}

										echo  $datefrom." - ".$dateto .$BR."<hr style='border: none; border-top: 1px solid #333 !important;'>" ;
									}
							?> -->
									
								</td>
							<td style='word-wrap: break-word; width: 10px;' align='center'>
						<!-- <?php 
								if ($intermediary != "") {
									$intermediaryD = " a.IntermediaryType = '$intermediary' and";
								}else{
									$intermediaryD = " a.IntermediaryType = '".$while['IntermediaryType']."' ";
								}
						// ============================== FIELD CCY =======================================
							$sqlCCY	=	mysqli_query($conn, "SELECT b.CCY,a.ID
										from tpaymentrequestheader a
										inner join tpaymentrequestdetail b on a.id=b.paymentrequestid
										where $intermediaryD and
												ifnull(a.voucherNo,'')<>'' and date_format(a.created_date,'%Y-%m-%d') between '$datefrom2' AND '$dateto2'
												Group by a.IntermediaryName,b.CCY");
							while ($CCYData=	mysqli_fetch_array($sqlCCY)) {

								$QUERY_ROW_CCY = mysqli_query($conn, "SELECT DISTINCT CCY from tpaymentrequestdetail where PaymentRequestID = '".$CCYData['ID']."' ");
								$ROW_CCY = mysqli_num_rows($QUERY_ROW_CCY);

								if ($ROW_CCY > 1) {
									$BR = '<br><br>';
								}else{
									$BR = '';
								}

								echo $CCYData[CCY]."<hr style='border: none; border-top: 1px solid #333 !important;'>";
							}
						?>  -->
							</td>
							<td style='word-wrap: break-word; width: 70px;' align='right'>
						<!-- <?php
						// ============================== FIELD GROSS =======================================
							$sqlGross	=	mysqli_query($conn, "SELECT sum(b.GrossPremium) as Gross
											from tpaymentrequestheader a
											inner join tpaymentrequestdetail b on a.id=b.paymentrequestid
											where a.intermediaryType = '".$while['IntermediaryType']."' 
											
											Group by b.CCY");
							while ($GrossData=	mysqli_fetch_array($sqlGross)) {

								$GrossPremium 	= 	number_format( $GrossData[Gross] , 2 , '.' , ',' );
								echo $GrossPremium ."<hr style='border: none; border-top: 1px solid #333 !important;'>";
							}
						?> -->
							</td>
							<td style='word-wrap: break-word; width: 70px;' align='right'>
						<!-- <?php 
						// ============================== FIELD AMOUNT =======================================
							$sqlAmount	=	mysqli_query($conn, "SELECT sum(b.amount) as Nilai
											from tpaymentrequestheader a
											inner join tpaymentrequestdetail b on a.id=b.paymentrequestid
											where a.intermediaryType = '".$while['IntermediaryType']."' 
											
											Group by b.CCY");
							while ($AmountData=	mysqli_fetch_array($sqlAmount)) {
								$Amount 	= 	number_format( $AmountData[Nilai] , 2 , '.' , ',' );
								echo $Amount ."<hr style='border: none; border-top: 1px solid #333 !important;'>";
							}
						?> -->
							</td>
							<td style='word-wrap: break-word; width: 20px;' align='right'>
						<!-- <?php
						// ============================== FIELD AVERAGE =======================================
							$sqlAmount	=	mysqli_query($conn, "SELECT a.rate
											from tpaymentrequestheader a
											inner join tpaymentrequestdetail b on a.id=b.paymentrequestid
											where a.intermediaryType = '".$while['IntermediaryType']."' 
											
											Group by b.CCY");
							while ($AmountData=	mysqli_fetch_array($sqlAmount)) {
								echo $AmountData[rate] ."<hr style='border: none; border-top: 1px solid #333 !important;'>" ;
							}
						?> -->
							</td>
						</tr>
			<?php
				$no++;
			}
			
			?>
					</table>
					<br>
				</div>
				
			</page>
		
<?php
$content = ob_get_clean();
 include '../html2pdf/html2pdf.class.php';
 try
{
    $html2pdf = new HTML2PDF('L', 'A4', 'en', false, 'UTF-8', array(10, 10, 4, 10));
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->writeHTML($content);
    $html2pdf -> Output('Report Payment Request Finance.pdf');
}
catch(HTML2PDF_exception $e) {
    echo $e;
    exit;
}
?>