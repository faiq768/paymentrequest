<?php 
/*
** Created By Faiq				**
** Modified By Hockey        	**
** Modified Date 20180322	 	**
** Validation Created 			**
** Rapiin tampilan	 			**
*/ 
session_start();
ob_start();
//koneksi
include "../koneksi/koneksi.php";
// $conn = mysqli_connect('localhost','root','') or die("cannot connect");
// 		mysqli_select_db($conn, 'paymentrequest');

if (empty($_GET['id']) || empty($_SESSION['username'])) 
{
	echo "FAILED";
}
else
{
	$ID 	= $_GET['id'];
	$user	= $_SESSION['username'];

	$queryPrint = "UPDATE tpaymentrequestheader 
							SET CountPrint = CountPrint + 1
								, MOD_DATE = NOW()
								, MOD_BY = '$user'
							WHERE ID = $ID ";
	$dataPrint		=	mysqli_query($conn, $queryPrint) or die(mysqli_error($conn));


	$query = "SELECT * FROM tpaymentrequestheader where ID = '$ID' AND CREATED_BY='".$_SESSION['username']."' ";
	if($data = mysqli_query($conn, $query))
	{
		$fetch = mysqli_fetch_array($data);

		$content = ob_get_clean();

		$content = '
		<style type="text/css">
		page{
			
		}
			p {
				line-height:1.5 !important;
			}
			.two{
				font-size : 10.5px;
				border-collapse : collapse;
			}
			.two thead tr th{
				text-align : center;
				padding : 5px;
			}
			.two tbody tr td{
				padding : 4px;
			}
			.ttd{
				width :450px;
				height :100px;
				overflow: hidden;
				margin-left : 623px;

			}
			.ttd table{
				border-collapse : collapse;
			}
			.ttd table tr th{
				padding : 5px 20px 5px 20px;
			}
			.ttd table tr td{
				padding : 40px;
				height : 40px;
				text-align : center;
			}
			.footer{
				width :450px;
				height :100px;
				overflow: hidden;
				margin-left : 623px;
				text-align : center;
			}
		</style>
		';
// Tanggal Print, jika tidak diinginkan hapus salah satu
//d = tanggal, m = bulan, Y = tahun, h = jam , i = menit, s=detik, a= am/pm
		date_default_timezone_set('Asia/Jakarta');
		$tgl = date("dmY H:i:s");
		$content .= "
		<page>

				<div style='text-align : center' padding-left: 8px;>
					<h3>PAYMENT REQUEST</h3>
					<p> $fetch[PaymentRequestNo]</p>
					<hr>
				</div>
			<page_footer style='font-size:8px; margin-top:5px; bottom:5px;'>
				<table style='width:100%'>
					<tr>
						<td style='width:50%'>$_SESSION[username] - $tgl ".strrev(str_replace('-', '', $fetch[PaymentRequestNo]))."</td>

						<td style='text-align: right; width:50%'>[[page_cu]]/[[page_nb]]</td>
					</tr>
				</table>
				
			</page_footer>

			
			<div style='font-size:10px;'>
				<p>Kepada Yth,</p>
				<p>Bagian Branch Co / Bagian Finance</p>
				<br>
				<p>Mohon diberikan persetujuan untuk pembayaran sebagai berikut :</p>
				<br>
				<table class='first'>
					<tr>
						<td>PAY TO / CLIENT NO</td>
						<td>:</td>
						<td>$fetch[BeneficiaryName] / $fetch[PayTo] </td>
					</tr>
					<tr>
						<td>BENEFICIARY NAME</td>
						<td>:</td>
						<td>$fetch[BeneficiaryName] </td>
					</tr>
					<tr>
						<td>BANK</td>
						<td>:</td>
						<td>$fetch[Bank] </td>
					</tr>
					<tr>
						<td>DESTINATION ACCOUNT NUMBER</td>
						<td>:</td>
						<td>$fetch[DestinationAccountNumber]</td>
					</tr>
					<tr>
						<td>BRANCH </td>
						<td>:</td>
						<td>$fetch[Branch]</td>
					</tr>
					<tr>
						<td>CITY</td>
						<td>:</td>
						<td>$fetch[City]</td>
					</tr>
					<tr>
						<td>RATE</td>
						<td>:</td>
						<td>$fetch[Rate]</td>
					</tr>
					<tr>
						<td>TYPE OF PAYMENT</td>
						<td>:</td>
						<td>$fetch[TypeOfPayment]</td>
					</tr>
					<tr>
						<td style='vertical-align: top'>Memo</td>
						<td style='vertical-align: top'>:</td>
						<td width='800' style='word-wrap: break-word;'>$fetch[Memo]</td>
					</tr>
				</table>
			</div>
			<br/>";
	
		$content .= "
			<table class='two' border='1px' cellpadding='1' cellspacing='0' style='font-size:9px;'>
				<thead>
					<tr>
						<th rowspan='2'>NO</th>
						<th rowspan='2'>POLIS NUMBER</th>
						<th rowspan='2'>CORR</th>
						<th rowspan='2'>INSURED NAME</th>
						<th colspan='6'>SETTLEMENT REFFERENCE</th>
						<th rowspan='2'>CCY</th>
						<th rowspan='2'>GROSS PREMIUM</th>
						<th rowspan='2'>AMOUNT</th>
						<th rowspan='2'>DESC</th>
					</tr>
					<tr>
						<th>NOTE NO</th>
						<th>STATUS</th>
						<th>DATE</th>
						<th>BANK</th>
						<th>ACCOUNT</th>
						<th>VOUCHER</th>
					</tr>
				</thead>
				<tbody>";

		$detailID = $fetch['ID'];
		$data2	=	mysqli_query($conn, "SELECT a.*,(CASE WHEN DATEDIFF(b.PAID_DATE, b.DUE_DATE) > 0 THEN 'OVERDUE' ELSE 'DUE' END) AS GROUP_TYPE FROM tpaymentrequestdetail a JOIN stg_pmt_req_datamart b ON b.NOTE_NO = a.NotaNo where a.PaymentRequestID = '$detailID' ORDER BY a.PolisNo ASC ");
		$ccy = mysqli_query($conn, "SELECT DISTINCT CCY FROM tpaymentrequestdetail where PaymentRequestID = '$detailID' ORDER BY PolisNo ASC");
		$no = 1;
		
		$createWhoData	 =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$fetch[CREATED_BY]'");
		$checkerWhoData	 =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$fetch[CHECKER_BY]'");
		$approvalWhoData =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$fetch[APPROVAL_BY]'");
		
		$createWho = mysqli_fetch_array($createWhoData);
		$checkerWho = mysqli_fetch_array($checkerWhoData);
		$approvalWho = mysqli_fetch_array($approvalWhoData);	
		
		while ($while = mysqli_fetch_array($data2)) 
		{
			$GrossPremium 	= 	number_format( $while['GrossPremium'] , 2 , '.' , ',' );
			$Amount 		=	number_format( $while['Amount'] , 2 , '.' , ',' );
			$bankSettlementData = wordwrap($while['BankSettlement'], 12, '<br />', true);
			$accountSettlementData = wordwrap($while['AccountSettlement'], 12, '<br />', true);
			$VoucherSettlement = wordwrap($while['VoucherSettlement'], 18, '<br />', true);
			$DateSettlement = wordwrap($while['DateSettlement'], 11, '<br />', true);
			$content .="
					<tr>
						<td width='5' align='center'>$no</td>
						<td width='120' style='word-wrap: break-word;'>$while[PolisNo]</td>
						<td width='30' align='center'>$while[Corr]</td>
						<td width='140' style='word-wrap: break-word;'>$while[InsuredName]</td>
						<td width='95' style='word-wrap: break-word;' align='center'>$while[NotaNo]</td>
						<td width='10' style='word-wrap: break-word;' align='center'>PAID</td>
						<td width='35' style='word-wrap:break-word;' align='center'>$DateSettlement</td>
						<td width='60' style='word-wrap:break-word;' align='center'>$bankSettlementData</td>
						<td width='51' style='word-wrap:break-word;' align='center'>$accountSettlementData</td>
						<td width='75' style='word-wrap:break-word;' align='center'>$VoucherSettlement</td>
						<td align='center'>$while[CCY]</td>
						<td width='50' style='word-wrap: break-word;' align='right'>$GrossPremium</td>
						<td width='20' style='word-wrap: break-word;' align='right'>$Amount</td>
						<td width='45' style='word-wrap: break-word;' align='center'>$while[GROUP_TYPE]</td>
					</tr>
					";
			$no++;
		}
		while($fetchccy	=	mysqli_fetch_array($ccy))
		{
			$totalquery 	=	mysqli_query($conn, "SELECT SUM(Amount) AS total from tpaymentrequestdetail where PaymentRequestID = '$detailID' AND CCY = '$fetchccy[CCY]' ");
			$totalquery2 	=	mysqli_query($conn, "SELECT SUM(GrossPremium) AS total from tpaymentrequestdetail where PaymentRequestID = '$detailID' AND CCY = '$fetchccy[CCY]' ");
			
			$totalAmount 	=	mysqli_fetch_array($totalquery);
			$totalAmount2 	=	mysqli_fetch_array($totalquery2);
			$totalbayar 	=	number_format( $totalAmount['total'] , 2 , '.' , ',' );
			$totalbayar2 	=	number_format( $totalAmount2['total'] , 2 , '.' , ',' );
			$content .= "
							<tr>
								<td colspan='11' align='right'>TOTAL $fetchccy[CCY]</td>
								<td align='right'>$totalbayar2</td>
								<td align='right'>$totalbayar</td>
							</tr>
				";
		}
		$content .= "
				</tbody>
			</table>
			
			<br>
			
			<div>
				<span style='font-size:9px;'>
					Note :  <ul>
								<li>This application already approve by system</li>
								<li>Polis yang ditampilkan adalah polis yang pembayaran preminya sudah lunas</li>
							</ul> 
						   
				</span>
			</div>

			<br>
			<div>
				<table border='1px' cellpadding='1' cellspacing='1' style='width :450px; height :100px; overflow: hidden;margin-left : 595px; text-align : center; align:center; font-size:10px;'>
					
						<tr>
							<th>DIBUAT</th>
							<th>DIPERIKSA</th>
							<th>DISETUJUI</th>
						</tr>
					
					
						<tr>
							<td style='height:100px; width:150px; text-align:justify; padding:0;'><span>&nbsp;</span></td>
							<td style='height:100px; width:150px; text-align:justify; padding:0;'><span>&nbsp;</span></td>
							<td style='height:100px; width:150px; text-align:justify; padding:0;'><span>&nbsp;</span></td>
						</tr>
						<tr>
						
						
							<td><span>$createWho[full_name]</span></td>
							<td><span>$checkerWho[full_name]</span></td>
							<td><span>$approvalWho[full_name]</span></td>
						</tr>
					
				</table>
			</div>
		</page>
		";

		require_once('../html2pdf/html2pdf.class.php');
		//$html2pdf = new HTML2PDF('L','A4','en', array('6','10','6','10'));
		$html2pdf = new HTML2PDF('L', 'A4', 'en', true, 'UTF-8', array('7','10','5','4'));
		$html2pdf -> writeHTML($content);
		$html2pdf -> Output('Paymet Request.pdf');
		
	}
	
	
}
?>