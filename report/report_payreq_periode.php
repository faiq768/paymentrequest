<?php
/*
** Created By Hockey			**
** Created Date 20180404	 	**
** Print PayReq Periode			**
*/ 
session_start();
ob_start();
//koneksi
include "../koneksi/koneksi.php";

if (isset($_GET['datefrom']) && isset($_GET['dateto']) ) 
{
	$datefrom 		= 	$_GET['datefrom'];
	$dateto	 		= 	$_GET['dateto'];
	$branch_code	= 	$_GET['bc'];

	$datefrom1 = date_create($datefrom);
	$datefrom2 = date_format($datefrom1,"Y-m-d");
	
	$dateto1 = date_create($dateto);
	$dateto2 = date_format($dateto1,"Y-m-d");

	$queryAll = "SELECT a.* FROM tpaymentrequestheader a 
		JOIN security_user b ON b.username=a.CREATED_BY
		JOIN (select * from security_user where username='".$_SESSION['username']."') c ON 
			(
				(
					c.home_branch=b.home_branch
					AND c.Dept_id=b.Dept_id
				)
				OR
				(
                    b.home_branch != (SELECT Value FROM mgeneral_table WHERE Code='BRANCH_HO') 
                    AND c.home_branch = (SELECT Value FROM mgeneral_table WHERE Code='BRANCH_HO')
                    AND c.Dept_id in (SELECT Value FROM mgeneral_table WHERE Code='DEPARTMENT_BRANCHCO')
                )
                OR
                (
                	c.home_branch = (SELECT Value FROM mgeneral_table WHERE Code='BRANCH_HO')
                    AND c.Dept_id in (SELECT Value FROM mgeneral_table WHERE Code='DEPARTMENT_FINANCE_PUSAT')
                )
			)
		WHERE a.STATUS_PR=(SELECT DESCRIPTION FROM mgeneral_table WHERE Code='PaymentRequest_STATUS' AND Value=5) 
			AND a.APPROVAL_DATE >='$datefrom2' AND a.APPROVAL_DATE <='$dateto2' AND a.PaymentRequestNo like '$branch_code%'
		ORDER BY a.PaymentRequestNo
		";

		
		
	$dataAll = mysqli_query($conn, $queryAll);
	$dataRow = $dataAll->num_rows;
		
	if($dataRow > 0)
	{
		require_once('../html2pdf/html2pdf.class.php');
		$html2pdf = new HTML2PDF('L', 'A4', 'en', true, 'UTF-8', array('5','10','5','10'));
		$countRow = 1;
		
		
			
		
		
	 	while($rows =	mysqli_fetch_array($dataAll))
		{
			$id 	= $rows['ID'];
			$payreqNo 	= $rows['PaymentRequestNo'];
			
			$createWhoData	 =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$rows[CREATED_BY]'");
		$checkerWhoData	 =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$rows[CHECKER_BY]'");
		$approvalWhoData =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$rows[APPROVAL_BY]'");
		
		$createWho = mysqli_fetch_array($createWhoData);
		$checkerWho = mysqli_fetch_array($checkerWhoData);
		$approvalWho = mysqli_fetch_array($approvalWhoData);
		
		

			date_default_timezone_set('Asia/Jakarta');
			$tgl = date("dmY H:i:s");
			
			if($countRow <= $dataRow)
			{
				$content = ob_get_clean();
			}
				
			$content .= "
			<style type='text/css'>
				p {
					line-height:1.5 !important;
				}
				.two{
					font-size : 10.5px;
					border-collapse : collapse;
				}
				.two thead tr th{
					text-align : center;
					padding : 5px;
				}
				.two tbody tr td{
					padding : 4px;
				}
				.ttd{
					width :450px;
					height :100px;
					overflow: hidden;
					margin-left : 623px;

				}
				.ttd table{
					border-collapse : collapse;
				}
				.ttd table tr th{
					padding : 5px 20px 5px 20px;
				}
				.ttd table tr td{
					padding : 40px;
					height : 40px;
					text-align : center;
				}
				.footer{
					width :450px;
					height :100px;
					overflow: hidden;
					margin-left : 623px;
					text-align : center;
				}
			</style>
			<page>
				<div style='text-align : center' padding-left: 8px;>
						<h3>PAYMENT REQUEST</h3>
						<p> $rows[PaymentRequestNo]</p>
						<hr>
					</div>
				<page_footer>
					<div>
						<!--
						<div align='left'>
							$_SESSION[username] - $tgl
						</div>
						-->
						<div align='right'>
							[[page_cu]]/[[page_nb]]
						</div>
					</div>
				</page_footer>

				
				<div>
					<p>Kepada Yth,</p>
					<p>Bagian Branch Co / Bagian Finance</p>
					<br>
					<p>Mohon diberikan persetujuan untuk pembayaran sebagai berikut :</p>
					<br>
					<table class='first'>
						<tr>
							<td>PAY TO / CLIENT NO</td>
							<td>:</td>
							<td>$rows[BeneficiaryName] / $rows[PayTo] </td>
						</tr>
						<tr>
							<td>BENEFICIARY NAME</td>
							<td>:</td>
							<td>$rows[BeneficiaryName] </td>
						</tr>
						<tr>
							<td>BANK</td>
							<td>:</td>
							<td>$rows[Bank] </td>
						</tr>
						<tr>
							<td>DESTINATION ACCOUNT NUMBER</td>
							<td>:</td>
							<td>$rows[DestinationAccountNumber]</td>
						</tr>
						<tr>
							<td>BRANCH </td>
							<td>:</td>
							<td>$rows[Branch]</td>
						</tr>
						<tr>
							<td>CITY</td>
							<td>:</td>
							<td>$rows[City]</td>
						</tr>
						<tr>
							<td>RATE</td>
							<td>:</td>
							<td>$rows[Rate]</td>
						</tr>
						<tr>
							<td>TYPE OF PAYMENT</td>
							<td>:</td>
							<td>$rows[TypeOfPayment]</td>
						</tr>
						<tr>
							<td style='vertical-align: top'>Memo</td>
							<td style='vertical-align: top'>:</td>
							<td width='800' style='word-wrap: break-word;'>$rows[Memo]</td>
						</tr>
					</table>
				</div>
				<br/>";
			$content .= "
				<table class='two' border='1px' cellpadding='1' cellspacing='0'>
					<thead>
						<tr>
							<th rowspan='2'>NO</th>
							<th rowspan='2'>POLIS NUMBER</th>
							<th rowspan='2'>CORR</th>
							<th rowspan='2'>INSURED NAME</th>
							<th colspan='6'>SETTLEMENT REFFERENCE</th>
							<th rowspan='2'>CCY</th>
							<th rowspan='2'>GROSS PREMIUM</th>
							<th rowspan='2'>AMOUNT</th>
						</tr>
						<tr>
							<th>NOTE NO</th>
							<th>STATUS</th>
							<th>DATE</th>
							<th>BANK</th>
							<th>ACCOUNT</th>
							<th>VOUCHER</th>
						</tr>
					</thead>
					<tbody>";

			$data2	=	mysqli_query($conn, "SELECT * FROM tpaymentrequestdetail where PaymentRequestID = '$id' ORDER BY PolisNo ASC ");
			$no = 1;
			while ($while = mysqli_fetch_array($data2)) 
			{
				$GrossPremium 	= 	number_format( $while['GrossPremium'] , 2 , '.' , ',' );
				$Amount 		=	number_format( $while['Amount'] , 2 , '.' , ',' );
				
				$bankSettlementData = wordwrap($while[BankSettlement], 12, '<br />', true);
				$accountSettlementData = wordwrap($while[AccountSettlement], 12, '<br />', true);
				
				$content .="
						<tr>
							<td align='center'>$no</td>
							<td style='word-wrap: break-word;'>$while[PolisNo]</td>
							<td>$while[Corr]</td>
							<td width='100' style='word-wrap: break-word;'>$while[InsuredName]</td>
							<td width='110' style='word-wrap: break-word;' align='center'>$while[NotaNo]</td>
							<td width='25' style='word-wrap: break-word;' align='center'>PAID</td>
							<td style='word-wrap: break-word;' align='center'>$while[DateSettlement]</td>
							<td width='70' style='word-wrap: break-word;' align='center'>$bankSettlementData</td>
							<td width='70' style='word-wrap: break-word;' align='center'>$accountSettlementData</td>
							<td style='word-wrap: break-word;' align='center'>$while[VoucherSettlement]</td>
							<td style='word-wrap: break-word;' align='center'>$while[CCY]</td>
							<td width='50' style='word-wrap: break-word;' align='right'>$GrossPremium</td>
							<td width='50' style='word-wrap: break-word;' align='right'>$Amount</td>
						</tr>
						";
				$no++;
			}
			
			$ccy = mysqli_query($conn, "SELECT DISTINCT CCY FROM tpaymentrequestdetail where PaymentRequestID = '$id' ORDER BY PolisNo ASC");
			while($fetchccy	=	mysqli_fetch_array($ccy))
			{
				$totalquery 	=	mysqli_query($conn, "SELECT SUM(Amount) AS total from tpaymentrequestdetail where PaymentRequestID = '$id' AND CCY = '$fetchccy[CCY]' ");
				$totalAmount 	=	mysqli_fetch_array($totalquery);
				$totalbayar 	=	number_format( $totalAmount['total'] , 2 , '.' , ',' );

				$content .= "
								<tr>
									<td colspan='12' align='right'>TOTAL PAYMENT $fetchccy[CCY]</td>
									<td align='right'>$totalbayar</td>
								</tr>
					";
			}

			$content .= "
					</tbody>
				</table>
				<br>
				<br>
				<div>
					<h4>
						Note : This application already approve by system
					</h4>
				</div>
				<br>
				<div>
					<table border='1' cellpadding='1' cellspacing='1' style='width :450px; height :100px; overflow: hidden;margin-left : 615px; text-align : center; align:center;'>
						
							<tr>
								<th>DIBUAT</th>
								<th>DIPERIKSA</th>
								<th>DISETUJUI</th>
							</tr>
						
						
							<tr>
								<td style='height:100px; width:150px; text-align:center; padding:0;'><span>&nbsp;</span></td>
								<td style='height:100px; width:150px; text-align:center; padding:0;'><span>&nbsp;</span></td>
								<td style='height:100px; width:150px; text-align:center; padding:0;'><span>&nbsp;</span></td>

							</tr>
						<tr>
								<td><span>$createWho[full_name]</span></td>
								<td><span>$checkerWho[full_name]</span></td>
								<td><span>$approvalWho[full_name]</span></td>

							</tr>
					</table>
				</div>
			</page>";
			$countRow = $countRow + 1;
			$html2pdf -> writeHTML($content);
				
		}
		
		$html2pdf -> Output('Paymet Request.pdf');	
		
	}
	else
	{
		$_SESSION['notif'] = 'PRINT_PR-FAILED';		
		echo "<script>javascript:history.back()</script>";
	}
}
else
{
	$_SESSION['notif'] = 'PRINT_PR-FAILED';		
	echo "<script>javascript:history.back()</script>";
}
?>