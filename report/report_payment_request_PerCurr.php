<?php 
/*
** Created By Faiq				**
** Modified By Hockey        	**
** Modified Date 20180322	 	**
** Validation Created 			**
** Rapiin tampilan	 			**
*/ 
session_start();
ob_start();
//koneksi
$conn = mysqli_connect('localhost','root','') or die("cannot connect");
		mysqli_select_db($conn, 'paymentrequest');

if (empty($_GET['id']) || empty($_SESSION['username'])) {
	echo "gagal";
}else{
	$ID = $_GET['id'];
	$query = "SELECT * FROM tpaymentrequestheader where ID = '$ID' AND CREATED_BY='".$_SESSION['username']."' ";
	if($data = mysqli_query($conn, $query))
	{
	$fetch = mysqli_fetch_array($data);

$content = ob_get_clean();

$content = '
<style type="text/css">
	p {
		line-height:1.5 !important;
	}
	.two{
		font-size : 10.5px;
		border-collapse : collapse;
	}
	.two thead tr th{
		text-align : center;
		padding : 5px;
	}
	.two tbody tr td{
		padding : 4px;
	}
	.ttd{
		width :450px;
		height :100px;
		overflow: hidden;
		margin-left : 623px;

	}
	.ttd table{
		border-collapse : collapse;
	}
	.ttd table tr th{
		padding : 5px 20px 5px 20px;
	}
	.ttd table tr td{
		padding : 40px;
		height : 40px;
		text-align : center;
	}
	.footer{
		width :450px;
		height :100px;
		overflow: hidden;
		margin-left : 623px;
		text-align : center;
	}
</style>
';

$content .= "
<page>

		<div style='text-align : center' padding-left: 8px;>
			<h3>PAYMENT REQUEST</h3>
			<p> $fetch[PaymentRequestNo]</p>
			<hr>
		</div>
	<!--<page_footer>
		<div align='right'>
			[[page_cu]]/[[page_nb]]
		</div>
	</page_footer>-->

	
	<div>
	<br>
		<p>Kepada Yth,</p>
		<p>Bagian Branch Co / Bagian Finance</p>
		<br>
		<p>Mohon diberikan persetujuan untuk pembayaran sebagai berikut :</p>
		<br>
		<table class='first'>
			<tr>
				<td>PAY TO / CLIENT NO</td>
				<td>:</td>
				<td>$fetch[BeneficiaryName] / $fetch[PayTo] </td>
			</tr>
			<tr>
				<td>BENEFICIARY NAME</td>
				<td>:</td>
				<td>$fetch[BeneficiaryName] </td>
			</tr>
			<tr>
				<td>BANK</td>
				<td>:</td>
				<td>$fetch[Bank] </td>
			</tr>
			<tr>
				<td>DESTINATION ACCOUNT NUMBER</td>
				<td>:</td>
				<td>$fetch[DestinationAccountNumber]</td>
			</tr>
			<tr>
				<td>BRANCH </td>
				<td>:</td>
				<td>$fetch[Branch]</td>
			</tr>
			<tr>
				<td>CITY</td>
				<td>:</td>
				<td>$fetch[City]</td>
			</tr>
			<tr>
				<td>RATE</td>
				<td>:</td>
				<td>$fetch[Rate]</td>
			</tr>
			<tr>
				<td>TYPE OF PAYMENT</td>
				<td>:</td>
				<td>$fetch[TypeOfPayment]</td>
			</tr>
		</table>
	</div><br/>";
$detailID = $fetch['ID'];
$ccy 	=	mysqli_query($conn, "SELECT DISTINCT CCY FROM tpaymentrequestdetail where PaymentRequestID = '$detailID' ");
while ($byccy = mysqli_fetch_array($ccy)) {
	
$content .= "
	<br>
	<table class='two' border='1px' cellpadding='1' cellspacing='0'>
		<thead>
			<tr>
				<th rowspan='2'>No</th>
				<th rowspan='2'>POLIS NUMBER</th>
				<th rowspan='2'>CORR</th>
				<th rowspan='2'>INSURED NAME</th>
				<th colspan='6'>SETTLEMENT REFFERENCE</th>
				<th rowspan='2'>CCY</th>
				<th rowspan='2'>GROSS PREMIUM</th>
				<th rowspan='2'>AMOUNT</th>
			</tr>
			<tr>
				<th>NOTE NO</th>
				<th>STATUS</th>
				<th>DATE</th>
				<th>BANK</th>
				<th>ACCOUNT</th>
				<th>VOUCHER</th>
			</tr>

		</thead>
		<tbody>";

$data2	=	mysqli_query($conn, "SELECT * FROM tpaymentrequestdetail where PaymentRequestID = '$detailID' AND CCY = '$byccy[CCY]' ");
$no = 1;


		$createWhoData	 =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$fetch[CREATED_BY]'");
		$checkerWhoData	 =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$fetch[CHECKER_BY]'");
		$approvalWhoData =	mysqli_query($conn, "SELECT * FROM security_user WHERE username = '$fetch[APPROVAL_BY]'");
		
		$createWho = mysqli_fetch_array($createWhoData);
		$checkerWho = mysqli_fetch_array($checkerWhoData);
		$approvalWho = mysqli_fetch_array($approvalWhoData);

while ($while = mysqli_fetch_array($data2)) {
	$GrossPremium 	= 	number_format( $while['GrossPremium'] , 2 , '.' , ',' );
	$Amount 		=	number_format( $while['Amount'] , 2 , '.' , ',' );
	$bankSettlementData = wordwrap($while[BankSettlement], 12, '<br />', true);
	$accountSettlementData = wordwrap($while[AccountSettlement], 12, '<br />', true);
	
	
	$content .="
					<tr>
						<td align='center'>$no</td>
						<td>$while[PolisNo]</td>
						<td>$while[Corr]</td>
						<td width='100' style='word-wrap: break-word;'>$while[InsuredName]</td>
						<td width='110' style='word-wrap: break-word;' align='center'>$while[NotaNo]</td>
						<td width='30' style='word-wrap: break-word;' align='center'>PAID</td>
						<td align='center'>$while[DateSettlement]</td>
						<td width='70' style='word-wrap: break-word;' align='center'>$bankSettlementData</td>
						<td width='70' style='word-wrap: break-word;' align='center'>$accountSettlementData</td>
						<td align='center'>$while[VoucherSettlement]</td>
						<td align='center'>$while[CCY]</td>
						<td width='60' style='word-wrap: break-word;' align='right'>$GrossPremium</td>
						<td width='60' style='word-wrap: break-word;' align='right'>$Amount</td>
					</tr>
					";
					$no++;
}
		
$totalquery 	=	mysqli_query($conn, "SELECT SUM(Amount) AS total from tpaymentrequestdetail where PaymentRequestID = '$detailID' AND CCY = '$byccy[CCY]' ");
$totalAmount 	=	mysqli_fetch_array($totalquery);
$totalbayar 	=	number_format( $totalAmount['total'] , 2 , '.' , ',' );
$content .= "
					<tr>
						<td colspan='12' align='right'>TOTAL </td>
						<td align='right'>$totalbayar</td>
					</tr>
		</tbody>
	</table>";
}
$content .= "<br>
	<br>
	<div>
		<h4>
			Note : This application already approve by system
		</h4>
	</div>
	<br>
	
	<div>
		<table border='1' cellpadding='1' cellspacing='1' style='width :450px; height :100px; overflow: hidden;margin-left : 615px; text-align : center; align:center;'>
			
				<tr>
					<th>DIBUAT</th>
					<th>DIPERIKSA</th>
					<th>DISETUJUI</th>
				</tr>
			
			
				<tr>
					<td style='height:100px; width:150px; text-align:center; padding:0;'><span>&nbsp;</span></td>
					<td style='height:100px; width:150px; text-align:center; padding:0;'><span>&nbsp;</span></td>
					<td style='height:100px; width:150px; text-align:center; padding:0;'><span>&nbsp;</span></td>
				</tr>
			<tr>
					<td><span>$createWho[full_name]</span></td>
					<td><span>$checkerWho[full_name]</span></td>
					<td><span>$approvalWho[full_name]</span></td>				
					
					
				</tr>
		</table>
	</div>
</page>
";

require_once('../html2pdf/html2pdf.class.php');
//$html2pdf = new HTML2PDF('L','A4','en', array('6','10','6','10'));
$html2pdf = new HTML2PDF('L', 'A4', 'en', true, 'UTF-8', array('5','10','5','10'));


$html2pdf -> writeHTML($content);
$html2pdf -> Output('Paymet Request.pdf');
}
}
?>